import { createContext } from "react";
import { ColorScheme } from "./types";

const initialState: ColorScheme = "system";

export const ColorSchemeStateContext = createContext<ColorScheme>(initialState);

export const ColorSchemeDispatchContext = createContext<(verticalMode: ColorScheme) => void>(() => undefined);
