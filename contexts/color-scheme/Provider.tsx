import { FC, ReactNode, useState } from "react";
import { ColorSchemeDispatchContext, ColorSchemeStateContext } from "./context";
import { ColorScheme } from "./types";

type Props = { colorScheme: ColorScheme; children: ReactNode };

export const ColorSchemeProvider: FC<Props> = ({ colorScheme: propsColorScheme, children }) => {
  const [colorScheme, setColorScheme] = useState(propsColorScheme);

  const updateColorScheme = (colorScheme: ColorScheme) => {
    setColorScheme(colorScheme);
  };

  return (
    <ColorSchemeStateContext.Provider value={colorScheme}>
      <ColorSchemeDispatchContext.Provider value={updateColorScheme}>{children}</ColorSchemeDispatchContext.Provider>
    </ColorSchemeStateContext.Provider>
  );
};
