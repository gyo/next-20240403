import { useContext } from "react";
import { ColorSchemeDispatchContext, ColorSchemeStateContext } from "./context";
import { ColorSchemeCssValue } from "./types";

export const useColorScheme = () => {
  const colorScheme = useContext(ColorSchemeStateContext);
  const colorSchemeCssValue: ColorSchemeCssValue = colorScheme === "system" ? "light dark" : colorScheme;
  return { colorScheme, colorSchemeCssValue };
};

export const useColorSchemeDispatch = () => {
  return useContext(ColorSchemeDispatchContext);
};
