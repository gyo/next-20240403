export type ColorScheme = "light" | "dark" | "system";
export type ColorSchemeCssValue = "light" | "dark" | "light dark";
