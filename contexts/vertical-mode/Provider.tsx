import { FC, ReactNode, useState } from "react";
import { VerticalModeDispatchContext, VerticalModeStateContext } from "./context";
import { VerticalMode } from "./types";

type Props = { verticalMode: VerticalMode; children: ReactNode };

export const VerticalModeProvider: FC<Props> = ({ verticalMode: propsVerticalMode, children }) => {
  const [verticalMode, setVerticalMode] = useState(propsVerticalMode);

  const updateVerticalMode = (verticalMode: VerticalMode) => {
    setVerticalMode(verticalMode);
  };

  return (
    <VerticalModeStateContext.Provider value={verticalMode}>
      <VerticalModeDispatchContext.Provider value={updateVerticalMode}>{children}</VerticalModeDispatchContext.Provider>
    </VerticalModeStateContext.Provider>
  );
};
