import { createContext } from "react";
import { VerticalMode } from "./types";

const initialState: VerticalMode = "horizontal-tb";

export const VerticalModeStateContext = createContext<VerticalMode>(initialState);

export const VerticalModeDispatchContext = createContext<(verticalMode: VerticalMode) => void>(() => undefined);
