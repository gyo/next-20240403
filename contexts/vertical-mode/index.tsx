import { useContext } from "react";
import { VerticalModeDispatchContext, VerticalModeStateContext } from "./context";

export const useVerticalMode = () => {
  return useContext(VerticalModeStateContext);
};

export const useVerticalModeDispatch = () => {
  return useContext(VerticalModeDispatchContext);
};
