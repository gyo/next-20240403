import { createContext } from "react";
import { makeTheme } from "./theme";

const initialState = makeTheme(0, 0, 0, 0);

export const ThemeStateContext = createContext(initialState);

export const ThemeDispatchContext = createContext<(baseSize: number, lightness: number, chroma: number, hue: number) => void>(() => undefined);
