export type Value = string;
export type Name = `--${string}`;
export type Variable = `var(${Name})`;

export type Values = { [key: string]: Value };
export type Names = { [key: string]: Name };
export type Variables = { [key: string]: Variable };
