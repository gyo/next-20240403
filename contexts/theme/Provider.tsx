import { FC, ReactNode, useState } from "react";
import { ThemeDispatchContext, ThemeStateContext } from "./context";
import { makeTheme } from "./theme";

type Props = { baseSize: number; lightness: number; chroma: number; hue: number; children: ReactNode };

export const ThemeProvider: FC<Props> = ({ baseSize, lightness, chroma, hue, children }) => {
  const [theme, setTheme] = useState(() => makeTheme(baseSize, lightness, chroma, hue));

  const updateTheme = (baseSize: number, lightness: number, chroma: number, hue: number) => {
    setTheme(makeTheme(baseSize, lightness, chroma, hue));
  };

  return (
    <ThemeStateContext.Provider value={theme}>
      <ThemeDispatchContext.Provider value={updateTheme}>{children}</ThemeDispatchContext.Provider>
    </ThemeStateContext.Provider>
  );
};
