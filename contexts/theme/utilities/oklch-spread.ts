import { getDynamicChromaSpread, getGrayChromaSpread } from "./chroma";
import { pickHighestContrastText, pickMinimumContrastText } from "./contrast";
import { getDynamicLightnessSpread, getStaticLightnessSpread } from "./lightness";

export const getGraySpread = (
  lightnessSpread: ReturnType<typeof getStaticLightnessSpread>,
  chromaSpread: ReturnType<typeof getGrayChromaSpread>,
  hue: number,
) => {
  return {
    gray01: { lightness: lightnessSpread.staticLightness01, chroma: chromaSpread.grayChroma01, hue: hue },
    gray02: { lightness: lightnessSpread.staticLightness02, chroma: chromaSpread.grayChroma02, hue: hue },
    gray03: { lightness: lightnessSpread.staticLightness03, chroma: chromaSpread.grayChroma03, hue: hue },
    gray04: { lightness: lightnessSpread.staticLightness04, chroma: chromaSpread.grayChroma04, hue: hue },
    gray05: { lightness: lightnessSpread.staticLightness05, chroma: chromaSpread.grayChroma05, hue: hue },
    gray06: { lightness: lightnessSpread.staticLightness06, chroma: chromaSpread.grayChroma06, hue: hue },
    gray07: { lightness: lightnessSpread.staticLightness07, chroma: chromaSpread.grayChroma07, hue: hue },
    gray08: { lightness: lightnessSpread.staticLightness08, chroma: chromaSpread.grayChroma08, hue: hue },
    gray09: { lightness: lightnessSpread.staticLightness09, chroma: chromaSpread.grayChroma09, hue: hue },
    gray10: { lightness: lightnessSpread.staticLightness10, chroma: chromaSpread.grayChroma10, hue: hue },
    gray11: { lightness: lightnessSpread.staticLightness11, chroma: chromaSpread.grayChroma11, hue: hue },
    gray12: { lightness: lightnessSpread.staticLightness12, chroma: chromaSpread.grayChroma12, hue: hue },
    gray13: { lightness: lightnessSpread.staticLightness13, chroma: chromaSpread.grayChroma13, hue: hue },
    gray14: { lightness: lightnessSpread.staticLightness14, chroma: chromaSpread.grayChroma14, hue: hue },
    gray15: { lightness: lightnessSpread.staticLightness15, chroma: chromaSpread.grayChroma15, hue: hue },
    gray16: { lightness: lightnessSpread.staticLightness16, chroma: chromaSpread.grayChroma16, hue: hue },
    gray17: { lightness: lightnessSpread.staticLightness17, chroma: chromaSpread.grayChroma17, hue: hue },
    gray18: { lightness: lightnessSpread.staticLightness18, chroma: chromaSpread.grayChroma18, hue: hue },
    gray19: { lightness: lightnessSpread.staticLightness19, chroma: chromaSpread.grayChroma19, hue: hue },
    gray20: { lightness: lightnessSpread.staticLightness20, chroma: chromaSpread.grayChroma20, hue: hue },
  };
};

export const getRedSpread = (
  lightnessSpread: ReturnType<typeof getDynamicLightnessSpread>,
  chromaSpread: ReturnType<typeof getDynamicChromaSpread>,
  hue: number,
) => {
  return {
    red01: { lightness: lightnessSpread.dynamicLightness01, chroma: chromaSpread.dynamicChroma01, hue: hue },
    red02: { lightness: lightnessSpread.dynamicLightness02, chroma: chromaSpread.dynamicChroma02, hue: hue },
    red03: { lightness: lightnessSpread.dynamicLightness03, chroma: chromaSpread.dynamicChroma03, hue: hue },
    red04: { lightness: lightnessSpread.dynamicLightness04, chroma: chromaSpread.dynamicChroma04, hue: hue },
    red05: { lightness: lightnessSpread.dynamicLightness05, chroma: chromaSpread.dynamicChroma05, hue: hue },
    red06: { lightness: lightnessSpread.dynamicLightness06, chroma: chromaSpread.dynamicChroma06, hue: hue },
    red07: { lightness: lightnessSpread.dynamicLightness07, chroma: chromaSpread.dynamicChroma07, hue: hue },
    red08: { lightness: lightnessSpread.dynamicLightness08, chroma: chromaSpread.dynamicChroma08, hue: hue },
    red09: { lightness: lightnessSpread.dynamicLightness09, chroma: chromaSpread.dynamicChroma09, hue: hue },
    red10: { lightness: lightnessSpread.dynamicLightness10, chroma: chromaSpread.dynamicChroma10, hue: hue },
    red11: { lightness: lightnessSpread.dynamicLightness11, chroma: chromaSpread.dynamicChroma11, hue: hue },
    red12: { lightness: lightnessSpread.dynamicLightness12, chroma: chromaSpread.dynamicChroma12, hue: hue },
    red13: { lightness: lightnessSpread.dynamicLightness13, chroma: chromaSpread.dynamicChroma13, hue: hue },
    red14: { lightness: lightnessSpread.dynamicLightness14, chroma: chromaSpread.dynamicChroma14, hue: hue },
    red15: { lightness: lightnessSpread.dynamicLightness15, chroma: chromaSpread.dynamicChroma15, hue: hue },
    red16: { lightness: lightnessSpread.dynamicLightness16, chroma: chromaSpread.dynamicChroma16, hue: hue },
    red17: { lightness: lightnessSpread.dynamicLightness17, chroma: chromaSpread.dynamicChroma17, hue: hue },
    red18: { lightness: lightnessSpread.dynamicLightness18, chroma: chromaSpread.dynamicChroma18, hue: hue },
    red19: { lightness: lightnessSpread.dynamicLightness19, chroma: chromaSpread.dynamicChroma19, hue: hue },
    red20: { lightness: lightnessSpread.dynamicLightness20, chroma: chromaSpread.dynamicChroma20, hue: hue },
  };
};

export const getBlueSpread = (
  lightnessSpread: ReturnType<typeof getDynamicLightnessSpread>,
  chromaSpread: ReturnType<typeof getDynamicChromaSpread>,
  hue: number,
) => {
  return {
    blue01: { lightness: lightnessSpread.dynamicLightness01, chroma: chromaSpread.dynamicChroma01, hue: hue },
    blue02: { lightness: lightnessSpread.dynamicLightness02, chroma: chromaSpread.dynamicChroma02, hue: hue },
    blue03: { lightness: lightnessSpread.dynamicLightness03, chroma: chromaSpread.dynamicChroma03, hue: hue },
    blue04: { lightness: lightnessSpread.dynamicLightness04, chroma: chromaSpread.dynamicChroma04, hue: hue },
    blue05: { lightness: lightnessSpread.dynamicLightness05, chroma: chromaSpread.dynamicChroma05, hue: hue },
    blue06: { lightness: lightnessSpread.dynamicLightness06, chroma: chromaSpread.dynamicChroma06, hue: hue },
    blue07: { lightness: lightnessSpread.dynamicLightness07, chroma: chromaSpread.dynamicChroma07, hue: hue },
    blue08: { lightness: lightnessSpread.dynamicLightness08, chroma: chromaSpread.dynamicChroma08, hue: hue },
    blue09: { lightness: lightnessSpread.dynamicLightness09, chroma: chromaSpread.dynamicChroma09, hue: hue },
    blue10: { lightness: lightnessSpread.dynamicLightness10, chroma: chromaSpread.dynamicChroma10, hue: hue },
    blue11: { lightness: lightnessSpread.dynamicLightness11, chroma: chromaSpread.dynamicChroma11, hue: hue },
    blue12: { lightness: lightnessSpread.dynamicLightness12, chroma: chromaSpread.dynamicChroma12, hue: hue },
    blue13: { lightness: lightnessSpread.dynamicLightness13, chroma: chromaSpread.dynamicChroma13, hue: hue },
    blue14: { lightness: lightnessSpread.dynamicLightness14, chroma: chromaSpread.dynamicChroma14, hue: hue },
    blue15: { lightness: lightnessSpread.dynamicLightness15, chroma: chromaSpread.dynamicChroma15, hue: hue },
    blue16: { lightness: lightnessSpread.dynamicLightness16, chroma: chromaSpread.dynamicChroma16, hue: hue },
    blue17: { lightness: lightnessSpread.dynamicLightness17, chroma: chromaSpread.dynamicChroma17, hue: hue },
    blue18: { lightness: lightnessSpread.dynamicLightness18, chroma: chromaSpread.dynamicChroma18, hue: hue },
    blue19: { lightness: lightnessSpread.dynamicLightness19, chroma: chromaSpread.dynamicChroma19, hue: hue },
    blue20: { lightness: lightnessSpread.dynamicLightness20, chroma: chromaSpread.dynamicChroma20, hue: hue },
  };
};

export const getThemeSpread = (
  lightnessSpread: ReturnType<typeof getDynamicLightnessSpread>,
  chromaSpread: ReturnType<typeof getDynamicChromaSpread>,
  hue: number,
) => {
  return {
    theme01: { lightness: lightnessSpread.dynamicLightness01, chroma: chromaSpread.dynamicChroma01, hue: hue },
    theme02: { lightness: lightnessSpread.dynamicLightness02, chroma: chromaSpread.dynamicChroma02, hue: hue },
    theme03: { lightness: lightnessSpread.dynamicLightness03, chroma: chromaSpread.dynamicChroma03, hue: hue },
    theme04: { lightness: lightnessSpread.dynamicLightness04, chroma: chromaSpread.dynamicChroma04, hue: hue },
    theme05: { lightness: lightnessSpread.dynamicLightness05, chroma: chromaSpread.dynamicChroma05, hue: hue },
    theme06: { lightness: lightnessSpread.dynamicLightness06, chroma: chromaSpread.dynamicChroma06, hue: hue },
    theme07: { lightness: lightnessSpread.dynamicLightness07, chroma: chromaSpread.dynamicChroma07, hue: hue },
    theme08: { lightness: lightnessSpread.dynamicLightness08, chroma: chromaSpread.dynamicChroma08, hue: hue },
    theme09: { lightness: lightnessSpread.dynamicLightness09, chroma: chromaSpread.dynamicChroma09, hue: hue },
    theme10: { lightness: lightnessSpread.dynamicLightness10, chroma: chromaSpread.dynamicChroma10, hue: hue },
    theme11: { lightness: lightnessSpread.dynamicLightness11, chroma: chromaSpread.dynamicChroma11, hue: hue },
    theme12: { lightness: lightnessSpread.dynamicLightness12, chroma: chromaSpread.dynamicChroma12, hue: hue },
    theme13: { lightness: lightnessSpread.dynamicLightness13, chroma: chromaSpread.dynamicChroma13, hue: hue },
    theme14: { lightness: lightnessSpread.dynamicLightness14, chroma: chromaSpread.dynamicChroma14, hue: hue },
    theme15: { lightness: lightnessSpread.dynamicLightness15, chroma: chromaSpread.dynamicChroma15, hue: hue },
    theme16: { lightness: lightnessSpread.dynamicLightness16, chroma: chromaSpread.dynamicChroma16, hue: hue },
    theme17: { lightness: lightnessSpread.dynamicLightness17, chroma: chromaSpread.dynamicChroma17, hue: hue },
    theme18: { lightness: lightnessSpread.dynamicLightness18, chroma: chromaSpread.dynamicChroma18, hue: hue },
    theme19: { lightness: lightnessSpread.dynamicLightness19, chroma: chromaSpread.dynamicChroma19, hue: hue },
    theme20: { lightness: lightnessSpread.dynamicLightness20, chroma: chromaSpread.dynamicChroma20, hue: hue },
  };
};

export const getGrayOnGraySpread = (backgroundSpread: ReturnType<typeof getGraySpread>, graySpread: ReturnType<typeof getGraySpread>) => {
  const pickList = [graySpread.gray01, graySpread.gray20];
  return {
    grayOnGray01: pickHighestContrastText(pickList, backgroundSpread.gray01),
    grayOnGray02: pickHighestContrastText(pickList, backgroundSpread.gray02),
    grayOnGray03: pickHighestContrastText(pickList, backgroundSpread.gray03),
    grayOnGray04: pickHighestContrastText(pickList, backgroundSpread.gray04),
    grayOnGray05: pickHighestContrastText(pickList, backgroundSpread.gray05),
    grayOnGray06: pickHighestContrastText(pickList, backgroundSpread.gray06),
    grayOnGray07: pickHighestContrastText(pickList, backgroundSpread.gray07),
    grayOnGray08: pickHighestContrastText(pickList, backgroundSpread.gray08),
    grayOnGray09: pickHighestContrastText(pickList, backgroundSpread.gray09),
    grayOnGray10: pickHighestContrastText(pickList, backgroundSpread.gray10),
    grayOnGray11: pickHighestContrastText(pickList, backgroundSpread.gray11),
    grayOnGray12: pickHighestContrastText(pickList, backgroundSpread.gray12),
    grayOnGray13: pickHighestContrastText(pickList, backgroundSpread.gray13),
    grayOnGray14: pickHighestContrastText(pickList, backgroundSpread.gray14),
    grayOnGray15: pickHighestContrastText(pickList, backgroundSpread.gray15),
    grayOnGray16: pickHighestContrastText(pickList, backgroundSpread.gray16),
    grayOnGray17: pickHighestContrastText(pickList, backgroundSpread.gray17),
    grayOnGray18: pickHighestContrastText(pickList, backgroundSpread.gray18),
    grayOnGray19: pickHighestContrastText(pickList, backgroundSpread.gray19),
    grayOnGray20: pickHighestContrastText(pickList, backgroundSpread.gray20),
  };
};

export const getGrayOnRedSpread = (backgroundSpread: ReturnType<typeof getRedSpread>, graySpread: ReturnType<typeof getGraySpread>) => {
  const pickList = [graySpread.gray01, graySpread.gray20];
  return {
    grayOnRed01: pickHighestContrastText(pickList, backgroundSpread.red01),
    grayOnRed02: pickHighestContrastText(pickList, backgroundSpread.red02),
    grayOnRed03: pickHighestContrastText(pickList, backgroundSpread.red03),
    grayOnRed04: pickHighestContrastText(pickList, backgroundSpread.red04),
    grayOnRed05: pickHighestContrastText(pickList, backgroundSpread.red05),
    grayOnRed06: pickHighestContrastText(pickList, backgroundSpread.red06),
    grayOnRed07: pickHighestContrastText(pickList, backgroundSpread.red07),
    grayOnRed08: pickHighestContrastText(pickList, backgroundSpread.red08),
    grayOnRed09: pickHighestContrastText(pickList, backgroundSpread.red09),
    grayOnRed10: pickHighestContrastText(pickList, backgroundSpread.red10),
    grayOnRed11: pickHighestContrastText(pickList, backgroundSpread.red11),
    grayOnRed12: pickHighestContrastText(pickList, backgroundSpread.red12),
    grayOnRed13: pickHighestContrastText(pickList, backgroundSpread.red13),
    grayOnRed14: pickHighestContrastText(pickList, backgroundSpread.red14),
    grayOnRed15: pickHighestContrastText(pickList, backgroundSpread.red15),
    grayOnRed16: pickHighestContrastText(pickList, backgroundSpread.red16),
    grayOnRed17: pickHighestContrastText(pickList, backgroundSpread.red17),
    grayOnRed18: pickHighestContrastText(pickList, backgroundSpread.red18),
    grayOnRed19: pickHighestContrastText(pickList, backgroundSpread.red19),
    grayOnRed20: pickHighestContrastText(pickList, backgroundSpread.red20),
  };
};

export const getGrayOnBlueSpread = (backgroundSpread: ReturnType<typeof getBlueSpread>, graySpread: ReturnType<typeof getGraySpread>) => {
  const pickList = [graySpread.gray01, graySpread.gray20];
  return {
    grayOnBlue01: pickHighestContrastText(pickList, backgroundSpread.blue01),
    grayOnBlue02: pickHighestContrastText(pickList, backgroundSpread.blue02),
    grayOnBlue03: pickHighestContrastText(pickList, backgroundSpread.blue03),
    grayOnBlue04: pickHighestContrastText(pickList, backgroundSpread.blue04),
    grayOnBlue05: pickHighestContrastText(pickList, backgroundSpread.blue05),
    grayOnBlue06: pickHighestContrastText(pickList, backgroundSpread.blue06),
    grayOnBlue07: pickHighestContrastText(pickList, backgroundSpread.blue07),
    grayOnBlue08: pickHighestContrastText(pickList, backgroundSpread.blue08),
    grayOnBlue09: pickHighestContrastText(pickList, backgroundSpread.blue09),
    grayOnBlue10: pickHighestContrastText(pickList, backgroundSpread.blue10),
    grayOnBlue11: pickHighestContrastText(pickList, backgroundSpread.blue11),
    grayOnBlue12: pickHighestContrastText(pickList, backgroundSpread.blue12),
    grayOnBlue13: pickHighestContrastText(pickList, backgroundSpread.blue13),
    grayOnBlue14: pickHighestContrastText(pickList, backgroundSpread.blue14),
    grayOnBlue15: pickHighestContrastText(pickList, backgroundSpread.blue15),
    grayOnBlue16: pickHighestContrastText(pickList, backgroundSpread.blue16),
    grayOnBlue17: pickHighestContrastText(pickList, backgroundSpread.blue17),
    grayOnBlue18: pickHighestContrastText(pickList, backgroundSpread.blue18),
    grayOnBlue19: pickHighestContrastText(pickList, backgroundSpread.blue19),
    grayOnBlue20: pickHighestContrastText(pickList, backgroundSpread.blue20),
  };
};

export const getGrayOnThemeSpread = (backgroundSpread: ReturnType<typeof getThemeSpread>, graySpread: ReturnType<typeof getGraySpread>) => {
  const pickList = [graySpread.gray01, graySpread.gray20];
  return {
    grayOnTheme01: pickHighestContrastText(pickList, backgroundSpread.theme01),
    grayOnTheme02: pickHighestContrastText(pickList, backgroundSpread.theme02),
    grayOnTheme03: pickHighestContrastText(pickList, backgroundSpread.theme03),
    grayOnTheme04: pickHighestContrastText(pickList, backgroundSpread.theme04),
    grayOnTheme05: pickHighestContrastText(pickList, backgroundSpread.theme05),
    grayOnTheme06: pickHighestContrastText(pickList, backgroundSpread.theme06),
    grayOnTheme07: pickHighestContrastText(pickList, backgroundSpread.theme07),
    grayOnTheme08: pickHighestContrastText(pickList, backgroundSpread.theme08),
    grayOnTheme09: pickHighestContrastText(pickList, backgroundSpread.theme09),
    grayOnTheme10: pickHighestContrastText(pickList, backgroundSpread.theme10),
    grayOnTheme11: pickHighestContrastText(pickList, backgroundSpread.theme11),
    grayOnTheme12: pickHighestContrastText(pickList, backgroundSpread.theme12),
    grayOnTheme13: pickHighestContrastText(pickList, backgroundSpread.theme13),
    grayOnTheme14: pickHighestContrastText(pickList, backgroundSpread.theme14),
    grayOnTheme15: pickHighestContrastText(pickList, backgroundSpread.theme15),
    grayOnTheme16: pickHighestContrastText(pickList, backgroundSpread.theme16),
    grayOnTheme17: pickHighestContrastText(pickList, backgroundSpread.theme17),
    grayOnTheme18: pickHighestContrastText(pickList, backgroundSpread.theme18),
    grayOnTheme19: pickHighestContrastText(pickList, backgroundSpread.theme19),
    grayOnTheme20: pickHighestContrastText(pickList, backgroundSpread.theme20),
  };
};

export const getThemeOnGraySpread = (backgroundSpread: ReturnType<typeof getGraySpread>, themeSpread: ReturnType<typeof getThemeSpread>) => {
  const pickList = [
    themeSpread.theme01,
    themeSpread.theme02,
    themeSpread.theme03,
    themeSpread.theme04,
    themeSpread.theme05,
    themeSpread.theme06,
    themeSpread.theme07,
    themeSpread.theme08,
    themeSpread.theme09,
    themeSpread.theme10,
    themeSpread.theme11,
    themeSpread.theme12,
    themeSpread.theme13,
    themeSpread.theme14,
    themeSpread.theme15,
    themeSpread.theme16,
    themeSpread.theme17,
    themeSpread.theme18,
    themeSpread.theme19,
    themeSpread.theme20,
  ];
  return {
    themeOnGray01: pickMinimumContrastText(pickList, backgroundSpread.gray01),
    themeOnGray02: pickMinimumContrastText(pickList, backgroundSpread.gray02),
    themeOnGray03: pickMinimumContrastText(pickList, backgroundSpread.gray03),
    themeOnGray04: pickMinimumContrastText(pickList, backgroundSpread.gray04),
    themeOnGray05: pickMinimumContrastText(pickList, backgroundSpread.gray05),
    themeOnGray06: pickMinimumContrastText(pickList, backgroundSpread.gray06),
    themeOnGray07: pickMinimumContrastText(pickList, backgroundSpread.gray07),
    themeOnGray08: pickMinimumContrastText(pickList, backgroundSpread.gray08),
    themeOnGray09: pickMinimumContrastText(pickList, backgroundSpread.gray09),
    themeOnGray10: pickMinimumContrastText(pickList, backgroundSpread.gray10),
    themeOnGray11: pickMinimumContrastText(pickList, backgroundSpread.gray11),
    themeOnGray12: pickMinimumContrastText(pickList, backgroundSpread.gray12),
    themeOnGray13: pickMinimumContrastText(pickList, backgroundSpread.gray13),
    themeOnGray14: pickMinimumContrastText(pickList, backgroundSpread.gray14),
    themeOnGray15: pickMinimumContrastText(pickList, backgroundSpread.gray15),
    themeOnGray16: pickMinimumContrastText(pickList, backgroundSpread.gray16),
    themeOnGray17: pickMinimumContrastText(pickList, backgroundSpread.gray17),
    themeOnGray18: pickMinimumContrastText(pickList, backgroundSpread.gray18),
    themeOnGray19: pickMinimumContrastText(pickList, backgroundSpread.gray19),
    themeOnGray20: pickMinimumContrastText(pickList, backgroundSpread.gray20),
  };
};
