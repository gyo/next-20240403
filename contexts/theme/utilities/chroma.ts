import { min, roundToThirdDigit } from "./math";

export const getDynamicChromaSpread = (themeChroma: number) => {
  const MUTABLE_RATE = 0.5;
  const IMMUTABLE_RATE = 1 - MUTABLE_RATE;

  return {
    dynamicChroma01: roundToThirdDigit((Math.sin((0 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma02: roundToThirdDigit((Math.sin((1 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma03: roundToThirdDigit((Math.sin((2 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma04: roundToThirdDigit((Math.sin((3 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma05: roundToThirdDigit((Math.sin((4 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma06: roundToThirdDigit((Math.sin((5 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma07: roundToThirdDigit((Math.sin((6 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma08: roundToThirdDigit((Math.sin((7 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma09: roundToThirdDigit((Math.sin((8 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma10: roundToThirdDigit((Math.sin((9 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma11: roundToThirdDigit((Math.sin((10 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma12: roundToThirdDigit((Math.sin((11 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma13: roundToThirdDigit((Math.sin((12 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma14: roundToThirdDigit((Math.sin((13 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma15: roundToThirdDigit((Math.sin((14 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma16: roundToThirdDigit((Math.sin((15 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma17: roundToThirdDigit((Math.sin((16 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma18: roundToThirdDigit((Math.sin((17 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma19: roundToThirdDigit((Math.sin((18 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
    dynamicChroma20: roundToThirdDigit((Math.sin((19 / 20) * Math.PI) * MUTABLE_RATE + IMMUTABLE_RATE) * themeChroma),
  };
};

export const getGrayChromaSpread = (themeChroma: number) => {
  const max = 0.01;

  const grayChroma = min(themeChroma, max);

  return {
    grayChroma01: grayChroma,
    grayChroma02: grayChroma,
    grayChroma03: grayChroma,
    grayChroma04: grayChroma,
    grayChroma05: grayChroma,
    grayChroma06: grayChroma,
    grayChroma07: grayChroma,
    grayChroma08: grayChroma,
    grayChroma09: grayChroma,
    grayChroma10: grayChroma,
    grayChroma11: grayChroma,
    grayChroma12: grayChroma,
    grayChroma13: grayChroma,
    grayChroma14: grayChroma,
    grayChroma15: grayChroma,
    grayChroma16: grayChroma,
    grayChroma17: grayChroma,
    grayChroma18: grayChroma,
    grayChroma19: grayChroma,
    grayChroma20: grayChroma,
  };
};
