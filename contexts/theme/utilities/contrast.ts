import Color from "colorjs.io";

const newColor = (lightness: number, chroma: number, hue: number) => {
  return new Color("oklch", [lightness / 100, chroma, hue]);
};

export const pickHighestContrastText = (
  pickList: { lightness: number; chroma: number; hue: number }[],
  background: { lightness: number; chroma: number; hue: number },
) => {
  const backgroundColor = newColor(background.lightness, background.chroma, background.hue);
  const contrasts = pickList.map((pickItem) => Math.abs(backgroundColor.contrast(newColor(pickItem.lightness, pickItem.chroma, pickItem.hue), "APCA")));

  let maximumValueIndex = 0;
  for (let index = 1; index < contrasts.length; index++) {
    if (contrasts[maximumValueIndex] < contrasts[index]) {
      maximumValueIndex = index;
    }
  }

  return pickList[maximumValueIndex];
};

export const pickMinimumContrastText = (
  pickList: { lightness: number; chroma: number; hue: number }[],
  background: { lightness: number; chroma: number; hue: number },
) => {
  const backgroundColor = newColor(background.lightness, background.chroma, background.hue);
  const contrasts = pickList.map((pickItem) => Math.abs(backgroundColor.contrast(newColor(pickItem.lightness, pickItem.chroma, pickItem.hue), "APCA")));

  let minimumValueIndex: number | undefined;
  for (let index = 0; index < contrasts.length; index++) {
    if (75 <= contrasts[index]) {
      if (minimumValueIndex === undefined) {
        minimumValueIndex = index;
      }
      if (contrasts[index] < contrasts[minimumValueIndex]) {
        minimumValueIndex = index;
      }
    }
  }

  if (minimumValueIndex === undefined) {
    return pickHighestContrastText(pickList, background);
  } else {
    return pickList[minimumValueIndex];
  }
};
