import { clamp } from "./math";

const LIGHTNESS_MIN = 3;
const LIGHTNESS_MAX = 99.9;

export const getDynamicLightnessSpread = (themeLightness: number) => {
  const step = 5;

  return {
    dynamicLightness01: clamp(LIGHTNESS_MIN, 10 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness02: clamp(LIGHTNESS_MIN, 9 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness03: clamp(LIGHTNESS_MIN, 8 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness04: clamp(LIGHTNESS_MIN, 7 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness05: clamp(LIGHTNESS_MIN, 6 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness06: clamp(LIGHTNESS_MIN, 5 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness07: clamp(LIGHTNESS_MIN, 4 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness08: clamp(LIGHTNESS_MIN, 3 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness09: clamp(LIGHTNESS_MIN, 2 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness10: clamp(LIGHTNESS_MIN, 1 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness11: clamp(LIGHTNESS_MIN, 0 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness12: clamp(LIGHTNESS_MIN, -1 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness13: clamp(LIGHTNESS_MIN, -2 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness14: clamp(LIGHTNESS_MIN, -3 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness15: clamp(LIGHTNESS_MIN, -4 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness16: clamp(LIGHTNESS_MIN, -5 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness17: clamp(LIGHTNESS_MIN, -6 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness18: clamp(LIGHTNESS_MIN, -7 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness19: clamp(LIGHTNESS_MIN, -8 * step + themeLightness, LIGHTNESS_MAX),
    dynamicLightness20: clamp(LIGHTNESS_MIN, -9 * step + themeLightness, LIGHTNESS_MAX),
  };
};

export const getStaticLightnessSpread = () => {
  return {
    staticLightness01: LIGHTNESS_MAX,
    staticLightness02: 95,
    staticLightness03: 90,
    staticLightness04: 85,
    staticLightness05: 80,
    staticLightness06: 75,
    staticLightness07: 70,
    staticLightness08: 65,
    staticLightness09: 60,
    staticLightness10: 55,
    staticLightness11: 50,
    staticLightness12: 45,
    staticLightness13: 40,
    staticLightness14: 35,
    staticLightness15: 30,
    staticLightness16: 25,
    staticLightness17: 20,
    staticLightness18: 15,
    staticLightness19: 10,
    staticLightness20: LIGHTNESS_MIN,
  };
};
