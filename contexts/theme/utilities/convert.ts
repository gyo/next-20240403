import { Name, Values, Variable } from "../types";

export const valuesToNames = <T extends Values>(values: T): { [key in keyof T]: Name } => {
  const keys = Object.keys(values);

  const names = {} as Values;

  for (const key of keys) {
    names[key] = `--${key}`;
  }

  return names as { [key in keyof T]: Name };
};

export const valuesToVariables = <T extends Values>(values: T): { [key in keyof T]: Variable } => {
  const keys = Object.keys(values);

  const variables = {} as Values;

  for (const key of keys) {
    variables[key] = `var(--${key})`;
  }

  return variables as { [key in keyof T]: Variable };
};
