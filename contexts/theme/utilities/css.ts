import { Names, Values } from "../types";

export const makeCustomPropertyDefinitions = (values: Values, names: Names) => {
  return Object.entries(values)
    .map(([key, value]) => `${names[key]}: ${value};`)
    .join("");
};

export const makeCustomPropertyDefinitionsStructured = (values: Values, names: Names): { [key: Names[string]]: Values[string] } => {
  const result = {} as { [key: Names[string]]: Values[string] };

  for (const [key, value] of Object.entries(values)) {
    result[names[key]] = value;
  }

  return result;
};
