/**
 * CSS の min() 関数と同じような挙動をするもの
 */
export const min = (...args: number[]) => {
  return args.reduce((previous, current) => {
    if (previous < current) {
      return previous;
    } else {
      return current;
    }
  });
};

/**
 * CSS の clamp() 関数と同じような挙動をするもの
 */
export const clamp = (min: number, val: number, max: number) => {
  if (val < min) {
    return min;
  } else if (max < val) {
    return max;
  } else {
    return val;
  }
};

/**
 * 小数点以下第3位で四捨五入する
 */
export const roundToThirdDigit = (val: number) => {
  return Math.round(val * 1000) / 1000;
};
