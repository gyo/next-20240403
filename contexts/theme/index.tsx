import { useContext } from "react";
import { ThemeDispatchContext, ThemeStateContext } from "./context";
import { makeCustomPropertyDefinitions, makeCustomPropertyDefinitionsStructured } from "./utilities/css";

export const useThemeCustomPropertyDefinitions = () => {
  const theme = useContext(ThemeStateContext);
  return makeCustomPropertyDefinitions(theme.allValues, theme.allNames);
};

export const useThemeCustomPropertyDefinitionsStructured = () => {
  const theme = useContext(ThemeStateContext);
  return makeCustomPropertyDefinitionsStructured(theme.allValues, theme.allNames);
};

export const useTheme = () => {
  const theme = useContext(ThemeStateContext);
  return theme.variables;
};

export const useThemeStructured = () => {
  const theme = useContext(ThemeStateContext);
  return theme.structuredTheme;
};

export const useThemeDispatch = () => {
  return useContext(ThemeDispatchContext);
};
