import { getDynamicChromaSpread, getGrayChromaSpread } from "./utilities/chroma";
import { valuesToNames, valuesToVariables } from "./utilities/convert";
import { getDynamicLightnessSpread, getStaticLightnessSpread } from "./utilities/lightness";
import {
  getBlueSpread,
  getGrayOnBlueSpread,
  getGrayOnGraySpread,
  getGrayOnRedSpread,
  getGrayOnThemeSpread,
  getGraySpread,
  getRedSpread,
  getThemeOnGraySpread,
  getThemeSpread,
} from "./utilities/oklch-spread";

const defineColorAtom = (themeLightness: number, themeChroma: number, themeHue: number) => {
  const dynamicLightnessSpread = getDynamicLightnessSpread(themeLightness);
  const staticLightnessSpread = getStaticLightnessSpread();
  const dynamicChromaSpread = getDynamicChromaSpread(themeChroma);
  const grayChromaSpread = getGrayChromaSpread(themeChroma);
  const redHue = 30;
  const blueHue = 270;
  const grayHue = themeHue;

  const graySpread = getGraySpread(staticLightnessSpread, grayChromaSpread, grayHue);
  const redSpread = getRedSpread(dynamicLightnessSpread, dynamicChromaSpread, redHue);
  const blueSpread = getBlueSpread(dynamicLightnessSpread, dynamicChromaSpread, blueHue);
  const themeSpread = getThemeSpread(dynamicLightnessSpread, dynamicChromaSpread, themeHue);
  const grayOnGraySpread = getGrayOnGraySpread(graySpread, graySpread);
  const grayOnRedSpread = getGrayOnRedSpread(redSpread, graySpread);
  const grayOnBlueSpread = getGrayOnBlueSpread(blueSpread, graySpread);
  const grayOnThemeSpread = getGrayOnThemeSpread(themeSpread, graySpread);
  const themeOnGraySpread = getThemeOnGraySpread(graySpread, themeSpread);

  const values = {
    grayLightness01: `${graySpread.gray01.lightness}%`,
    grayLightness02: `${graySpread.gray02.lightness}%`,
    grayLightness03: `${graySpread.gray03.lightness}%`,
    grayLightness04: `${graySpread.gray04.lightness}%`,
    grayLightness05: `${graySpread.gray05.lightness}%`,
    grayLightness06: `${graySpread.gray06.lightness}%`,
    grayLightness07: `${graySpread.gray07.lightness}%`,
    grayLightness08: `${graySpread.gray08.lightness}%`,
    grayLightness09: `${graySpread.gray09.lightness}%`,
    grayLightness10: `${graySpread.gray10.lightness}%`,
    grayLightness11: `${graySpread.gray11.lightness}%`,
    grayLightness12: `${graySpread.gray12.lightness}%`,
    grayLightness13: `${graySpread.gray13.lightness}%`,
    grayLightness14: `${graySpread.gray14.lightness}%`,
    grayLightness15: `${graySpread.gray15.lightness}%`,
    grayLightness16: `${graySpread.gray16.lightness}%`,
    grayLightness17: `${graySpread.gray17.lightness}%`,
    grayLightness18: `${graySpread.gray18.lightness}%`,
    grayLightness19: `${graySpread.gray19.lightness}%`,
    grayLightness20: `${graySpread.gray20.lightness}%`,
    grayChroma01: `${graySpread.gray01.chroma}`,
    grayChroma02: `${graySpread.gray02.chroma}`,
    grayChroma03: `${graySpread.gray03.chroma}`,
    grayChroma04: `${graySpread.gray04.chroma}`,
    grayChroma05: `${graySpread.gray05.chroma}`,
    grayChroma06: `${graySpread.gray06.chroma}`,
    grayChroma07: `${graySpread.gray07.chroma}`,
    grayChroma08: `${graySpread.gray08.chroma}`,
    grayChroma09: `${graySpread.gray09.chroma}`,
    grayChroma10: `${graySpread.gray10.chroma}`,
    grayChroma11: `${graySpread.gray11.chroma}`,
    grayChroma12: `${graySpread.gray12.chroma}`,
    grayChroma13: `${graySpread.gray13.chroma}`,
    grayChroma14: `${graySpread.gray14.chroma}`,
    grayChroma15: `${graySpread.gray15.chroma}`,
    grayChroma16: `${graySpread.gray16.chroma}`,
    grayChroma17: `${graySpread.gray17.chroma}`,
    grayChroma18: `${graySpread.gray18.chroma}`,
    grayChroma19: `${graySpread.gray19.chroma}`,
    grayChroma20: `${graySpread.gray20.chroma}`,
    grayHue01: `${graySpread.gray01.hue}`,
    grayHue02: `${graySpread.gray02.hue}`,
    grayHue03: `${graySpread.gray03.hue}`,
    grayHue04: `${graySpread.gray04.hue}`,
    grayHue05: `${graySpread.gray05.hue}`,
    grayHue06: `${graySpread.gray06.hue}`,
    grayHue07: `${graySpread.gray07.hue}`,
    grayHue08: `${graySpread.gray08.hue}`,
    grayHue09: `${graySpread.gray09.hue}`,
    grayHue10: `${graySpread.gray10.hue}`,
    grayHue11: `${graySpread.gray11.hue}`,
    grayHue12: `${graySpread.gray12.hue}`,
    grayHue13: `${graySpread.gray13.hue}`,
    grayHue14: `${graySpread.gray14.hue}`,
    grayHue15: `${graySpread.gray15.hue}`,
    grayHue16: `${graySpread.gray16.hue}`,
    grayHue17: `${graySpread.gray17.hue}`,
    grayHue18: `${graySpread.gray18.hue}`,
    grayHue19: `${graySpread.gray19.hue}`,
    grayHue20: `${graySpread.gray20.hue}`,
    redLightness01: `${redSpread.red01.lightness}%`,
    redLightness02: `${redSpread.red02.lightness}%`,
    redLightness03: `${redSpread.red03.lightness}%`,
    redLightness04: `${redSpread.red04.lightness}%`,
    redLightness05: `${redSpread.red05.lightness}%`,
    redLightness06: `${redSpread.red06.lightness}%`,
    redLightness07: `${redSpread.red07.lightness}%`,
    redLightness08: `${redSpread.red08.lightness}%`,
    redLightness09: `${redSpread.red09.lightness}%`,
    redLightness10: `${redSpread.red10.lightness}%`,
    redLightness11: `${redSpread.red11.lightness}%`,
    redLightness12: `${redSpread.red12.lightness}%`,
    redLightness13: `${redSpread.red13.lightness}%`,
    redLightness14: `${redSpread.red14.lightness}%`,
    redLightness15: `${redSpread.red15.lightness}%`,
    redLightness16: `${redSpread.red16.lightness}%`,
    redLightness17: `${redSpread.red17.lightness}%`,
    redLightness18: `${redSpread.red18.lightness}%`,
    redLightness19: `${redSpread.red19.lightness}%`,
    redLightness20: `${redSpread.red20.lightness}%`,
    redChroma01: `${redSpread.red01.chroma}`,
    redChroma02: `${redSpread.red02.chroma}`,
    redChroma03: `${redSpread.red03.chroma}`,
    redChroma04: `${redSpread.red04.chroma}`,
    redChroma05: `${redSpread.red05.chroma}`,
    redChroma06: `${redSpread.red06.chroma}`,
    redChroma07: `${redSpread.red07.chroma}`,
    redChroma08: `${redSpread.red08.chroma}`,
    redChroma09: `${redSpread.red09.chroma}`,
    redChroma10: `${redSpread.red10.chroma}`,
    redChroma11: `${redSpread.red11.chroma}`,
    redChroma12: `${redSpread.red12.chroma}`,
    redChroma13: `${redSpread.red13.chroma}`,
    redChroma14: `${redSpread.red14.chroma}`,
    redChroma15: `${redSpread.red15.chroma}`,
    redChroma16: `${redSpread.red16.chroma}`,
    redChroma17: `${redSpread.red17.chroma}`,
    redChroma18: `${redSpread.red18.chroma}`,
    redChroma19: `${redSpread.red19.chroma}`,
    redChroma20: `${redSpread.red20.chroma}`,
    redHue01: `${redSpread.red01.hue}`,
    redHue02: `${redSpread.red02.hue}`,
    redHue03: `${redSpread.red03.hue}`,
    redHue04: `${redSpread.red04.hue}`,
    redHue05: `${redSpread.red05.hue}`,
    redHue06: `${redSpread.red06.hue}`,
    redHue07: `${redSpread.red07.hue}`,
    redHue08: `${redSpread.red08.hue}`,
    redHue09: `${redSpread.red09.hue}`,
    redHue10: `${redSpread.red10.hue}`,
    redHue11: `${redSpread.red11.hue}`,
    redHue12: `${redSpread.red12.hue}`,
    redHue13: `${redSpread.red13.hue}`,
    redHue14: `${redSpread.red14.hue}`,
    redHue15: `${redSpread.red15.hue}`,
    redHue16: `${redSpread.red16.hue}`,
    redHue17: `${redSpread.red17.hue}`,
    redHue18: `${redSpread.red18.hue}`,
    redHue19: `${redSpread.red19.hue}`,
    redHue20: `${redSpread.red20.hue}`,
    blueLightness01: `${blueSpread.blue01.lightness}%`,
    blueLightness02: `${blueSpread.blue02.lightness}%`,
    blueLightness03: `${blueSpread.blue03.lightness}%`,
    blueLightness04: `${blueSpread.blue04.lightness}%`,
    blueLightness05: `${blueSpread.blue05.lightness}%`,
    blueLightness06: `${blueSpread.blue06.lightness}%`,
    blueLightness07: `${blueSpread.blue07.lightness}%`,
    blueLightness08: `${blueSpread.blue08.lightness}%`,
    blueLightness09: `${blueSpread.blue09.lightness}%`,
    blueLightness10: `${blueSpread.blue10.lightness}%`,
    blueLightness11: `${blueSpread.blue11.lightness}%`,
    blueLightness12: `${blueSpread.blue12.lightness}%`,
    blueLightness13: `${blueSpread.blue13.lightness}%`,
    blueLightness14: `${blueSpread.blue14.lightness}%`,
    blueLightness15: `${blueSpread.blue15.lightness}%`,
    blueLightness16: `${blueSpread.blue16.lightness}%`,
    blueLightness17: `${blueSpread.blue17.lightness}%`,
    blueLightness18: `${blueSpread.blue18.lightness}%`,
    blueLightness19: `${blueSpread.blue19.lightness}%`,
    blueLightness20: `${blueSpread.blue20.lightness}%`,
    blueChroma01: `${blueSpread.blue01.chroma}`,
    blueChroma02: `${blueSpread.blue02.chroma}`,
    blueChroma03: `${blueSpread.blue03.chroma}`,
    blueChroma04: `${blueSpread.blue04.chroma}`,
    blueChroma05: `${blueSpread.blue05.chroma}`,
    blueChroma06: `${blueSpread.blue06.chroma}`,
    blueChroma07: `${blueSpread.blue07.chroma}`,
    blueChroma08: `${blueSpread.blue08.chroma}`,
    blueChroma09: `${blueSpread.blue09.chroma}`,
    blueChroma10: `${blueSpread.blue10.chroma}`,
    blueChroma11: `${blueSpread.blue11.chroma}`,
    blueChroma12: `${blueSpread.blue12.chroma}`,
    blueChroma13: `${blueSpread.blue13.chroma}`,
    blueChroma14: `${blueSpread.blue14.chroma}`,
    blueChroma15: `${blueSpread.blue15.chroma}`,
    blueChroma16: `${blueSpread.blue16.chroma}`,
    blueChroma17: `${blueSpread.blue17.chroma}`,
    blueChroma18: `${blueSpread.blue18.chroma}`,
    blueChroma19: `${blueSpread.blue19.chroma}`,
    blueChroma20: `${blueSpread.blue20.chroma}`,
    blueHue01: `${blueSpread.blue01.hue}`,
    blueHue02: `${blueSpread.blue02.hue}`,
    blueHue03: `${blueSpread.blue03.hue}`,
    blueHue04: `${blueSpread.blue04.hue}`,
    blueHue05: `${blueSpread.blue05.hue}`,
    blueHue06: `${blueSpread.blue06.hue}`,
    blueHue07: `${blueSpread.blue07.hue}`,
    blueHue08: `${blueSpread.blue08.hue}`,
    blueHue09: `${blueSpread.blue09.hue}`,
    blueHue10: `${blueSpread.blue10.hue}`,
    blueHue11: `${blueSpread.blue11.hue}`,
    blueHue12: `${blueSpread.blue12.hue}`,
    blueHue13: `${blueSpread.blue13.hue}`,
    blueHue14: `${blueSpread.blue14.hue}`,
    blueHue15: `${blueSpread.blue15.hue}`,
    blueHue16: `${blueSpread.blue16.hue}`,
    blueHue17: `${blueSpread.blue17.hue}`,
    blueHue18: `${blueSpread.blue18.hue}`,
    blueHue19: `${blueSpread.blue19.hue}`,
    blueHue20: `${blueSpread.blue20.hue}`,
    themeLightness01: `${themeSpread.theme01.lightness}%`,
    themeLightness02: `${themeSpread.theme02.lightness}%`,
    themeLightness03: `${themeSpread.theme03.lightness}%`,
    themeLightness04: `${themeSpread.theme04.lightness}%`,
    themeLightness05: `${themeSpread.theme05.lightness}%`,
    themeLightness06: `${themeSpread.theme06.lightness}%`,
    themeLightness07: `${themeSpread.theme07.lightness}%`,
    themeLightness08: `${themeSpread.theme08.lightness}%`,
    themeLightness09: `${themeSpread.theme09.lightness}%`,
    themeLightness10: `${themeSpread.theme10.lightness}%`,
    themeLightness11: `${themeSpread.theme11.lightness}%`,
    themeLightness12: `${themeSpread.theme12.lightness}%`,
    themeLightness13: `${themeSpread.theme13.lightness}%`,
    themeLightness14: `${themeSpread.theme14.lightness}%`,
    themeLightness15: `${themeSpread.theme15.lightness}%`,
    themeLightness16: `${themeSpread.theme16.lightness}%`,
    themeLightness17: `${themeSpread.theme17.lightness}%`,
    themeLightness18: `${themeSpread.theme18.lightness}%`,
    themeLightness19: `${themeSpread.theme19.lightness}%`,
    themeLightness20: `${themeSpread.theme20.lightness}%`,
    themeChroma01: `${themeSpread.theme01.chroma}`,
    themeChroma02: `${themeSpread.theme02.chroma}`,
    themeChroma03: `${themeSpread.theme03.chroma}`,
    themeChroma04: `${themeSpread.theme04.chroma}`,
    themeChroma05: `${themeSpread.theme05.chroma}`,
    themeChroma06: `${themeSpread.theme06.chroma}`,
    themeChroma07: `${themeSpread.theme07.chroma}`,
    themeChroma08: `${themeSpread.theme08.chroma}`,
    themeChroma09: `${themeSpread.theme09.chroma}`,
    themeChroma10: `${themeSpread.theme10.chroma}`,
    themeChroma11: `${themeSpread.theme11.chroma}`,
    themeChroma12: `${themeSpread.theme12.chroma}`,
    themeChroma13: `${themeSpread.theme13.chroma}`,
    themeChroma14: `${themeSpread.theme14.chroma}`,
    themeChroma15: `${themeSpread.theme15.chroma}`,
    themeChroma16: `${themeSpread.theme16.chroma}`,
    themeChroma17: `${themeSpread.theme17.chroma}`,
    themeChroma18: `${themeSpread.theme18.chroma}`,
    themeChroma19: `${themeSpread.theme19.chroma}`,
    themeChroma20: `${themeSpread.theme20.chroma}`,
    themeHue01: `${themeSpread.theme01.hue}`,
    themeHue02: `${themeSpread.theme02.hue}`,
    themeHue03: `${themeSpread.theme03.hue}`,
    themeHue04: `${themeSpread.theme04.hue}`,
    themeHue05: `${themeSpread.theme05.hue}`,
    themeHue06: `${themeSpread.theme06.hue}`,
    themeHue07: `${themeSpread.theme07.hue}`,
    themeHue08: `${themeSpread.theme08.hue}`,
    themeHue09: `${themeSpread.theme09.hue}`,
    themeHue10: `${themeSpread.theme10.hue}`,
    themeHue11: `${themeSpread.theme11.hue}`,
    themeHue12: `${themeSpread.theme12.hue}`,
    themeHue13: `${themeSpread.theme13.hue}`,
    themeHue14: `${themeSpread.theme14.hue}`,
    themeHue15: `${themeSpread.theme15.hue}`,
    themeHue16: `${themeSpread.theme16.hue}`,
    themeHue17: `${themeSpread.theme17.hue}`,
    themeHue18: `${themeSpread.theme18.hue}`,
    themeHue19: `${themeSpread.theme19.hue}`,
    themeHue20: `${themeSpread.theme20.hue}`,
    grayOnGrayLightness01: `${grayOnGraySpread.grayOnGray01.lightness}%`,
    grayOnGrayLightness02: `${grayOnGraySpread.grayOnGray02.lightness}%`,
    grayOnGrayLightness03: `${grayOnGraySpread.grayOnGray03.lightness}%`,
    grayOnGrayLightness04: `${grayOnGraySpread.grayOnGray04.lightness}%`,
    grayOnGrayLightness05: `${grayOnGraySpread.grayOnGray05.lightness}%`,
    grayOnGrayLightness06: `${grayOnGraySpread.grayOnGray06.lightness}%`,
    grayOnGrayLightness07: `${grayOnGraySpread.grayOnGray07.lightness}%`,
    grayOnGrayLightness08: `${grayOnGraySpread.grayOnGray08.lightness}%`,
    grayOnGrayLightness09: `${grayOnGraySpread.grayOnGray09.lightness}%`,
    grayOnGrayLightness10: `${grayOnGraySpread.grayOnGray10.lightness}%`,
    grayOnGrayLightness11: `${grayOnGraySpread.grayOnGray11.lightness}%`,
    grayOnGrayLightness12: `${grayOnGraySpread.grayOnGray12.lightness}%`,
    grayOnGrayLightness13: `${grayOnGraySpread.grayOnGray13.lightness}%`,
    grayOnGrayLightness14: `${grayOnGraySpread.grayOnGray14.lightness}%`,
    grayOnGrayLightness15: `${grayOnGraySpread.grayOnGray15.lightness}%`,
    grayOnGrayLightness16: `${grayOnGraySpread.grayOnGray16.lightness}%`,
    grayOnGrayLightness17: `${grayOnGraySpread.grayOnGray17.lightness}%`,
    grayOnGrayLightness18: `${grayOnGraySpread.grayOnGray18.lightness}%`,
    grayOnGrayLightness19: `${grayOnGraySpread.grayOnGray19.lightness}%`,
    grayOnGrayLightness20: `${grayOnGraySpread.grayOnGray20.lightness}%`,
    grayOnGrayChroma01: `${grayOnGraySpread.grayOnGray01.chroma}`,
    grayOnGrayChroma02: `${grayOnGraySpread.grayOnGray02.chroma}`,
    grayOnGrayChroma03: `${grayOnGraySpread.grayOnGray03.chroma}`,
    grayOnGrayChroma04: `${grayOnGraySpread.grayOnGray04.chroma}`,
    grayOnGrayChroma05: `${grayOnGraySpread.grayOnGray05.chroma}`,
    grayOnGrayChroma06: `${grayOnGraySpread.grayOnGray06.chroma}`,
    grayOnGrayChroma07: `${grayOnGraySpread.grayOnGray07.chroma}`,
    grayOnGrayChroma08: `${grayOnGraySpread.grayOnGray08.chroma}`,
    grayOnGrayChroma09: `${grayOnGraySpread.grayOnGray09.chroma}`,
    grayOnGrayChroma10: `${grayOnGraySpread.grayOnGray10.chroma}`,
    grayOnGrayChroma11: `${grayOnGraySpread.grayOnGray11.chroma}`,
    grayOnGrayChroma12: `${grayOnGraySpread.grayOnGray12.chroma}`,
    grayOnGrayChroma13: `${grayOnGraySpread.grayOnGray13.chroma}`,
    grayOnGrayChroma14: `${grayOnGraySpread.grayOnGray14.chroma}`,
    grayOnGrayChroma15: `${grayOnGraySpread.grayOnGray15.chroma}`,
    grayOnGrayChroma16: `${grayOnGraySpread.grayOnGray16.chroma}`,
    grayOnGrayChroma17: `${grayOnGraySpread.grayOnGray17.chroma}`,
    grayOnGrayChroma18: `${grayOnGraySpread.grayOnGray18.chroma}`,
    grayOnGrayChroma19: `${grayOnGraySpread.grayOnGray19.chroma}`,
    grayOnGrayChroma20: `${grayOnGraySpread.grayOnGray20.chroma}`,
    grayOnGrayHue01: `${grayOnGraySpread.grayOnGray01.hue}`,
    grayOnGrayHue02: `${grayOnGraySpread.grayOnGray02.hue}`,
    grayOnGrayHue03: `${grayOnGraySpread.grayOnGray03.hue}`,
    grayOnGrayHue04: `${grayOnGraySpread.grayOnGray04.hue}`,
    grayOnGrayHue05: `${grayOnGraySpread.grayOnGray05.hue}`,
    grayOnGrayHue06: `${grayOnGraySpread.grayOnGray06.hue}`,
    grayOnGrayHue07: `${grayOnGraySpread.grayOnGray07.hue}`,
    grayOnGrayHue08: `${grayOnGraySpread.grayOnGray08.hue}`,
    grayOnGrayHue09: `${grayOnGraySpread.grayOnGray09.hue}`,
    grayOnGrayHue10: `${grayOnGraySpread.grayOnGray10.hue}`,
    grayOnGrayHue11: `${grayOnGraySpread.grayOnGray11.hue}`,
    grayOnGrayHue12: `${grayOnGraySpread.grayOnGray12.hue}`,
    grayOnGrayHue13: `${grayOnGraySpread.grayOnGray13.hue}`,
    grayOnGrayHue14: `${grayOnGraySpread.grayOnGray14.hue}`,
    grayOnGrayHue15: `${grayOnGraySpread.grayOnGray15.hue}`,
    grayOnGrayHue16: `${grayOnGraySpread.grayOnGray16.hue}`,
    grayOnGrayHue17: `${grayOnGraySpread.grayOnGray17.hue}`,
    grayOnGrayHue18: `${grayOnGraySpread.grayOnGray18.hue}`,
    grayOnGrayHue19: `${grayOnGraySpread.grayOnGray19.hue}`,
    grayOnGrayHue20: `${grayOnGraySpread.grayOnGray20.hue}`,
    grayOnRedLightness01: `${grayOnRedSpread.grayOnRed01.lightness}%`,
    grayOnRedLightness02: `${grayOnRedSpread.grayOnRed02.lightness}%`,
    grayOnRedLightness03: `${grayOnRedSpread.grayOnRed03.lightness}%`,
    grayOnRedLightness04: `${grayOnRedSpread.grayOnRed04.lightness}%`,
    grayOnRedLightness05: `${grayOnRedSpread.grayOnRed05.lightness}%`,
    grayOnRedLightness06: `${grayOnRedSpread.grayOnRed06.lightness}%`,
    grayOnRedLightness07: `${grayOnRedSpread.grayOnRed07.lightness}%`,
    grayOnRedLightness08: `${grayOnRedSpread.grayOnRed08.lightness}%`,
    grayOnRedLightness09: `${grayOnRedSpread.grayOnRed09.lightness}%`,
    grayOnRedLightness10: `${grayOnRedSpread.grayOnRed10.lightness}%`,
    grayOnRedLightness11: `${grayOnRedSpread.grayOnRed11.lightness}%`,
    grayOnRedLightness12: `${grayOnRedSpread.grayOnRed12.lightness}%`,
    grayOnRedLightness13: `${grayOnRedSpread.grayOnRed13.lightness}%`,
    grayOnRedLightness14: `${grayOnRedSpread.grayOnRed14.lightness}%`,
    grayOnRedLightness15: `${grayOnRedSpread.grayOnRed15.lightness}%`,
    grayOnRedLightness16: `${grayOnRedSpread.grayOnRed16.lightness}%`,
    grayOnRedLightness17: `${grayOnRedSpread.grayOnRed17.lightness}%`,
    grayOnRedLightness18: `${grayOnRedSpread.grayOnRed18.lightness}%`,
    grayOnRedLightness19: `${grayOnRedSpread.grayOnRed19.lightness}%`,
    grayOnRedLightness20: `${grayOnRedSpread.grayOnRed20.lightness}%`,
    grayOnRedChroma01: `${grayOnRedSpread.grayOnRed01.chroma}`,
    grayOnRedChroma02: `${grayOnRedSpread.grayOnRed02.chroma}`,
    grayOnRedChroma03: `${grayOnRedSpread.grayOnRed03.chroma}`,
    grayOnRedChroma04: `${grayOnRedSpread.grayOnRed04.chroma}`,
    grayOnRedChroma05: `${grayOnRedSpread.grayOnRed05.chroma}`,
    grayOnRedChroma06: `${grayOnRedSpread.grayOnRed06.chroma}`,
    grayOnRedChroma07: `${grayOnRedSpread.grayOnRed07.chroma}`,
    grayOnRedChroma08: `${grayOnRedSpread.grayOnRed08.chroma}`,
    grayOnRedChroma09: `${grayOnRedSpread.grayOnRed09.chroma}`,
    grayOnRedChroma10: `${grayOnRedSpread.grayOnRed10.chroma}`,
    grayOnRedChroma11: `${grayOnRedSpread.grayOnRed11.chroma}`,
    grayOnRedChroma12: `${grayOnRedSpread.grayOnRed12.chroma}`,
    grayOnRedChroma13: `${grayOnRedSpread.grayOnRed13.chroma}`,
    grayOnRedChroma14: `${grayOnRedSpread.grayOnRed14.chroma}`,
    grayOnRedChroma15: `${grayOnRedSpread.grayOnRed15.chroma}`,
    grayOnRedChroma16: `${grayOnRedSpread.grayOnRed16.chroma}`,
    grayOnRedChroma17: `${grayOnRedSpread.grayOnRed17.chroma}`,
    grayOnRedChroma18: `${grayOnRedSpread.grayOnRed18.chroma}`,
    grayOnRedChroma19: `${grayOnRedSpread.grayOnRed19.chroma}`,
    grayOnRedChroma20: `${grayOnRedSpread.grayOnRed20.chroma}`,
    grayOnRedHue01: `${grayOnRedSpread.grayOnRed01.hue}`,
    grayOnRedHue02: `${grayOnRedSpread.grayOnRed02.hue}`,
    grayOnRedHue03: `${grayOnRedSpread.grayOnRed03.hue}`,
    grayOnRedHue04: `${grayOnRedSpread.grayOnRed04.hue}`,
    grayOnRedHue05: `${grayOnRedSpread.grayOnRed05.hue}`,
    grayOnRedHue06: `${grayOnRedSpread.grayOnRed06.hue}`,
    grayOnRedHue07: `${grayOnRedSpread.grayOnRed07.hue}`,
    grayOnRedHue08: `${grayOnRedSpread.grayOnRed08.hue}`,
    grayOnRedHue09: `${grayOnRedSpread.grayOnRed09.hue}`,
    grayOnRedHue10: `${grayOnRedSpread.grayOnRed10.hue}`,
    grayOnRedHue11: `${grayOnRedSpread.grayOnRed11.hue}`,
    grayOnRedHue12: `${grayOnRedSpread.grayOnRed12.hue}`,
    grayOnRedHue13: `${grayOnRedSpread.grayOnRed13.hue}`,
    grayOnRedHue14: `${grayOnRedSpread.grayOnRed14.hue}`,
    grayOnRedHue15: `${grayOnRedSpread.grayOnRed15.hue}`,
    grayOnRedHue16: `${grayOnRedSpread.grayOnRed16.hue}`,
    grayOnRedHue17: `${grayOnRedSpread.grayOnRed17.hue}`,
    grayOnRedHue18: `${grayOnRedSpread.grayOnRed18.hue}`,
    grayOnRedHue19: `${grayOnRedSpread.grayOnRed19.hue}`,
    grayOnRedHue20: `${grayOnRedSpread.grayOnRed20.hue}`,
    grayOnBlueLightness01: `${grayOnBlueSpread.grayOnBlue01.lightness}%`,
    grayOnBlueLightness02: `${grayOnBlueSpread.grayOnBlue02.lightness}%`,
    grayOnBlueLightness03: `${grayOnBlueSpread.grayOnBlue03.lightness}%`,
    grayOnBlueLightness04: `${grayOnBlueSpread.grayOnBlue04.lightness}%`,
    grayOnBlueLightness05: `${grayOnBlueSpread.grayOnBlue05.lightness}%`,
    grayOnBlueLightness06: `${grayOnBlueSpread.grayOnBlue06.lightness}%`,
    grayOnBlueLightness07: `${grayOnBlueSpread.grayOnBlue07.lightness}%`,
    grayOnBlueLightness08: `${grayOnBlueSpread.grayOnBlue08.lightness}%`,
    grayOnBlueLightness09: `${grayOnBlueSpread.grayOnBlue09.lightness}%`,
    grayOnBlueLightness10: `${grayOnBlueSpread.grayOnBlue10.lightness}%`,
    grayOnBlueLightness11: `${grayOnBlueSpread.grayOnBlue11.lightness}%`,
    grayOnBlueLightness12: `${grayOnBlueSpread.grayOnBlue12.lightness}%`,
    grayOnBlueLightness13: `${grayOnBlueSpread.grayOnBlue13.lightness}%`,
    grayOnBlueLightness14: `${grayOnBlueSpread.grayOnBlue14.lightness}%`,
    grayOnBlueLightness15: `${grayOnBlueSpread.grayOnBlue15.lightness}%`,
    grayOnBlueLightness16: `${grayOnBlueSpread.grayOnBlue16.lightness}%`,
    grayOnBlueLightness17: `${grayOnBlueSpread.grayOnBlue17.lightness}%`,
    grayOnBlueLightness18: `${grayOnBlueSpread.grayOnBlue18.lightness}%`,
    grayOnBlueLightness19: `${grayOnBlueSpread.grayOnBlue19.lightness}%`,
    grayOnBlueLightness20: `${grayOnBlueSpread.grayOnBlue20.lightness}%`,
    grayOnBlueChroma01: `${grayOnBlueSpread.grayOnBlue01.chroma}`,
    grayOnBlueChroma02: `${grayOnBlueSpread.grayOnBlue02.chroma}`,
    grayOnBlueChroma03: `${grayOnBlueSpread.grayOnBlue03.chroma}`,
    grayOnBlueChroma04: `${grayOnBlueSpread.grayOnBlue04.chroma}`,
    grayOnBlueChroma05: `${grayOnBlueSpread.grayOnBlue05.chroma}`,
    grayOnBlueChroma06: `${grayOnBlueSpread.grayOnBlue06.chroma}`,
    grayOnBlueChroma07: `${grayOnBlueSpread.grayOnBlue07.chroma}`,
    grayOnBlueChroma08: `${grayOnBlueSpread.grayOnBlue08.chroma}`,
    grayOnBlueChroma09: `${grayOnBlueSpread.grayOnBlue09.chroma}`,
    grayOnBlueChroma10: `${grayOnBlueSpread.grayOnBlue10.chroma}`,
    grayOnBlueChroma11: `${grayOnBlueSpread.grayOnBlue11.chroma}`,
    grayOnBlueChroma12: `${grayOnBlueSpread.grayOnBlue12.chroma}`,
    grayOnBlueChroma13: `${grayOnBlueSpread.grayOnBlue13.chroma}`,
    grayOnBlueChroma14: `${grayOnBlueSpread.grayOnBlue14.chroma}`,
    grayOnBlueChroma15: `${grayOnBlueSpread.grayOnBlue15.chroma}`,
    grayOnBlueChroma16: `${grayOnBlueSpread.grayOnBlue16.chroma}`,
    grayOnBlueChroma17: `${grayOnBlueSpread.grayOnBlue17.chroma}`,
    grayOnBlueChroma18: `${grayOnBlueSpread.grayOnBlue18.chroma}`,
    grayOnBlueChroma19: `${grayOnBlueSpread.grayOnBlue19.chroma}`,
    grayOnBlueChroma20: `${grayOnBlueSpread.grayOnBlue20.chroma}`,
    grayOnBlueHue01: `${grayOnBlueSpread.grayOnBlue01.hue}`,
    grayOnBlueHue02: `${grayOnBlueSpread.grayOnBlue02.hue}`,
    grayOnBlueHue03: `${grayOnBlueSpread.grayOnBlue03.hue}`,
    grayOnBlueHue04: `${grayOnBlueSpread.grayOnBlue04.hue}`,
    grayOnBlueHue05: `${grayOnBlueSpread.grayOnBlue05.hue}`,
    grayOnBlueHue06: `${grayOnBlueSpread.grayOnBlue06.hue}`,
    grayOnBlueHue07: `${grayOnBlueSpread.grayOnBlue07.hue}`,
    grayOnBlueHue08: `${grayOnBlueSpread.grayOnBlue08.hue}`,
    grayOnBlueHue09: `${grayOnBlueSpread.grayOnBlue09.hue}`,
    grayOnBlueHue10: `${grayOnBlueSpread.grayOnBlue10.hue}`,
    grayOnBlueHue11: `${grayOnBlueSpread.grayOnBlue11.hue}`,
    grayOnBlueHue12: `${grayOnBlueSpread.grayOnBlue12.hue}`,
    grayOnBlueHue13: `${grayOnBlueSpread.grayOnBlue13.hue}`,
    grayOnBlueHue14: `${grayOnBlueSpread.grayOnBlue14.hue}`,
    grayOnBlueHue15: `${grayOnBlueSpread.grayOnBlue15.hue}`,
    grayOnBlueHue16: `${grayOnBlueSpread.grayOnBlue16.hue}`,
    grayOnBlueHue17: `${grayOnBlueSpread.grayOnBlue17.hue}`,
    grayOnBlueHue18: `${grayOnBlueSpread.grayOnBlue18.hue}`,
    grayOnBlueHue19: `${grayOnBlueSpread.grayOnBlue19.hue}`,
    grayOnBlueHue20: `${grayOnBlueSpread.grayOnBlue20.hue}`,
    grayOnThemeLightness01: `${grayOnThemeSpread.grayOnTheme01.lightness}%`,
    grayOnThemeLightness02: `${grayOnThemeSpread.grayOnTheme02.lightness}%`,
    grayOnThemeLightness03: `${grayOnThemeSpread.grayOnTheme03.lightness}%`,
    grayOnThemeLightness04: `${grayOnThemeSpread.grayOnTheme04.lightness}%`,
    grayOnThemeLightness05: `${grayOnThemeSpread.grayOnTheme05.lightness}%`,
    grayOnThemeLightness06: `${grayOnThemeSpread.grayOnTheme06.lightness}%`,
    grayOnThemeLightness07: `${grayOnThemeSpread.grayOnTheme07.lightness}%`,
    grayOnThemeLightness08: `${grayOnThemeSpread.grayOnTheme08.lightness}%`,
    grayOnThemeLightness09: `${grayOnThemeSpread.grayOnTheme09.lightness}%`,
    grayOnThemeLightness10: `${grayOnThemeSpread.grayOnTheme10.lightness}%`,
    grayOnThemeLightness11: `${grayOnThemeSpread.grayOnTheme11.lightness}%`,
    grayOnThemeLightness12: `${grayOnThemeSpread.grayOnTheme12.lightness}%`,
    grayOnThemeLightness13: `${grayOnThemeSpread.grayOnTheme13.lightness}%`,
    grayOnThemeLightness14: `${grayOnThemeSpread.grayOnTheme14.lightness}%`,
    grayOnThemeLightness15: `${grayOnThemeSpread.grayOnTheme15.lightness}%`,
    grayOnThemeLightness16: `${grayOnThemeSpread.grayOnTheme16.lightness}%`,
    grayOnThemeLightness17: `${grayOnThemeSpread.grayOnTheme17.lightness}%`,
    grayOnThemeLightness18: `${grayOnThemeSpread.grayOnTheme18.lightness}%`,
    grayOnThemeLightness19: `${grayOnThemeSpread.grayOnTheme19.lightness}%`,
    grayOnThemeLightness20: `${grayOnThemeSpread.grayOnTheme20.lightness}%`,
    grayOnThemeChroma01: `${grayOnThemeSpread.grayOnTheme01.chroma}`,
    grayOnThemeChroma02: `${grayOnThemeSpread.grayOnTheme02.chroma}`,
    grayOnThemeChroma03: `${grayOnThemeSpread.grayOnTheme03.chroma}`,
    grayOnThemeChroma04: `${grayOnThemeSpread.grayOnTheme04.chroma}`,
    grayOnThemeChroma05: `${grayOnThemeSpread.grayOnTheme05.chroma}`,
    grayOnThemeChroma06: `${grayOnThemeSpread.grayOnTheme06.chroma}`,
    grayOnThemeChroma07: `${grayOnThemeSpread.grayOnTheme07.chroma}`,
    grayOnThemeChroma08: `${grayOnThemeSpread.grayOnTheme08.chroma}`,
    grayOnThemeChroma09: `${grayOnThemeSpread.grayOnTheme09.chroma}`,
    grayOnThemeChroma10: `${grayOnThemeSpread.grayOnTheme10.chroma}`,
    grayOnThemeChroma11: `${grayOnThemeSpread.grayOnTheme11.chroma}`,
    grayOnThemeChroma12: `${grayOnThemeSpread.grayOnTheme12.chroma}`,
    grayOnThemeChroma13: `${grayOnThemeSpread.grayOnTheme13.chroma}`,
    grayOnThemeChroma14: `${grayOnThemeSpread.grayOnTheme14.chroma}`,
    grayOnThemeChroma15: `${grayOnThemeSpread.grayOnTheme15.chroma}`,
    grayOnThemeChroma16: `${grayOnThemeSpread.grayOnTheme16.chroma}`,
    grayOnThemeChroma17: `${grayOnThemeSpread.grayOnTheme17.chroma}`,
    grayOnThemeChroma18: `${grayOnThemeSpread.grayOnTheme18.chroma}`,
    grayOnThemeChroma19: `${grayOnThemeSpread.grayOnTheme19.chroma}`,
    grayOnThemeChroma20: `${grayOnThemeSpread.grayOnTheme20.chroma}`,
    grayOnThemeHue01: `${grayOnThemeSpread.grayOnTheme01.hue}`,
    grayOnThemeHue02: `${grayOnThemeSpread.grayOnTheme02.hue}`,
    grayOnThemeHue03: `${grayOnThemeSpread.grayOnTheme03.hue}`,
    grayOnThemeHue04: `${grayOnThemeSpread.grayOnTheme04.hue}`,
    grayOnThemeHue05: `${grayOnThemeSpread.grayOnTheme05.hue}`,
    grayOnThemeHue06: `${grayOnThemeSpread.grayOnTheme06.hue}`,
    grayOnThemeHue07: `${grayOnThemeSpread.grayOnTheme07.hue}`,
    grayOnThemeHue08: `${grayOnThemeSpread.grayOnTheme08.hue}`,
    grayOnThemeHue09: `${grayOnThemeSpread.grayOnTheme09.hue}`,
    grayOnThemeHue10: `${grayOnThemeSpread.grayOnTheme10.hue}`,
    grayOnThemeHue11: `${grayOnThemeSpread.grayOnTheme11.hue}`,
    grayOnThemeHue12: `${grayOnThemeSpread.grayOnTheme12.hue}`,
    grayOnThemeHue13: `${grayOnThemeSpread.grayOnTheme13.hue}`,
    grayOnThemeHue14: `${grayOnThemeSpread.grayOnTheme14.hue}`,
    grayOnThemeHue15: `${grayOnThemeSpread.grayOnTheme15.hue}`,
    grayOnThemeHue16: `${grayOnThemeSpread.grayOnTheme16.hue}`,
    grayOnThemeHue17: `${grayOnThemeSpread.grayOnTheme17.hue}`,
    grayOnThemeHue18: `${grayOnThemeSpread.grayOnTheme18.hue}`,
    grayOnThemeHue19: `${grayOnThemeSpread.grayOnTheme19.hue}`,
    grayOnThemeHue20: `${grayOnThemeSpread.grayOnTheme20.hue}`,
    themeOnGrayLightness01: `${themeOnGraySpread.themeOnGray01.lightness}%`,
    themeOnGrayLightness02: `${themeOnGraySpread.themeOnGray02.lightness}%`,
    themeOnGrayLightness03: `${themeOnGraySpread.themeOnGray03.lightness}%`,
    themeOnGrayLightness04: `${themeOnGraySpread.themeOnGray04.lightness}%`,
    themeOnGrayLightness05: `${themeOnGraySpread.themeOnGray05.lightness}%`,
    themeOnGrayLightness06: `${themeOnGraySpread.themeOnGray06.lightness}%`,
    themeOnGrayLightness07: `${themeOnGraySpread.themeOnGray07.lightness}%`,
    themeOnGrayLightness08: `${themeOnGraySpread.themeOnGray08.lightness}%`,
    themeOnGrayLightness09: `${themeOnGraySpread.themeOnGray09.lightness}%`,
    themeOnGrayLightness10: `${themeOnGraySpread.themeOnGray10.lightness}%`,
    themeOnGrayLightness11: `${themeOnGraySpread.themeOnGray11.lightness}%`,
    themeOnGrayLightness12: `${themeOnGraySpread.themeOnGray12.lightness}%`,
    themeOnGrayLightness13: `${themeOnGraySpread.themeOnGray13.lightness}%`,
    themeOnGrayLightness14: `${themeOnGraySpread.themeOnGray14.lightness}%`,
    themeOnGrayLightness15: `${themeOnGraySpread.themeOnGray15.lightness}%`,
    themeOnGrayLightness16: `${themeOnGraySpread.themeOnGray16.lightness}%`,
    themeOnGrayLightness17: `${themeOnGraySpread.themeOnGray17.lightness}%`,
    themeOnGrayLightness18: `${themeOnGraySpread.themeOnGray18.lightness}%`,
    themeOnGrayLightness19: `${themeOnGraySpread.themeOnGray19.lightness}%`,
    themeOnGrayLightness20: `${themeOnGraySpread.themeOnGray20.lightness}%`,
    themeOnGrayChroma01: `${themeOnGraySpread.themeOnGray01.chroma}`,
    themeOnGrayChroma02: `${themeOnGraySpread.themeOnGray02.chroma}`,
    themeOnGrayChroma03: `${themeOnGraySpread.themeOnGray03.chroma}`,
    themeOnGrayChroma04: `${themeOnGraySpread.themeOnGray04.chroma}`,
    themeOnGrayChroma05: `${themeOnGraySpread.themeOnGray05.chroma}`,
    themeOnGrayChroma06: `${themeOnGraySpread.themeOnGray06.chroma}`,
    themeOnGrayChroma07: `${themeOnGraySpread.themeOnGray07.chroma}`,
    themeOnGrayChroma08: `${themeOnGraySpread.themeOnGray08.chroma}`,
    themeOnGrayChroma09: `${themeOnGraySpread.themeOnGray09.chroma}`,
    themeOnGrayChroma10: `${themeOnGraySpread.themeOnGray10.chroma}`,
    themeOnGrayChroma11: `${themeOnGraySpread.themeOnGray11.chroma}`,
    themeOnGrayChroma12: `${themeOnGraySpread.themeOnGray12.chroma}`,
    themeOnGrayChroma13: `${themeOnGraySpread.themeOnGray13.chroma}`,
    themeOnGrayChroma14: `${themeOnGraySpread.themeOnGray14.chroma}`,
    themeOnGrayChroma15: `${themeOnGraySpread.themeOnGray15.chroma}`,
    themeOnGrayChroma16: `${themeOnGraySpread.themeOnGray16.chroma}`,
    themeOnGrayChroma17: `${themeOnGraySpread.themeOnGray17.chroma}`,
    themeOnGrayChroma18: `${themeOnGraySpread.themeOnGray18.chroma}`,
    themeOnGrayChroma19: `${themeOnGraySpread.themeOnGray19.chroma}`,
    themeOnGrayChroma20: `${themeOnGraySpread.themeOnGray20.chroma}`,
    themeOnGrayHue01: `${themeOnGraySpread.themeOnGray01.hue}`,
    themeOnGrayHue02: `${themeOnGraySpread.themeOnGray02.hue}`,
    themeOnGrayHue03: `${themeOnGraySpread.themeOnGray03.hue}`,
    themeOnGrayHue04: `${themeOnGraySpread.themeOnGray04.hue}`,
    themeOnGrayHue05: `${themeOnGraySpread.themeOnGray05.hue}`,
    themeOnGrayHue06: `${themeOnGraySpread.themeOnGray06.hue}`,
    themeOnGrayHue07: `${themeOnGraySpread.themeOnGray07.hue}`,
    themeOnGrayHue08: `${themeOnGraySpread.themeOnGray08.hue}`,
    themeOnGrayHue09: `${themeOnGraySpread.themeOnGray09.hue}`,
    themeOnGrayHue10: `${themeOnGraySpread.themeOnGray10.hue}`,
    themeOnGrayHue11: `${themeOnGraySpread.themeOnGray11.hue}`,
    themeOnGrayHue12: `${themeOnGraySpread.themeOnGray12.hue}`,
    themeOnGrayHue13: `${themeOnGraySpread.themeOnGray13.hue}`,
    themeOnGrayHue14: `${themeOnGraySpread.themeOnGray14.hue}`,
    themeOnGrayHue15: `${themeOnGraySpread.themeOnGray15.hue}`,
    themeOnGrayHue16: `${themeOnGraySpread.themeOnGray16.hue}`,
    themeOnGrayHue17: `${themeOnGraySpread.themeOnGray17.hue}`,
    themeOnGrayHue18: `${themeOnGraySpread.themeOnGray18.hue}`,
    themeOnGrayHue19: `${themeOnGraySpread.themeOnGray19.hue}`,
    themeOnGrayHue20: `${themeOnGraySpread.themeOnGray20.hue}`,
  };

  return {
    values,
    names: valuesToNames(values),
    variables: valuesToVariables(values),
  };
};

const definePrimitiveColor = (v: ReturnType<typeof defineColorAtom>["variables"]) => {
  const gray = {
    gray01: `oklch(${v.grayLightness01} ${v.grayChroma01} ${v.grayHue01})`,
    gray02: `oklch(${v.grayLightness02} ${v.grayChroma02} ${v.grayHue02})`,
    gray03: `oklch(${v.grayLightness03} ${v.grayChroma03} ${v.grayHue03})`,
    gray04: `oklch(${v.grayLightness04} ${v.grayChroma04} ${v.grayHue04})`,
    gray05: `oklch(${v.grayLightness05} ${v.grayChroma05} ${v.grayHue05})`,
    gray06: `oklch(${v.grayLightness06} ${v.grayChroma06} ${v.grayHue06})`,
    gray07: `oklch(${v.grayLightness07} ${v.grayChroma07} ${v.grayHue07})`,
    gray08: `oklch(${v.grayLightness08} ${v.grayChroma08} ${v.grayHue08})`,
    gray09: `oklch(${v.grayLightness09} ${v.grayChroma09} ${v.grayHue09})`,
    gray10: `oklch(${v.grayLightness10} ${v.grayChroma10} ${v.grayHue10})`,
    gray11: `oklch(${v.grayLightness11} ${v.grayChroma11} ${v.grayHue11})`,
    gray12: `oklch(${v.grayLightness12} ${v.grayChroma12} ${v.grayHue12})`,
    gray13: `oklch(${v.grayLightness13} ${v.grayChroma13} ${v.grayHue13})`,
    gray14: `oklch(${v.grayLightness14} ${v.grayChroma14} ${v.grayHue14})`,
    gray15: `oklch(${v.grayLightness15} ${v.grayChroma15} ${v.grayHue15})`,
    gray16: `oklch(${v.grayLightness16} ${v.grayChroma16} ${v.grayHue16})`,
    gray17: `oklch(${v.grayLightness17} ${v.grayChroma17} ${v.grayHue17})`,
    gray18: `oklch(${v.grayLightness18} ${v.grayChroma18} ${v.grayHue18})`,
    gray19: `oklch(${v.grayLightness19} ${v.grayChroma19} ${v.grayHue19})`,
    gray20: `oklch(${v.grayLightness20} ${v.grayChroma20} ${v.grayHue20})`,
  };
  const grayAlpha = {
    grayAlpha01: `oklch(${v.grayLightness01} ${v.grayChroma01} ${v.grayHue01} / 0.5)`,
    grayAlpha02: `oklch(${v.grayLightness02} ${v.grayChroma02} ${v.grayHue02} / 0.5)`,
    grayAlpha03: `oklch(${v.grayLightness03} ${v.grayChroma03} ${v.grayHue03} / 0.5)`,
    grayAlpha04: `oklch(${v.grayLightness04} ${v.grayChroma04} ${v.grayHue04} / 0.5)`,
    grayAlpha05: `oklch(${v.grayLightness05} ${v.grayChroma05} ${v.grayHue05} / 0.5)`,
    grayAlpha06: `oklch(${v.grayLightness06} ${v.grayChroma06} ${v.grayHue06} / 0.5)`,
    grayAlpha07: `oklch(${v.grayLightness07} ${v.grayChroma07} ${v.grayHue07} / 0.5)`,
    grayAlpha08: `oklch(${v.grayLightness08} ${v.grayChroma08} ${v.grayHue08} / 0.5)`,
    grayAlpha09: `oklch(${v.grayLightness09} ${v.grayChroma09} ${v.grayHue09} / 0.5)`,
    grayAlpha10: `oklch(${v.grayLightness10} ${v.grayChroma10} ${v.grayHue10} / 0.5)`,
    grayAlpha11: `oklch(${v.grayLightness11} ${v.grayChroma11} ${v.grayHue11} / 0.5)`,
    grayAlpha12: `oklch(${v.grayLightness12} ${v.grayChroma12} ${v.grayHue12} / 0.5)`,
    grayAlpha13: `oklch(${v.grayLightness13} ${v.grayChroma13} ${v.grayHue13} / 0.5)`,
    grayAlpha14: `oklch(${v.grayLightness14} ${v.grayChroma14} ${v.grayHue14} / 0.5)`,
    grayAlpha15: `oklch(${v.grayLightness15} ${v.grayChroma15} ${v.grayHue15} / 0.5)`,
    grayAlpha16: `oklch(${v.grayLightness16} ${v.grayChroma16} ${v.grayHue16} / 0.5)`,
    grayAlpha17: `oklch(${v.grayLightness17} ${v.grayChroma17} ${v.grayHue17} / 0.5)`,
    grayAlpha18: `oklch(${v.grayLightness18} ${v.grayChroma18} ${v.grayHue18} / 0.5)`,
    grayAlpha19: `oklch(${v.grayLightness19} ${v.grayChroma19} ${v.grayHue19} / 0.5)`,
    grayAlpha20: `oklch(${v.grayLightness20} ${v.grayChroma20} ${v.grayHue20} / 0.5)`,
  };
  const red = {
    red01: `oklch(${v.redLightness01} ${v.redChroma01} ${v.redHue01})`,
    red02: `oklch(${v.redLightness02} ${v.redChroma02} ${v.redHue02})`,
    red03: `oklch(${v.redLightness03} ${v.redChroma03} ${v.redHue03})`,
    red04: `oklch(${v.redLightness04} ${v.redChroma04} ${v.redHue04})`,
    red05: `oklch(${v.redLightness05} ${v.redChroma05} ${v.redHue05})`,
    red06: `oklch(${v.redLightness06} ${v.redChroma06} ${v.redHue06})`,
    red07: `oklch(${v.redLightness07} ${v.redChroma07} ${v.redHue07})`,
    red08: `oklch(${v.redLightness08} ${v.redChroma08} ${v.redHue08})`,
    red09: `oklch(${v.redLightness09} ${v.redChroma09} ${v.redHue09})`,
    red10: `oklch(${v.redLightness10} ${v.redChroma10} ${v.redHue10})`,
    red11: `oklch(${v.redLightness11} ${v.redChroma11} ${v.redHue11})`,
    red12: `oklch(${v.redLightness12} ${v.redChroma12} ${v.redHue12})`,
    red13: `oklch(${v.redLightness13} ${v.redChroma13} ${v.redHue13})`,
    red14: `oklch(${v.redLightness14} ${v.redChroma14} ${v.redHue14})`,
    red15: `oklch(${v.redLightness15} ${v.redChroma15} ${v.redHue15})`,
    red16: `oklch(${v.redLightness16} ${v.redChroma16} ${v.redHue16})`,
    red17: `oklch(${v.redLightness17} ${v.redChroma17} ${v.redHue17})`,
    red18: `oklch(${v.redLightness18} ${v.redChroma18} ${v.redHue18})`,
    red19: `oklch(${v.redLightness19} ${v.redChroma19} ${v.redHue19})`,
    red20: `oklch(${v.redLightness20} ${v.redChroma20} ${v.redHue20})`,
  };
  const blue = {
    blue01: `oklch(${v.blueLightness01} ${v.blueChroma01} ${v.blueHue01})`,
    blue02: `oklch(${v.blueLightness02} ${v.blueChroma02} ${v.blueHue02})`,
    blue03: `oklch(${v.blueLightness03} ${v.blueChroma03} ${v.blueHue03})`,
    blue04: `oklch(${v.blueLightness04} ${v.blueChroma04} ${v.blueHue04})`,
    blue05: `oklch(${v.blueLightness05} ${v.blueChroma05} ${v.blueHue05})`,
    blue06: `oklch(${v.blueLightness06} ${v.blueChroma06} ${v.blueHue06})`,
    blue07: `oklch(${v.blueLightness07} ${v.blueChroma07} ${v.blueHue07})`,
    blue08: `oklch(${v.blueLightness08} ${v.blueChroma08} ${v.blueHue08})`,
    blue09: `oklch(${v.blueLightness09} ${v.blueChroma09} ${v.blueHue09})`,
    blue10: `oklch(${v.blueLightness10} ${v.blueChroma10} ${v.blueHue10})`,
    blue11: `oklch(${v.blueLightness11} ${v.blueChroma11} ${v.blueHue11})`,
    blue12: `oklch(${v.blueLightness12} ${v.blueChroma12} ${v.blueHue12})`,
    blue13: `oklch(${v.blueLightness13} ${v.blueChroma13} ${v.blueHue13})`,
    blue14: `oklch(${v.blueLightness14} ${v.blueChroma14} ${v.blueHue14})`,
    blue15: `oklch(${v.blueLightness15} ${v.blueChroma15} ${v.blueHue15})`,
    blue16: `oklch(${v.blueLightness16} ${v.blueChroma16} ${v.blueHue16})`,
    blue17: `oklch(${v.blueLightness17} ${v.blueChroma17} ${v.blueHue17})`,
    blue18: `oklch(${v.blueLightness18} ${v.blueChroma18} ${v.blueHue18})`,
    blue19: `oklch(${v.blueLightness19} ${v.blueChroma19} ${v.blueHue19})`,
    blue20: `oklch(${v.blueLightness20} ${v.blueChroma20} ${v.blueHue20})`,
  };
  const theme = {
    theme01: `oklch(${v.themeLightness01} ${v.themeChroma01} ${v.themeHue01})`,
    theme02: `oklch(${v.themeLightness02} ${v.themeChroma02} ${v.themeHue02})`,
    theme03: `oklch(${v.themeLightness03} ${v.themeChroma03} ${v.themeHue03})`,
    theme04: `oklch(${v.themeLightness04} ${v.themeChroma04} ${v.themeHue04})`,
    theme05: `oklch(${v.themeLightness05} ${v.themeChroma05} ${v.themeHue05})`,
    theme06: `oklch(${v.themeLightness06} ${v.themeChroma06} ${v.themeHue06})`,
    theme07: `oklch(${v.themeLightness07} ${v.themeChroma07} ${v.themeHue07})`,
    theme08: `oklch(${v.themeLightness08} ${v.themeChroma08} ${v.themeHue08})`,
    theme09: `oklch(${v.themeLightness09} ${v.themeChroma09} ${v.themeHue09})`,
    theme10: `oklch(${v.themeLightness10} ${v.themeChroma10} ${v.themeHue10})`,
    theme11: `oklch(${v.themeLightness11} ${v.themeChroma11} ${v.themeHue11})`,
    theme12: `oklch(${v.themeLightness12} ${v.themeChroma12} ${v.themeHue12})`,
    theme13: `oklch(${v.themeLightness13} ${v.themeChroma13} ${v.themeHue13})`,
    theme14: `oklch(${v.themeLightness14} ${v.themeChroma14} ${v.themeHue14})`,
    theme15: `oklch(${v.themeLightness15} ${v.themeChroma15} ${v.themeHue15})`,
    theme16: `oklch(${v.themeLightness16} ${v.themeChroma16} ${v.themeHue16})`,
    theme17: `oklch(${v.themeLightness17} ${v.themeChroma17} ${v.themeHue17})`,
    theme18: `oklch(${v.themeLightness18} ${v.themeChroma18} ${v.themeHue18})`,
    theme19: `oklch(${v.themeLightness19} ${v.themeChroma19} ${v.themeHue19})`,
    theme20: `oklch(${v.themeLightness20} ${v.themeChroma20} ${v.themeHue20})`,
  };
  const grayOnGray = {
    grayOnGray01: `oklch(${v.grayOnGrayLightness01} ${v.grayOnGrayChroma01} ${v.grayOnGrayHue01})`,
    grayOnGray02: `oklch(${v.grayOnGrayLightness02} ${v.grayOnGrayChroma02} ${v.grayOnGrayHue02})`,
    grayOnGray03: `oklch(${v.grayOnGrayLightness03} ${v.grayOnGrayChroma03} ${v.grayOnGrayHue03})`,
    grayOnGray04: `oklch(${v.grayOnGrayLightness04} ${v.grayOnGrayChroma04} ${v.grayOnGrayHue04})`,
    grayOnGray05: `oklch(${v.grayOnGrayLightness05} ${v.grayOnGrayChroma05} ${v.grayOnGrayHue05})`,
    grayOnGray06: `oklch(${v.grayOnGrayLightness06} ${v.grayOnGrayChroma06} ${v.grayOnGrayHue06})`,
    grayOnGray07: `oklch(${v.grayOnGrayLightness07} ${v.grayOnGrayChroma07} ${v.grayOnGrayHue07})`,
    grayOnGray08: `oklch(${v.grayOnGrayLightness08} ${v.grayOnGrayChroma08} ${v.grayOnGrayHue08})`,
    grayOnGray09: `oklch(${v.grayOnGrayLightness09} ${v.grayOnGrayChroma09} ${v.grayOnGrayHue09})`,
    grayOnGray10: `oklch(${v.grayOnGrayLightness10} ${v.grayOnGrayChroma10} ${v.grayOnGrayHue10})`,
    grayOnGray11: `oklch(${v.grayOnGrayLightness11} ${v.grayOnGrayChroma11} ${v.grayOnGrayHue11})`,
    grayOnGray12: `oklch(${v.grayOnGrayLightness12} ${v.grayOnGrayChroma12} ${v.grayOnGrayHue12})`,
    grayOnGray13: `oklch(${v.grayOnGrayLightness13} ${v.grayOnGrayChroma13} ${v.grayOnGrayHue13})`,
    grayOnGray14: `oklch(${v.grayOnGrayLightness14} ${v.grayOnGrayChroma14} ${v.grayOnGrayHue14})`,
    grayOnGray15: `oklch(${v.grayOnGrayLightness15} ${v.grayOnGrayChroma15} ${v.grayOnGrayHue15})`,
    grayOnGray16: `oklch(${v.grayOnGrayLightness16} ${v.grayOnGrayChroma16} ${v.grayOnGrayHue16})`,
    grayOnGray17: `oklch(${v.grayOnGrayLightness17} ${v.grayOnGrayChroma17} ${v.grayOnGrayHue17})`,
    grayOnGray18: `oklch(${v.grayOnGrayLightness18} ${v.grayOnGrayChroma18} ${v.grayOnGrayHue18})`,
    grayOnGray19: `oklch(${v.grayOnGrayLightness19} ${v.grayOnGrayChroma19} ${v.grayOnGrayHue19})`,
    grayOnGray20: `oklch(${v.grayOnGrayLightness20} ${v.grayOnGrayChroma20} ${v.grayOnGrayHue20})`,
  };
  const grayOnRed = {
    grayOnRed01: `oklch(${v.grayOnRedLightness01} ${v.grayOnRedChroma01} ${v.grayOnRedHue01})`,
    grayOnRed02: `oklch(${v.grayOnRedLightness02} ${v.grayOnRedChroma02} ${v.grayOnRedHue02})`,
    grayOnRed03: `oklch(${v.grayOnRedLightness03} ${v.grayOnRedChroma03} ${v.grayOnRedHue03})`,
    grayOnRed04: `oklch(${v.grayOnRedLightness04} ${v.grayOnRedChroma04} ${v.grayOnRedHue04})`,
    grayOnRed05: `oklch(${v.grayOnRedLightness05} ${v.grayOnRedChroma05} ${v.grayOnRedHue05})`,
    grayOnRed06: `oklch(${v.grayOnRedLightness06} ${v.grayOnRedChroma06} ${v.grayOnRedHue06})`,
    grayOnRed07: `oklch(${v.grayOnRedLightness07} ${v.grayOnRedChroma07} ${v.grayOnRedHue07})`,
    grayOnRed08: `oklch(${v.grayOnRedLightness08} ${v.grayOnRedChroma08} ${v.grayOnRedHue08})`,
    grayOnRed09: `oklch(${v.grayOnRedLightness09} ${v.grayOnRedChroma09} ${v.grayOnRedHue09})`,
    grayOnRed10: `oklch(${v.grayOnRedLightness10} ${v.grayOnRedChroma10} ${v.grayOnRedHue10})`,
    grayOnRed11: `oklch(${v.grayOnRedLightness11} ${v.grayOnRedChroma11} ${v.grayOnRedHue11})`,
    grayOnRed12: `oklch(${v.grayOnRedLightness12} ${v.grayOnRedChroma12} ${v.grayOnRedHue12})`,
    grayOnRed13: `oklch(${v.grayOnRedLightness13} ${v.grayOnRedChroma13} ${v.grayOnRedHue13})`,
    grayOnRed14: `oklch(${v.grayOnRedLightness14} ${v.grayOnRedChroma14} ${v.grayOnRedHue14})`,
    grayOnRed15: `oklch(${v.grayOnRedLightness15} ${v.grayOnRedChroma15} ${v.grayOnRedHue15})`,
    grayOnRed16: `oklch(${v.grayOnRedLightness16} ${v.grayOnRedChroma16} ${v.grayOnRedHue16})`,
    grayOnRed17: `oklch(${v.grayOnRedLightness17} ${v.grayOnRedChroma17} ${v.grayOnRedHue17})`,
    grayOnRed18: `oklch(${v.grayOnRedLightness18} ${v.grayOnRedChroma18} ${v.grayOnRedHue18})`,
    grayOnRed19: `oklch(${v.grayOnRedLightness19} ${v.grayOnRedChroma19} ${v.grayOnRedHue19})`,
    grayOnRed20: `oklch(${v.grayOnRedLightness20} ${v.grayOnRedChroma20} ${v.grayOnRedHue20})`,
  };
  const grayOnBlue = {
    grayOnBlue01: `oklch(${v.grayOnBlueLightness01} ${v.grayOnBlueChroma01} ${v.grayOnBlueHue01})`,
    grayOnBlue02: `oklch(${v.grayOnBlueLightness02} ${v.grayOnBlueChroma02} ${v.grayOnBlueHue02})`,
    grayOnBlue03: `oklch(${v.grayOnBlueLightness03} ${v.grayOnBlueChroma03} ${v.grayOnBlueHue03})`,
    grayOnBlue04: `oklch(${v.grayOnBlueLightness04} ${v.grayOnBlueChroma04} ${v.grayOnBlueHue04})`,
    grayOnBlue05: `oklch(${v.grayOnBlueLightness05} ${v.grayOnBlueChroma05} ${v.grayOnBlueHue05})`,
    grayOnBlue06: `oklch(${v.grayOnBlueLightness06} ${v.grayOnBlueChroma06} ${v.grayOnBlueHue06})`,
    grayOnBlue07: `oklch(${v.grayOnBlueLightness07} ${v.grayOnBlueChroma07} ${v.grayOnBlueHue07})`,
    grayOnBlue08: `oklch(${v.grayOnBlueLightness08} ${v.grayOnBlueChroma08} ${v.grayOnBlueHue08})`,
    grayOnBlue09: `oklch(${v.grayOnBlueLightness09} ${v.grayOnBlueChroma09} ${v.grayOnBlueHue09})`,
    grayOnBlue10: `oklch(${v.grayOnBlueLightness10} ${v.grayOnBlueChroma10} ${v.grayOnBlueHue10})`,
    grayOnBlue11: `oklch(${v.grayOnBlueLightness11} ${v.grayOnBlueChroma11} ${v.grayOnBlueHue11})`,
    grayOnBlue12: `oklch(${v.grayOnBlueLightness12} ${v.grayOnBlueChroma12} ${v.grayOnBlueHue12})`,
    grayOnBlue13: `oklch(${v.grayOnBlueLightness13} ${v.grayOnBlueChroma13} ${v.grayOnBlueHue13})`,
    grayOnBlue14: `oklch(${v.grayOnBlueLightness14} ${v.grayOnBlueChroma14} ${v.grayOnBlueHue14})`,
    grayOnBlue15: `oklch(${v.grayOnBlueLightness15} ${v.grayOnBlueChroma15} ${v.grayOnBlueHue15})`,
    grayOnBlue16: `oklch(${v.grayOnBlueLightness16} ${v.grayOnBlueChroma16} ${v.grayOnBlueHue16})`,
    grayOnBlue17: `oklch(${v.grayOnBlueLightness17} ${v.grayOnBlueChroma17} ${v.grayOnBlueHue17})`,
    grayOnBlue18: `oklch(${v.grayOnBlueLightness18} ${v.grayOnBlueChroma18} ${v.grayOnBlueHue18})`,
    grayOnBlue19: `oklch(${v.grayOnBlueLightness19} ${v.grayOnBlueChroma19} ${v.grayOnBlueHue19})`,
    grayOnBlue20: `oklch(${v.grayOnBlueLightness20} ${v.grayOnBlueChroma20} ${v.grayOnBlueHue20})`,
  };
  const grayOnTheme = {
    grayOnTheme01: `oklch(${v.grayOnThemeLightness01} ${v.grayOnThemeChroma01} ${v.grayOnThemeHue01})`,
    grayOnTheme02: `oklch(${v.grayOnThemeLightness02} ${v.grayOnThemeChroma02} ${v.grayOnThemeHue02})`,
    grayOnTheme03: `oklch(${v.grayOnThemeLightness03} ${v.grayOnThemeChroma03} ${v.grayOnThemeHue03})`,
    grayOnTheme04: `oklch(${v.grayOnThemeLightness04} ${v.grayOnThemeChroma04} ${v.grayOnThemeHue04})`,
    grayOnTheme05: `oklch(${v.grayOnThemeLightness05} ${v.grayOnThemeChroma05} ${v.grayOnThemeHue05})`,
    grayOnTheme06: `oklch(${v.grayOnThemeLightness06} ${v.grayOnThemeChroma06} ${v.grayOnThemeHue06})`,
    grayOnTheme07: `oklch(${v.grayOnThemeLightness07} ${v.grayOnThemeChroma07} ${v.grayOnThemeHue07})`,
    grayOnTheme08: `oklch(${v.grayOnThemeLightness08} ${v.grayOnThemeChroma08} ${v.grayOnThemeHue08})`,
    grayOnTheme09: `oklch(${v.grayOnThemeLightness09} ${v.grayOnThemeChroma09} ${v.grayOnThemeHue09})`,
    grayOnTheme10: `oklch(${v.grayOnThemeLightness10} ${v.grayOnThemeChroma10} ${v.grayOnThemeHue10})`,
    grayOnTheme11: `oklch(${v.grayOnThemeLightness11} ${v.grayOnThemeChroma11} ${v.grayOnThemeHue11})`,
    grayOnTheme12: `oklch(${v.grayOnThemeLightness12} ${v.grayOnThemeChroma12} ${v.grayOnThemeHue12})`,
    grayOnTheme13: `oklch(${v.grayOnThemeLightness13} ${v.grayOnThemeChroma13} ${v.grayOnThemeHue13})`,
    grayOnTheme14: `oklch(${v.grayOnThemeLightness14} ${v.grayOnThemeChroma14} ${v.grayOnThemeHue14})`,
    grayOnTheme15: `oklch(${v.grayOnThemeLightness15} ${v.grayOnThemeChroma15} ${v.grayOnThemeHue15})`,
    grayOnTheme16: `oklch(${v.grayOnThemeLightness16} ${v.grayOnThemeChroma16} ${v.grayOnThemeHue16})`,
    grayOnTheme17: `oklch(${v.grayOnThemeLightness17} ${v.grayOnThemeChroma17} ${v.grayOnThemeHue17})`,
    grayOnTheme18: `oklch(${v.grayOnThemeLightness18} ${v.grayOnThemeChroma18} ${v.grayOnThemeHue18})`,
    grayOnTheme19: `oklch(${v.grayOnThemeLightness19} ${v.grayOnThemeChroma19} ${v.grayOnThemeHue19})`,
    grayOnTheme20: `oklch(${v.grayOnThemeLightness20} ${v.grayOnThemeChroma20} ${v.grayOnThemeHue20})`,
  };
  const themeOnGray = {
    themeOnGray01: `oklch(${v.themeOnGrayLightness01} ${v.themeOnGrayChroma01} ${v.themeOnGrayHue01})`,
    themeOnGray02: `oklch(${v.themeOnGrayLightness02} ${v.themeOnGrayChroma02} ${v.themeOnGrayHue02})`,
    themeOnGray03: `oklch(${v.themeOnGrayLightness03} ${v.themeOnGrayChroma03} ${v.themeOnGrayHue03})`,
    themeOnGray04: `oklch(${v.themeOnGrayLightness04} ${v.themeOnGrayChroma04} ${v.themeOnGrayHue04})`,
    themeOnGray05: `oklch(${v.themeOnGrayLightness05} ${v.themeOnGrayChroma05} ${v.themeOnGrayHue05})`,
    themeOnGray06: `oklch(${v.themeOnGrayLightness06} ${v.themeOnGrayChroma06} ${v.themeOnGrayHue06})`,
    themeOnGray07: `oklch(${v.themeOnGrayLightness07} ${v.themeOnGrayChroma07} ${v.themeOnGrayHue07})`,
    themeOnGray08: `oklch(${v.themeOnGrayLightness08} ${v.themeOnGrayChroma08} ${v.themeOnGrayHue08})`,
    themeOnGray09: `oklch(${v.themeOnGrayLightness09} ${v.themeOnGrayChroma09} ${v.themeOnGrayHue09})`,
    themeOnGray10: `oklch(${v.themeOnGrayLightness10} ${v.themeOnGrayChroma10} ${v.themeOnGrayHue10})`,
    themeOnGray11: `oklch(${v.themeOnGrayLightness11} ${v.themeOnGrayChroma11} ${v.themeOnGrayHue11})`,
    themeOnGray12: `oklch(${v.themeOnGrayLightness12} ${v.themeOnGrayChroma12} ${v.themeOnGrayHue12})`,
    themeOnGray13: `oklch(${v.themeOnGrayLightness13} ${v.themeOnGrayChroma13} ${v.themeOnGrayHue13})`,
    themeOnGray14: `oklch(${v.themeOnGrayLightness14} ${v.themeOnGrayChroma14} ${v.themeOnGrayHue14})`,
    themeOnGray15: `oklch(${v.themeOnGrayLightness15} ${v.themeOnGrayChroma15} ${v.themeOnGrayHue15})`,
    themeOnGray16: `oklch(${v.themeOnGrayLightness16} ${v.themeOnGrayChroma16} ${v.themeOnGrayHue16})`,
    themeOnGray17: `oklch(${v.themeOnGrayLightness17} ${v.themeOnGrayChroma17} ${v.themeOnGrayHue17})`,
    themeOnGray18: `oklch(${v.themeOnGrayLightness18} ${v.themeOnGrayChroma18} ${v.themeOnGrayHue18})`,
    themeOnGray19: `oklch(${v.themeOnGrayLightness19} ${v.themeOnGrayChroma19} ${v.themeOnGrayHue19})`,
    themeOnGray20: `oklch(${v.themeOnGrayLightness20} ${v.themeOnGrayChroma20} ${v.themeOnGrayHue20})`,
  };

  const values = {
    ...gray,
    ...grayAlpha,
    ...red,
    ...blue,
    ...theme,
    ...grayOnGray,
    ...grayOnRed,
    ...grayOnBlue,
    ...grayOnTheme,
    ...themeOnGray,
  };

  return {
    values,
    names: valuesToNames(values),
    variables: valuesToVariables(values),
  };
};

const defineSemanticColor = (v: ReturnType<typeof definePrimitiveColor>["variables"]) => {
  const backgroundColor = {
    /**
     * 基本的な背景色
     * - 背景として：⭕
     * - 文字やアイコンとして：❌
     */
    colorBackgroundDefault: `light-dark(${v.gray01}, ${v.gray15})`,

    /**
     * さりげない背景色
     * - 背景として：⭕
     * - 文字やアイコンとして：❌
     */
    colorBackgroundSubtle: `light-dark(${v.gray02}, ${v.gray16})`,

    /**
     * 基本的な背景色を反転したかのような背景色
     * - 背景として：⭕
     * - 文字やアイコンとして：❌
     */
    colorBackgroundInverted: `light-dark(${v.gray20}, ${v.gray03})`,
  };

  /**
   * surface
   * - background の上で使用する色
   */
  const surfaceColor = {
    /**
     * テーマカラーを用いた背景色
     * - 背景として：⭕
     * - 文字やアイコンとして：❌
     */
    colorSurfaceTheme: `light-dark(${v.theme11}, ${v.theme11})`,

    /**
     * テーマカラーを用いたさりげない背景色
     * - 背景として：⭕
     * - 文字やアイコンとして：❌
     */
    colorSurfaceThemeSubtle: `light-dark(${v.theme01}, ${v.theme15})`,

    /**
     * 必須であることを表現する背景色
     * - 背景として：⭕
     * - 文字やアイコンとして：❌
     */
    colorSurfaceRequired: `light-dark(${v.red08}, ${v.red04})`,
  };

  /**
   * object
   * - background もしくは surface の上で使用する色
   * - 文字やアイコンの色
   */
  const objectColor = {
    /**
     * 基本的な文字色
     * - 背景として：❌
     * - 文字やアイコンとして
     *   - 基本的な背景色のなかで：⭕
     *   - さりげない背景色のなかで：⭕
     */
    colorObjectDefault: `light-dark(${v.gray20}, ${v.gray02})`,

    /**
     * さりげない文字色
     * - 背景として：❌
     * - 文字やアイコンとして
     *   - 基本的な背景色のなかで：⭕
     *   - さりげない背景色のなかで：⭕
     */
    colorObjectSubtle: `light-dark(${v.gray10}, ${v.gray07})`,

    /**
     * 基本的な文字色を反転したかのような文字色
     * - 背景として：❌
     * - 文字やアイコンとして
     *   - 基本的な背景色を反転したかのような背景色のなかで：⭕
     */
    colorObjectInverted: `light-dark(${v.gray01}, ${v.gray15})`,

    /**
     * テーマカラーを用いた文字色
     * - 背景として：❌
     * - 文字やアイコンとして
     *   - 基本的な背景色のなかで：⭕
     *   - さりげない背景色のなかで：⭕
     */
    colorObjectTheme: `light-dark(${v.themeOnGray01}, ${v.themeOnGray15})`,

    /**
     * 基本的な文字色
     * - 背景として：❌
     * - 文字やアイコンとして
     *   - テーマカラーを用いた背景色のなかで：⭕
     */
    colorObjectOnTheme: `light-dark(${v.grayOnTheme11}, ${v.grayOnTheme11})`,

    /**
     * 基本的な文字色
     * - 背景として：❌
     * - 文字やアイコンとして
     *   - テーマカラーを用いたさりげない背景色のなかで：⭕
     */
    colorObjectOnThemeSubtle: `light-dark(${v.grayOnTheme01}, ${v.grayOnTheme15})`,

    /**
     * 必須であることを表現する色
     * - 背景として：❌
     * - 文字やアイコンとして
     *   - 基本的な背景色のなかで：⭕
     *   - 必須であることを表現する背景色のなかで：⭕
     */
    colorObjectOnRequired: `light-dark(${v.grayOnRed08}, ${v.grayOnRed04})`,

    /**
     * エラーであることを表現する色
     * - 背景として：❌
     * - 文字やアイコンとして
     *   - 基本的な背景色のなかで：⭕
     *   - さりげない背景色のなかで：⭕
     */
    colorObjectError: `light-dark(${v.red11}, ${v.red04})`,

    /**
     * リンクの色
     * - 背景として：❌
     * - 文字やアイコンとして
     *   - 基本的な背景色のなかで：⭕
     *   - さりげない背景色のなかで：⭕
     */
    colorObjectLink: `light-dark(${v.blue13}, ${v.blue04})`,
  };

  const borderColor = {
    /**
     * 基本的なボーダーの色
     */
    colorBorderDefault: `light-dark(${v.gray03}, ${v.gray12})`,

    /**
     * 目立つボーダーの色
     */
    colorBorderObvious: `light-dark(${v.gray05}, ${v.gray12})`,
  };

  const shadowColor = {
    /**
     * 基本的なシャドウの色
     */
    colorShadowGray: `light-dark(${v.grayAlpha20}, ${v.grayAlpha20})`,
  };

  const values = {
    ...backgroundColor,
    ...surfaceColor,
    ...objectColor,
    ...borderColor,
    ...shadowColor,
  };

  return {
    values,
    names: valuesToNames(values),
    variables: valuesToVariables(values),
  };
};

const defineSizeAtom = (baseSize: number) => {
  const values = {
    baseSize: `${baseSize}`,
  };

  return {
    values,
    names: valuesToNames(values),
    variables: valuesToVariables(values),
  };
};

const defineSize = (v: ReturnType<typeof defineSizeAtom>["variables"]) => {
  const values = {
    // size フィボナッチ数列を使って生成した値に対して倍率を乗算したもの
    size002: `calc(2 * 1rem / 16 * ${v.baseSize} / 16)`,
    size004: `calc(4 * 1rem / 16 * ${v.baseSize} / 16)`,
    size008: `calc(8 * 1rem / 16 * ${v.baseSize} / 16)`,
    size012: `calc(12 * 1rem / 16 * ${v.baseSize} / 16)`,
    size016: `calc(16 * 1rem / 16 * ${v.baseSize} / 16)`,
    size020: `calc(20 * 1rem / 16 * ${v.baseSize} / 16)`,
    size032: `calc(32 * 1rem / 16 * ${v.baseSize} / 16)`,
    size052: `calc(52 * 1rem / 16 * ${v.baseSize} / 16)`,
    size084: `calc(84 * 1rem / 16 * ${v.baseSize} / 16)`,
    size136: `calc(136 * 1rem / 16 * ${v.baseSize} / 16)`,
    size220: `calc(220 * 1rem / 16 * ${v.baseSize} / 16)`,
    size356: `calc(356 * 1rem / 16 * ${v.baseSize} / 16)`,
  };

  return {
    values,
    names: valuesToNames(values),
    variables: valuesToVariables(values),
  };
};

const defineBorderRadius = (v: ReturnType<typeof defineSize>["variables"]) => {
  const values = {
    borderRadius8: v.size008,
  };

  return {
    values,
    names: valuesToNames(values),
    variables: valuesToVariables(values),
  };
};

const defineShadow = (vc: ReturnType<typeof defineSemanticColor>["variables"], vs: ReturnType<typeof defineSize>["variables"]) => {
  const values = {
    boxShadow8: `0 ${vs.size008} ${vs.size012} calc(-1 * ${vs.size008}) ${vc.colorShadowGray}`,
  };

  return {
    values,
    names: valuesToNames(values),
    variables: valuesToVariables(values),
  };
};

const defineFontAtom = (v: ReturnType<typeof defineSizeAtom>["variables"]) => {
  const values = {
    fontFamilySans: "system-ui, sans-serif",
    fontFamilySerif: "ui-serif, serif",
    fontFamilyMono: "ui-monospace, monospace",
    fontWeightNormal: "normal",
    fontWeightBold: "bold",

    // fontSize 調和数列を使って生成した値に近い整数に対して倍率を乗算したもの
    fontSize11: `calc(11 * 1rem / 16 * ${v.baseSize} / 16)`, // 8 / 12 * 16px ≈ 11px
    fontSize12: `calc(12 * 1rem / 16 * ${v.baseSize} / 16)`, // 8 / 11 * 16px ≈ 12px
    fontSize13: `calc(13 * 1rem / 16 * ${v.baseSize} / 16)`, // 8 / 10 * 16px ≈ 13px
    fontSize14: `calc(14 * 1rem / 16 * ${v.baseSize} / 16)`, // 8 / 9 * 16px ≈ 14px
    fontSize16: `calc(16 * 1rem / 16 * ${v.baseSize} / 16)`, // 8 / 8 * 16px ≈ 16px
    fontSize18: `calc(18 * 1rem / 16 * ${v.baseSize} / 16)`, // 8 / 7 * 16px ≈ 18px
    fontSize21: `calc(21 * 1rem / 16 * ${v.baseSize} / 16)`, // 8 / 6 * 16px ≈ 21px
    fontSize26: `calc(26 * 1rem / 16 * ${v.baseSize} / 16)`, // 8 / 5 * 16px ≈ 26px
    fontSize32: `calc(32 * 1rem / 16 * ${v.baseSize} / 16)`, // 8 / 4 * 16px ≈ 32px

    // lineHeight フォントサイズの1.5〜1.7倍程度の4の倍数に対して倍率を乗算したもの
    lineHeight16: `calc(16 * 1rem / 16 * ${v.baseSize} / 16)`,
    lineHeight20: `calc(20 * 1rem / 16 * ${v.baseSize} / 16)`,
    lineHeight24: `calc(24 * 1rem / 16 * ${v.baseSize} / 16)`,
    lineHeight32: `calc(32 * 1rem / 16 * ${v.baseSize} / 16)`,
    lineHeight40: `calc(40 * 1rem / 16 * ${v.baseSize} / 16)`,
    lineHeight48: `calc(48 * 1rem / 16 * ${v.baseSize} / 16)`,
  };

  return {
    values,
    names: valuesToNames(values),
    variables: valuesToVariables(values),
  };
};

const defineFont = (v: ReturnType<typeof defineFontAtom>["variables"]) => {
  const values = {
    fontSans11Normal: `${v.fontWeightNormal} ${v.fontSize11} / ${v.lineHeight16} ${v.fontFamilySans}`,
    fontSans12Normal: `${v.fontWeightNormal} ${v.fontSize12} / ${v.lineHeight20} ${v.fontFamilySans}`,
    fontSans13Normal: `${v.fontWeightNormal} ${v.fontSize13} / ${v.lineHeight20} ${v.fontFamilySans}`,
    fontSans14Normal: `${v.fontWeightNormal} ${v.fontSize14} / ${v.lineHeight24} ${v.fontFamilySans}`,
    fontSans16Normal: `${v.fontWeightNormal} ${v.fontSize16} / ${v.lineHeight24} ${v.fontFamilySans}`,
    fontSans18Normal: `${v.fontWeightNormal} ${v.fontSize18} / ${v.lineHeight32} ${v.fontFamilySans}`,
    fontSans21Normal: `${v.fontWeightNormal} ${v.fontSize21} / ${v.lineHeight32} ${v.fontFamilySans}`,
    fontSans26Normal: `${v.fontWeightNormal} ${v.fontSize26} / ${v.lineHeight40} ${v.fontFamilySans}`,
    fontSans32Normal: `${v.fontWeightNormal} ${v.fontSize32} / ${v.lineHeight48} ${v.fontFamilySans}`,
    fontSans11Bold: `${v.fontWeightBold} ${v.fontSize11} / ${v.lineHeight16} ${v.fontFamilySans}`,
    fontSans12Bold: `${v.fontWeightBold} ${v.fontSize12} / ${v.lineHeight20} ${v.fontFamilySans}`,
    fontSans13Bold: `${v.fontWeightBold} ${v.fontSize13} / ${v.lineHeight20} ${v.fontFamilySans}`,
    fontSans14Bold: `${v.fontWeightBold} ${v.fontSize14} / ${v.lineHeight24} ${v.fontFamilySans}`,
    fontSans16Bold: `${v.fontWeightBold} ${v.fontSize16} / ${v.lineHeight24} ${v.fontFamilySans}`,
    fontSans18Bold: `${v.fontWeightBold} ${v.fontSize18} / ${v.lineHeight32} ${v.fontFamilySans}`,
    fontSans21Bold: `${v.fontWeightBold} ${v.fontSize21} / ${v.lineHeight32} ${v.fontFamilySans}`,
    fontSans26Bold: `${v.fontWeightBold} ${v.fontSize26} / ${v.lineHeight40} ${v.fontFamilySans}`,
    fontSans32Bold: `${v.fontWeightBold} ${v.fontSize32} / ${v.lineHeight48} ${v.fontFamilySans}`,
    fontSerif11Normal: `${v.fontWeightNormal} ${v.fontSize11} / ${v.lineHeight16} ${v.fontFamilySerif}`,
    fontSerif12Normal: `${v.fontWeightNormal} ${v.fontSize12} / ${v.lineHeight20} ${v.fontFamilySerif}`,
    fontSerif13Normal: `${v.fontWeightNormal} ${v.fontSize13} / ${v.lineHeight20} ${v.fontFamilySerif}`,
    fontSerif14Normal: `${v.fontWeightNormal} ${v.fontSize14} / ${v.lineHeight24} ${v.fontFamilySerif}`,
    fontSerif16Normal: `${v.fontWeightNormal} ${v.fontSize16} / ${v.lineHeight24} ${v.fontFamilySerif}`,
    fontSerif18Normal: `${v.fontWeightNormal} ${v.fontSize18} / ${v.lineHeight32} ${v.fontFamilySerif}`,
    fontSerif21Normal: `${v.fontWeightNormal} ${v.fontSize21} / ${v.lineHeight32} ${v.fontFamilySerif}`,
    fontSerif26Normal: `${v.fontWeightNormal} ${v.fontSize26} / ${v.lineHeight40} ${v.fontFamilySerif}`,
    fontSerif32Normal: `${v.fontWeightNormal} ${v.fontSize32} / ${v.lineHeight48} ${v.fontFamilySerif}`,
    fontSerif11Bold: `${v.fontWeightBold} ${v.fontSize11} / ${v.lineHeight16} ${v.fontFamilySerif}`,
    fontSerif12Bold: `${v.fontWeightBold} ${v.fontSize12} / ${v.lineHeight20} ${v.fontFamilySerif}`,
    fontSerif13Bold: `${v.fontWeightBold} ${v.fontSize13} / ${v.lineHeight20} ${v.fontFamilySerif}`,
    fontSerif14Bold: `${v.fontWeightBold} ${v.fontSize14} / ${v.lineHeight24} ${v.fontFamilySerif}`,
    fontSerif16Bold: `${v.fontWeightBold} ${v.fontSize16} / ${v.lineHeight24} ${v.fontFamilySerif}`,
    fontSerif18Bold: `${v.fontWeightBold} ${v.fontSize18} / ${v.lineHeight32} ${v.fontFamilySerif}`,
    fontSerif21Bold: `${v.fontWeightBold} ${v.fontSize21} / ${v.lineHeight32} ${v.fontFamilySerif}`,
    fontSerif26Bold: `${v.fontWeightBold} ${v.fontSize26} / ${v.lineHeight40} ${v.fontFamilySerif}`,
    fontSerif32Bold: `${v.fontWeightBold} ${v.fontSize32} / ${v.lineHeight48} ${v.fontFamilySerif}`,
    fontMono11Normal: `${v.fontWeightNormal} ${v.fontSize11} / ${v.lineHeight16} ${v.fontFamilyMono}`,
    fontMono12Normal: `${v.fontWeightNormal} ${v.fontSize12} / ${v.lineHeight20} ${v.fontFamilyMono}`,
    fontMono13Normal: `${v.fontWeightNormal} ${v.fontSize13} / ${v.lineHeight20} ${v.fontFamilyMono}`,
    fontMono14Normal: `${v.fontWeightNormal} ${v.fontSize14} / ${v.lineHeight24} ${v.fontFamilyMono}`,
    fontMono16Normal: `${v.fontWeightNormal} ${v.fontSize16} / ${v.lineHeight24} ${v.fontFamilyMono}`,
    fontMono18Normal: `${v.fontWeightNormal} ${v.fontSize18} / ${v.lineHeight32} ${v.fontFamilyMono}`,
    fontMono21Normal: `${v.fontWeightNormal} ${v.fontSize21} / ${v.lineHeight32} ${v.fontFamilyMono}`,
    fontMono26Normal: `${v.fontWeightNormal} ${v.fontSize26} / ${v.lineHeight40} ${v.fontFamilyMono}`,
    fontMono32Normal: `${v.fontWeightNormal} ${v.fontSize32} / ${v.lineHeight48} ${v.fontFamilyMono}`,
    fontMono11Bold: `${v.fontWeightBold} ${v.fontSize11} / ${v.lineHeight16} ${v.fontFamilyMono}`,
    fontMono12Bold: `${v.fontWeightBold} ${v.fontSize12} / ${v.lineHeight20} ${v.fontFamilyMono}`,
    fontMono13Bold: `${v.fontWeightBold} ${v.fontSize13} / ${v.lineHeight20} ${v.fontFamilyMono}`,
    fontMono14Bold: `${v.fontWeightBold} ${v.fontSize14} / ${v.lineHeight24} ${v.fontFamilyMono}`,
    fontMono16Bold: `${v.fontWeightBold} ${v.fontSize16} / ${v.lineHeight24} ${v.fontFamilyMono}`,
    fontMono18Bold: `${v.fontWeightBold} ${v.fontSize18} / ${v.lineHeight32} ${v.fontFamilyMono}`,
    fontMono21Bold: `${v.fontWeightBold} ${v.fontSize21} / ${v.lineHeight32} ${v.fontFamilyMono}`,
    fontMono26Bold: `${v.fontWeightBold} ${v.fontSize26} / ${v.lineHeight40} ${v.fontFamilyMono}`,
    fontMono32Bold: `${v.fontWeightBold} ${v.fontSize32} / ${v.lineHeight48} ${v.fontFamilyMono}`,
  };

  return {
    values,
    names: valuesToNames(values),
    variables: valuesToVariables(values),
  };
};

export const makeTheme = (baseSize: number, themeLightness: number, themeChroma: number, themeHue: number) => {
  // Atom
  const colorAtom = defineColorAtom(themeLightness, themeChroma, themeHue);
  const sizeAtom = defineSizeAtom(baseSize);
  const fontAtom = defineFontAtom(sizeAtom.variables);

  // Color
  const primitiveColor = definePrimitiveColor(colorAtom.variables);
  const semanticColor = defineSemanticColor(primitiveColor.variables);

  // Size
  const size = defineSize(sizeAtom.variables);

  // BorderRadius
  const borderRadius = defineBorderRadius(size.variables);

  // Shadow
  const shadow = defineShadow(semanticColor.variables, size.variables);

  // Font
  const font = defineFont(fontAtom.variables);

  const structuredTheme = {
    atom: { color: colorAtom, size: sizeAtom, font: fontAtom },
    color: { primitive: primitiveColor, semantic: semanticColor },
    size: size,
    borderRadius: borderRadius,
    shadow: shadow,
    font: font,
  };

  const allValues = {
    ...colorAtom.values,
    ...sizeAtom.values,
    ...fontAtom.values,
    ...primitiveColor.values,
    ...semanticColor.values,
    ...size.values,
    ...borderRadius.values,
    ...shadow.values,
    ...font.values,
  };

  const allNames = {
    ...colorAtom.names,
    ...sizeAtom.names,
    ...fontAtom.names,
    ...primitiveColor.names,
    ...semanticColor.names,
    ...size.names,
    ...borderRadius.names,
    ...shadow.names,
    ...font.names,
  };

  const allVariables = {
    ...colorAtom.variables,
    ...sizeAtom.variables,
    ...fontAtom.variables,
    ...primitiveColor.variables,
    ...semanticColor.variables,
    ...size.variables,
    ...borderRadius.variables,
    ...shadow.variables,
    ...font.variables,
  };

  const variables = {
    ...primitiveColor.variables,
    ...semanticColor.variables,
    ...size.variables,
    ...borderRadius.variables,
    ...shadow.variables,
    ...font.variables,
  };

  return {
    structuredTheme,
    allValues,
    allNames,
    allVariables,
    variables,
  };
};
