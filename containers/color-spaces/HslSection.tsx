import { FC } from "react";
import { HeadingLevel1 } from "../../components/general/HeadingLevel1";
import { useTheme } from "../../contexts/theme";
import { Controls, useControls } from "./Controls";
import { SingleColorResult } from "./SingleColorResult";

export const HslSection: FC = () => {
  const theme = useTheme();

  const {
    componentProps,
    values: [hue, saturation, lightness],
  } = useControls(
    { label: "Hue（色相角）", defaultValue: 180, max: 360, step: 1 },
    { label: "Saturation（彩度）", defaultValue: 50, max: 100, step: 1 },
    { label: "Lightness（輝度）", defaultValue: 50, max: 100, step: 1 },
  );

  return (
    <>
      <section>
        <HeadingLevel1>HSL</HeadingLevel1>

        <Controls {...componentProps} />

        <SingleColorResult color={`hsl(${hue} ${saturation}% ${lightness}%)`} />
      </section>

      <style jsx>{`
        section {
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size012};
        }
      `}</style>
    </>
  );
};
