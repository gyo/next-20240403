import { FC } from "react";
import { HeadingLevel1 } from "../../components/general/HeadingLevel1";
import { useTheme } from "../../contexts/theme";
import { Controls, useControls } from "./Controls";
import { SingleColorResult } from "./SingleColorResult";

export const RgbSection: FC = () => {
  const theme = useTheme();

  const {
    componentProps,
    values: [red, green, blue],
  } = useControls(
    { label: "Red（赤）", defaultValue: 128, max: 255, step: 1 },
    { label: "Green（緑）", defaultValue: 128, max: 255, step: 1 },
    { label: "Blue（青）", defaultValue: 128, max: 255, step: 1 },
  );

  return (
    <>
      <section>
        <HeadingLevel1>RGB</HeadingLevel1>

        <Controls {...componentProps} />

        <SingleColorResult color={`rgb(${red} ${green} ${blue})`} />
      </section>

      <style jsx>{`
        section {
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size012};
        }
      `}</style>
    </>
  );
};
