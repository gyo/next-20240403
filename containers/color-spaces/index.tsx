import Head from "next/head";
import Link from "next/link";
import { AppWrapper } from "../../components/layout/AppWrapper";
import { useTheme } from "../../contexts/theme";
import { HslHueRotateSection } from "./HslHueRotateSection";
import { HslListSection } from "./HslListSection";
import { HslSection } from "./HslSection";
import { LchHueRotateSection } from "./LchHueRotateSection";
import { LchListSection } from "./LchListSection";
import { LchSection } from "./LchSection";
import { OklchHueRotateSection } from "./OklchHueRotateSection";
import { OklchListSection } from "./OklchListSection";
import { OklchSection } from "./OklchSection";
import { RgbSection } from "./RgbSection";

export const Page = () => {
  const theme = useTheme();

  return (
    <>
      <Head>
        <title>色空間について</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <AppWrapper>
        <div>
          <Link href="../">
            <span className="link">{"<< "}インデックスページ</span>
          </Link>
        </div>

        <div className="single-color-section">
          <RgbSection />
          <HslSection />
          <LchSection />
          <OklchSection />
        </div>

        <div className="multiple-color-section">
          <HslHueRotateSection />
          <LchHueRotateSection />
          <OklchHueRotateSection />
        </div>

        <div className="multiple-color-section">
          <HslListSection />
          <LchListSection />
          <OklchListSection />
        </div>
      </AppWrapper>

      <style jsx>{`
        .link {
          font: ${theme.fontSans16Normal};
          color: ${theme.colorObjectLink};
        }
        .single-color-section {
          margin-block-start: ${theme.size032};
          display: grid;
          grid-template-columns: repeat(auto-fill, minmax(400px, 1fr));
          gap: ${theme.size020};
        }
        .multiple-color-section {
          margin-block-start: ${theme.size032};
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size020};
        }
      `}</style>
    </>
  );
};
