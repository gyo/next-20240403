import { FC } from "react";
import { useTheme } from "../../contexts/theme";

const hueMax = 360;
const hueLength = 18;
const hueList = new Array(hueLength).fill(0).map((_, i) => (hueMax / hueLength) * i);

const lightnessMax = 100;
const lightnessLength = 20;
const lightnessList = new Array(lightnessLength)
  .fill(0)
  .map((_, i) => (lightnessMax / lightnessLength) * i)
  .concat(lightnessMax)
  .reverse();

type Props = {
  buildColor: (lightless: number, hue: number) => string;
};

export const GridColorResult: FC<Props> = ({ buildColor }) => {
  const theme = useTheme();

  return (
    <>
      <output className="grid-color-result">
        <div>
          {lightnessList.map((lightness) => {
            return (
              <p className="grid-color-result__text" key={lightness}>
                {lightness}
              </p>
            );
          })}
        </div>

        {hueList.map((hue) => {
          return (
            <div key={hue}>
              {lightnessList.map((lightness) => {
                const background = buildColor(lightness, hue);
                return <div className="grid-color-result__color" style={{ background }} key={background} />;
              })}
              <p className="grid-color-result__text">{hue}</p>
            </div>
          );
        })}
      </output>

      <style jsx>{`
        .grid-color-result {
          margin-block-start: ${theme.size012};
          display: flex;
          overflow: auto;
        }
        .grid-color-result__text {
          inline-size: ${theme.size052};
          font: ${theme.fontSans12Normal};
          color: ${theme.colorObjectDefault};
          text-align: center;
          text-wrap: nowrap;
        }
        .grid-color-result__color {
          inline-size: ${theme.size052};
          block-size: ${theme.size020};
        }
      `}</style>
    </>
  );
};
