import { FC } from "react";
import { HeadingLevel1 } from "../../components/general/HeadingLevel1";
import { useTheme } from "../../contexts/theme";
import { Controls, useControls } from "./Controls";
import { HueRotateResult } from "./HueRotateResult";

export const LchHueRotateSection: FC = () => {
  const theme = useTheme();

  const {
    componentProps,
    values: [lightness, chroma],
  } = useControls(
    { label: "Lightness（明るさ）", defaultValue: 50, max: 100, step: 1 },
    { label: "Chroma（彩度）", defaultValue: 60, max: 230, step: 1 },
    { label: "Hue（色相角）", staticValue: "横軸" },
  );

  return (
    <>
      <section>
        <HeadingLevel1>LCH Hue Rotate</HeadingLevel1>

        <Controls {...componentProps} />

        <HueRotateResult buildColor={(hue) => `lch(${lightness}% ${chroma} ${hue})`} />
      </section>

      <style jsx>{`
        section {
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size012};
        }
      `}</style>
    </>
  );
};
