import { FC } from "react";
import { HeadingLevel1 } from "../../components/general/HeadingLevel1";
import { useTheme } from "../../contexts/theme";
import { Controls, useControls } from "./Controls";
import { GridColorResult } from "./GridColorResult";

export const LchListSection: FC = () => {
  const theme = useTheme();

  const {
    componentProps,
    values: [lightness, chroma, hue],
  } = useControls(
    { label: "Lightness（明るさ）", staticValue: "縦軸" },
    { label: "Chroma（彩度）", defaultValue: 60, max: 230, step: 1 },
    { label: "Hue（色相角）", staticValue: "横軸" },
  );

  return (
    <>
      <section>
        <HeadingLevel1>LCH List</HeadingLevel1>

        <Controls {...componentProps} />

        <GridColorResult buildColor={(lightness, hue) => `lch(${lightness}% ${chroma} ${hue})`} />
      </section>

      <style jsx>{`
        section {
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size012};
        }
      `}</style>
    </>
  );
};
