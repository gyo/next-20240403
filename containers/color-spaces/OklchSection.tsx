import { FC } from "react";
import { HeadingLevel1 } from "../../components/general/HeadingLevel1";
import { useTheme } from "../../contexts/theme";
import { Controls, useControls } from "./Controls";
import { SingleColorResult } from "./SingleColorResult";

export const OklchSection: FC = () => {
  const theme = useTheme();

  const {
    componentProps,
    values: [lightness, chroma, hue],
  } = useControls(
    { label: "Lightness（明るさ）", defaultValue: 50, max: 100, step: 1 },
    { label: "Chroma（彩度）", defaultValue: 0.18, max: 0.5, step: 0.002 },
    { label: "Hue（色相角）", defaultValue: 180, max: 360, step: 1 },
  );

  return (
    <>
      <section>
        <HeadingLevel1>OKLCH</HeadingLevel1>

        <Controls {...componentProps} />

        <SingleColorResult color={`oklch(${lightness}% ${chroma} ${hue})`} />
      </section>

      <style jsx>{`
        section {
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size012};
        }
      `}</style>
    </>
  );
};
