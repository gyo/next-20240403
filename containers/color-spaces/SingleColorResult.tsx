import { FC } from "react";
import { useTheme } from "../../contexts/theme";

type Props = {
  color: string;
};

export const SingleColorResult: FC<Props> = ({ color }) => {
  const theme = useTheme();

  return (
    <>
      <output className="single-color-result">
        <div className="single-color-result__block" style={{ background: color }}></div>
        <p className="single-color-result__value">{color}</p>
      </output>

      <style jsx>{`
        .single-color-result {
          display: grid;
          grid-template-columns: auto 1fr;
          align-items: center;
          gap: ${theme.size020};
        }
        .single-color-result__block {
          border-radius: ${theme.size008};
          inline-size: ${theme.size136};
          block-size: ${theme.size136};
        }
        .single-color-result__value {
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectDefault};
        }
      `}</style>
    </>
  );
};
