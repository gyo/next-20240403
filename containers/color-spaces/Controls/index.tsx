import { ChangeEventHandler, FC, useId } from "react";
import { useTheme } from "../../../contexts/theme";
export * from "./hooks";

type ControllableValue = {
  label: string;
  value: number;
  max: number;
  step: number;
  handleChangeValue: ChangeEventHandler<HTMLInputElement>;
};

type StaticValue = {
  label: string;
  staticValue: string;
};

type Value = { label: string } & (ControllableValue | StaticValue);

export type Props = {
  value1?: Value;
  value2?: Value;
  value3?: Value;
};

export const Controls: FC<Props> = (props) => {
  const theme = useTheme();

  const id1 = useId();
  const id2 = useId();
  const id3 = useId();

  return (
    <>
      <div className="controls">
        {props.value1 != null ? (
          <>
            <label htmlFor={id1}>{props.value1.label}</label>

            {"staticValue" in props.value1 ? (
              <span className="controls__static-value">{props.value1.staticValue}</span>
            ) : (
              <span className="controls__input">
                <input
                  id={id1}
                  type="range"
                  min="0"
                  max={props.value1.max}
                  step={props.value1.step}
                  value={props.value1.value}
                  onChange={props.value1.handleChangeValue}
                />
                <output>{Number.isInteger(props.value1.value) ? props.value1.value : props.value1.value.toFixed(3)}</output>
              </span>
            )}
          </>
        ) : null}

        {props.value2 != null ? (
          <>
            <label htmlFor={id2}>{props.value2.label}</label>

            {"staticValue" in props.value2 ? (
              <span className="controls__static-value">{props.value2.staticValue}</span>
            ) : (
              <span className="controls__input">
                <input
                  id={id2}
                  type="range"
                  min="0"
                  max={props.value2.max}
                  step={props.value2.step}
                  value={props.value2.value}
                  onChange={props.value2.handleChangeValue}
                />
                <output>{Number.isInteger(props.value2.value) ? props.value2.value : props.value2.value.toFixed(3)}</output>
              </span>
            )}
          </>
        ) : null}

        {props.value3 != null ? (
          <>
            <label htmlFor={id3}>{props.value3.label}</label>

            {"staticValue" in props.value3 ? (
              <span className="controls__static-value">{props.value3.staticValue}</span>
            ) : (
              <span className="controls__input">
                <input
                  id={id3}
                  type="range"
                  min="0"
                  max={props.value3.max}
                  step={props.value3.step}
                  value={props.value3.value}
                  onChange={props.value3.handleChangeValue}
                />
                <output>{Number.isInteger(props.value3.value) ? props.value3.value : props.value3.value.toFixed(3)}</output>
              </span>
            )}
          </>
        ) : null}
      </div>

      <style jsx>{`
        .controls {
          padding-inline-start: ${theme.size004};
          display: grid;
          grid-template-columns: auto 1fr;
          gap: ${theme.size004};
        }
        .controls__static-value {
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectDefault};
          white-space: nowrap;
        }
        .controls__input {
          display: flex;
          align-items: center;
          gap: ${theme.size004};
        }
        label {
          justify-self: end;
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectDefault};
          white-space: nowrap;
        }
        input {
          flex-shrink: 0;
          display: block;
          inline-size: ${theme.size220};
        }
        output {
          flex-shrink: 0;
          display: block;
          font: ${theme.fontMono14Normal};
          color: ${theme.colorObjectDefault};
        }
      `}</style>
    </>
  );
};
