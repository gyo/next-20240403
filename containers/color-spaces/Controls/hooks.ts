import { useInputNumber } from "../../../hooks/useInput";
import { Props as ComponentProps } from "./index";

type ControllableValue = {
  defaultValue: number;
  max: number;
  step: number;
};

type StaticValue = {
  staticValue: string;
};

type Value = { label: string } & (ControllableValue | StaticValue);

type UseControls = (
  value1?: Value,
  value2?: Value,
  value3?: Value,
) => {
  componentProps: ComponentProps;
  values: [number, number, number];
};

export const useControls: UseControls = (propsValue1, propsValue2, propsValue3) => {
  const { value: value1, handleChangeValue: handleChangeValue1 } = useInputNumber(
    propsValue1 != null && "defaultValue" in propsValue1 ? propsValue1.defaultValue : 0,
  );

  const { value: value2, handleChangeValue: handleChangeValue2 } = useInputNumber(
    propsValue2 != null && "defaultValue" in propsValue2 ? propsValue2.defaultValue : 0,
  );

  const { value: value3, handleChangeValue: handleChangeValue3 } = useInputNumber(
    propsValue3 != null && "defaultValue" in propsValue3 ? propsValue3.defaultValue : 0,
  );

  return {
    componentProps: {
      value1:
        propsValue1 != null
          ? "defaultValue" in propsValue1
            ? { label: propsValue1.label, value: value1, max: propsValue1.max, step: propsValue1.step, handleChangeValue: handleChangeValue1 }
            : { label: propsValue1.label, staticValue: propsValue1.staticValue }
          : undefined,
      value2:
        propsValue2 != null
          ? "defaultValue" in propsValue2
            ? { label: propsValue2.label, value: value2, max: propsValue2.max, step: propsValue2.step, handleChangeValue: handleChangeValue2 }
            : { label: propsValue2.label, staticValue: propsValue2.staticValue }
          : undefined,
      value3:
        propsValue3 != null
          ? "defaultValue" in propsValue3
            ? { label: propsValue3.label, value: value3, max: propsValue3.max, step: propsValue3.step, handleChangeValue: handleChangeValue3 }
            : { label: propsValue3.label, staticValue: propsValue3.staticValue }
          : undefined,
    },
    values: [value1, value2, value3],
  };
};
