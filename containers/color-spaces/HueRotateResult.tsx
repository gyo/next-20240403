import { FC } from "react";
import { useTheme } from "../../contexts/theme";

const hueMax = 360;
const hueLength = 8;
const hueList = new Array(hueLength).fill(0).map((_, i) => (hueMax / hueLength) * i);

type Props = {
  buildColor: (hue: number) => string;
};

export const HueRotateResult: FC<Props> = ({ buildColor }) => {
  const theme = useTheme();

  return (
    <>
      <output className="hue-rotate-result">
        {hueList.map((hue) => {
          const background = buildColor(hue);
          return (
            <div className="hue-rotate-result__block" style={{ background }} key={background}>
              <p className="hue-rotate-result__color-default">通常文字</p>
              <p className="hue-rotate-result__color-inverted">反転文字</p>
            </div>
          );
        })}
      </output>

      <style jsx>{`
        .hue-rotate-result {
          display: flex;
          gap: ${theme.size016};
          overflow: auto;
        }
        .hue-rotate-result__block {
          flex-shrink: 0;
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
          gap: ${theme.size008};
          inline-size: ${theme.size136};
          block-size: ${theme.size136};
          border-radius: ${theme.size008};
          font: ${theme.fontSans16Normal};
        }
        .hue-rotate-result__color-default {
          color: ${theme.colorObjectDefault};
        }
        .hue-rotate-result__color-inverted {
          color: ${theme.colorObjectInverted};
        }
      `}</style>
    </>
  );
};
