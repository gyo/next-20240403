import Head from "next/head";
import Link from "next/link";
import { AppWrapper } from "../components/layout/AppWrapper";
import { useTheme } from "../contexts/theme";

export const Page = () => {
  const theme = useTheme();

  return (
    <>
      <Head>
        <title>Dynamic Theming</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <AppWrapper>
        <div className="section">
          <h1>Dynamic Theming</h1>

          <nav>
            <ul>
              <li>
                <Link href="./sample-pages">
                  <span className="link">ページサンプル</span>
                </Link>
              </li>
              <li>
                <Link href="./custom-properties">
                  <span className="link">CSS変数一覧</span>
                </Link>
              </li>
              <li>
                <Link href="./tokens">
                  <span className="link">デザイントークン</span>
                </Link>
              </li>
              <li>
                <Link href="./color-spaces">
                  <span className="link">色空間について</span>
                </Link>
              </li>
            </ul>
          </nav>

          <a href="https://gitlab.com/gyo/dynamic-theme" target="_blank" rel="noopener noreferrer">
            README
          </a>
        </div>
      </AppWrapper>

      <style jsx>{`
        .section {
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size016};
        }
        h1 {
          font: ${theme.fontSans32Bold};
          color: ${theme.colorObjectDefault};
        }
        ul {
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size004};
        }
        .link {
          font: ${theme.fontSans16Normal};
          color: ${theme.colorObjectLink};
        }
      `}</style>
    </>
  );
};
