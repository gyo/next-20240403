import Head from "next/head";
import Link from "next/link";
import { HeadingLevel1 } from "../../components/general/HeadingLevel1";
import { AppWrapper } from "../../components/layout/AppWrapper";
import { useTheme, useThemeCustomPropertyDefinitionsStructured } from "../../contexts/theme";

export const Page = () => {
  const theme = useTheme();

  const structuredCustomPropertyDefinitions = useThemeCustomPropertyDefinitionsStructured();

  return (
    <>
      <Head>
        <title>CSS変数一覧</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <AppWrapper>
        <div>
          <Link href="../">
            <span className="link">{"<< "}インデックスページ</span>
          </Link>
        </div>

        <section>
          <HeadingLevel1>CSS変数一覧</HeadingLevel1>

          <ul>
            {Object.entries(structuredCustomPropertyDefinitions).map(([property, value]) => (
              <li key={`${property}${value}`}>
                <div>{property}</div>
                <div>{value}</div>
              </li>
            ))}
          </ul>
        </section>
      </AppWrapper>

      <style jsx>{`
        .link {
          font: ${theme.fontSans16Normal};
          color: ${theme.colorObjectLink};
        }
        section {
          margin-block-start: ${theme.size032};
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size020};
        }
        ul {
          display: grid;
          grid-template-columns: auto 1fr;
          gap: ${theme.size008};
        }
        li {
          display: grid;
          grid-template-columns: subgrid;
          grid-column: span 2;
          font: ${theme.fontSans16Normal};
          color: ${theme.colorObjectDefault};
        }
      `}</style>
    </>
  );
};
