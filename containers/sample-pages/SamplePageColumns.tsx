import { FC } from "react";
import { CardList } from "../../components/sample-pages/CardList";
import { Community } from "../../components/sample-pages/Community";
import { Form } from "../../components/sample-pages/Form";
import { Home } from "../../components/sample-pages/Home";
import { ColorSchemeCssValue } from "../../contexts/color-scheme/types";
import { useTheme } from "../../contexts/theme";

type Props = {
  forceColorScheme?: ColorSchemeCssValue;
};

export const SamplePageColumns: FC<Props> = ({ forceColorScheme }) => {
  const theme = useTheme();

  const rootClassNames =
    forceColorScheme === "light" ? "page-columns page-columns--light" : forceColorScheme === "dark" ? "page-columns page-columns--dark" : "page-columns";

  return (
    <>
      <div className={rootClassNames}>
        <div className="page-columns__item">
          <Home />
        </div>
        <div className="page-columns__item">
          <CardList />
        </div>
        <div className="page-columns__item">
          <Community />
        </div>
        <div className="page-columns__item">
          <Form />
        </div>
      </div>

      <style jsx>{`
        .page-columns {
          display: flex;
          gap: ${theme.size016};
          overflow: auto;
          max-inline-size: 100%;
        }
        .page-columns--light {
          color-scheme: light;
        }
        .page-columns--dark {
          color-scheme: dark;
        }
        .page-columns__item {
          flex-shrink: 0;
          inline-size: 375px;
          block-size: 700px;
          overflow: auto;
        }
      `}</style>
    </>
  );
};
