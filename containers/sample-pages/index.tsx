import Head from "next/head";
import Link from "next/link";
import { HeadingLevel1 } from "../../components/general/HeadingLevel1";
import { HeadingLevel2 } from "../../components/general/HeadingLevel2";
import { AppWrapper } from "../../components/layout/AppWrapper";
import { useColorScheme } from "../../contexts/color-scheme";
import { useTheme } from "../../contexts/theme";
import { SamplePageColumns } from "./SamplePageColumns";

export const Page = () => {
  const { colorSchemeCssValue } = useColorScheme();

  const theme = useTheme();

  return (
    <>
      <Head>
        <title>ページサンプル</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <AppWrapper>
        <div>
          <Link href="../">
            <span className="link">{"<< "}インデックスページ</span>
          </Link>
        </div>

        <section className="section-level-1">
          <HeadingLevel1>ページサンプル</HeadingLevel1>

          <section className="section-level-2">
            <HeadingLevel2>システムテーマ</HeadingLevel2>
            <SamplePageColumns />
          </section>

          <section className="section-level-2 light">
            <HeadingLevel2>ライトテーマ</HeadingLevel2>
            <SamplePageColumns forceColorScheme="light" />
          </section>

          <section className="section-level-2 dark">
            <HeadingLevel2>ダークテーマ</HeadingLevel2>
            <SamplePageColumns forceColorScheme="dark" />
          </section>
        </section>
      </AppWrapper>

      <style jsx>{`
        .link {
          font: ${theme.fontSans16Normal};
          color: ${theme.colorObjectLink};
        }
        .section-level-1 {
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size032};
          margin-block-start: ${theme.size032};
        }
        .section-level-2 {
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size020};
        }
      `}</style>
    </>
  );
};
