import { FC } from "react";
import { HeadingLevel1 } from "../../components/general/HeadingLevel1";
import { useTheme, useThemeStructured } from "../../contexts/theme";

export const BorderRadiusSection: FC = () => {
  const theme = useTheme();

  const structuredTheme = useThemeStructured();
  const borderRadiusVariables = structuredTheme.borderRadius.variables;

  return (
    <>
      <section>
        <HeadingLevel1>Border Radius</HeadingLevel1>

        <div className="grid">
          {Object.entries(borderRadiusVariables).map(([key, variable]) => (
            <div key={key}>
              <div className="grid__text">{key}</div>
              <div className="grid__square" style={{ borderRadius: variable }}></div>
            </div>
          ))}
        </div>
      </section>

      <style jsx>{`
        section {
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size012};
        }
        .grid {
          display: flex;
          align-items: flex-start;
          flex-wrap: wrap;
          gap: ${theme.size008};
        }
        .grid__text {
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectDefault};
        }
        .grid__square {
          background: ${theme.colorObjectSubtle};
          block-size: ${theme.size052};
          inline-size: ${theme.size052};
        }
      `}</style>
    </>
  );
};
