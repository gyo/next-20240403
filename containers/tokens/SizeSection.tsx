import { FC } from "react";
import { HeadingLevel1 } from "../../components/general/HeadingLevel1";
import { useTheme, useThemeStructured } from "../../contexts/theme";

export const SizeSection: FC = () => {
  const theme = useTheme();

  const structuredTheme = useThemeStructured();
  const sizeVariables = structuredTheme.size.variables;

  return (
    <>
      <section>
        <HeadingLevel1>Size</HeadingLevel1>

        <div className="grid">
          {Object.entries(sizeVariables).map(([key, variable]) => (
            <div key={key}>
              <div className="grid__text">{key}</div>
              <div className="grid__square" style={{ blockSize: variable, inlineSize: variable }}></div>
            </div>
          ))}
        </div>
      </section>

      <style jsx>{`
        section {
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size012};
        }
        .grid {
          display: flex;
          align-items: flex-start;
          overflow-x: auto;
          gap: ${theme.size008};
        }
        .grid__text {
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectDefault};
        }
        .grid__square {
          background: ${theme.colorObjectSubtle};
        }
      `}</style>
    </>
  );
};
