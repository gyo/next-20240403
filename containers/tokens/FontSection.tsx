import { FC, Fragment } from "react";
import { HeadingLevel1 } from "../../components/general/HeadingLevel1";
import { useTheme, useThemeStructured } from "../../contexts/theme";

export const FontSection: FC = () => {
  const theme = useTheme();

  const structuredTheme = useThemeStructured();
  const fontVariables = structuredTheme.font.variables;

  return (
    <>
      <section>
        <HeadingLevel1>Font</HeadingLevel1>

        <div className="grid">
          {Object.entries(fontVariables).map(([key, variable]) => (
            <Fragment key={key}>
              <div className="grid__odd" style={{ font: variable }}>
                {key}
              </div>
              <div className="grid__even" style={{ font: variable }}>
                あのイーハトーヴォのすきとおった風、夏でも底に冷たさをもつ青いそら、うつくしい森で飾られたモリーオ市、郊外のぎらぎらひかる草の波。
              </div>
            </Fragment>
          ))}
        </div>
      </section>

      <style jsx>{`
        section {
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size012};
        }
        .grid {
          display: grid;
          grid-template-columns: auto 1fr;
          column-gap: ${theme.size012};
        }
        .grid__odd {
          color: ${theme.colorObjectDefault};
        }
        .grid__even {
          color: ${theme.colorObjectDefault};
        }
      `}</style>
    </>
  );
};
