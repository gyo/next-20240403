import { FC } from "react";
import { HeadingLevel1 } from "../../components/general/HeadingLevel1";
import { useTheme, useThemeStructured } from "../../contexts/theme";
import { ColorGridCell } from "./ColorGridCell";

export const PrimitiveColorSection: FC = () => {
  const theme = useTheme();

  const structuredTheme = useThemeStructured();
  const variables = structuredTheme.color.primitive.variables;

  return (
    <>
      <section>
        <HeadingLevel1>Primitive Color</HeadingLevel1>

        <div className="grid">
          <div className="grid__column">
            <ColorGridCell background={variables.gray01} backgroundName="gray01" color={variables.grayOnGray01} colorName="grayOnGray01" />
            <ColorGridCell background={variables.gray02} backgroundName="gray02" color={variables.grayOnGray02} colorName="grayOnGray02" />
            <ColorGridCell background={variables.gray03} backgroundName="gray03" color={variables.grayOnGray03} colorName="grayOnGray03" />
            <ColorGridCell background={variables.gray04} backgroundName="gray04" color={variables.grayOnGray04} colorName="grayOnGray04" />
            <ColorGridCell background={variables.gray05} backgroundName="gray05" color={variables.grayOnGray05} colorName="grayOnGray05" />
            <ColorGridCell background={variables.gray06} backgroundName="gray06" color={variables.grayOnGray06} colorName="grayOnGray06" />
            <ColorGridCell background={variables.gray07} backgroundName="gray07" color={variables.grayOnGray07} colorName="grayOnGray07" />
            <ColorGridCell background={variables.gray08} backgroundName="gray08" color={variables.grayOnGray08} colorName="grayOnGray08" />
            <ColorGridCell background={variables.gray09} backgroundName="gray09" color={variables.grayOnGray09} colorName="grayOnGray09" />
            <ColorGridCell background={variables.gray10} backgroundName="gray10" color={variables.grayOnGray10} colorName="grayOnGray10" />
            <ColorGridCell background={variables.gray11} backgroundName="gray11" color={variables.grayOnGray11} colorName="grayOnGray11" />
            <ColorGridCell background={variables.gray12} backgroundName="gray12" color={variables.grayOnGray12} colorName="grayOnGray12" />
            <ColorGridCell background={variables.gray13} backgroundName="gray13" color={variables.grayOnGray13} colorName="grayOnGray13" />
            <ColorGridCell background={variables.gray14} backgroundName="gray14" color={variables.grayOnGray14} colorName="grayOnGray14" />
            <ColorGridCell background={variables.gray15} backgroundName="gray15" color={variables.grayOnGray15} colorName="grayOnGray15" />
            <ColorGridCell background={variables.gray16} backgroundName="gray16" color={variables.grayOnGray16} colorName="grayOnGray16" />
            <ColorGridCell background={variables.gray17} backgroundName="gray17" color={variables.grayOnGray17} colorName="grayOnGray17" />
            <ColorGridCell background={variables.gray18} backgroundName="gray18" color={variables.grayOnGray18} colorName="grayOnGray18" />
            <ColorGridCell background={variables.gray19} backgroundName="gray19" color={variables.grayOnGray19} colorName="grayOnGray19" />
            <ColorGridCell background={variables.gray20} backgroundName="gray20" color={variables.grayOnGray20} colorName="grayOnGray20" />
          </div>

          <div className="grid__column">
            <ColorGridCell background={variables.grayAlpha01} backgroundName="grayAlpha01" color={variables.grayOnGray01} colorName="grayOnGray01" />
            <ColorGridCell background={variables.grayAlpha02} backgroundName="grayAlpha02" color={variables.grayOnGray02} colorName="grayOnGray02" />
            <ColorGridCell background={variables.grayAlpha03} backgroundName="grayAlpha03" color={variables.grayOnGray03} colorName="grayOnGray03" />
            <ColorGridCell background={variables.grayAlpha04} backgroundName="grayAlpha04" color={variables.grayOnGray04} colorName="grayOnGray04" />
            <ColorGridCell background={variables.grayAlpha05} backgroundName="grayAlpha05" color={variables.grayOnGray05} colorName="grayOnGray05" />
            <ColorGridCell background={variables.grayAlpha06} backgroundName="grayAlpha06" color={variables.grayOnGray06} colorName="grayOnGray06" />
            <ColorGridCell background={variables.grayAlpha07} backgroundName="grayAlpha07" color={variables.grayOnGray07} colorName="grayOnGray07" />
            <ColorGridCell background={variables.grayAlpha08} backgroundName="grayAlpha08" color={variables.grayOnGray08} colorName="grayOnGray08" />
            <ColorGridCell background={variables.grayAlpha09} backgroundName="grayAlpha09" color={variables.grayOnGray09} colorName="grayOnGray09" />
            <ColorGridCell background={variables.grayAlpha10} backgroundName="grayAlpha10" color={variables.grayOnGray10} colorName="grayOnGray10" />
            <ColorGridCell background={variables.grayAlpha11} backgroundName="grayAlpha11" color={variables.grayOnGray11} colorName="grayOnGray11" />
            <ColorGridCell background={variables.grayAlpha12} backgroundName="grayAlpha12" color={variables.grayOnGray12} colorName="grayOnGray12" />
            <ColorGridCell background={variables.grayAlpha13} backgroundName="grayAlpha13" color={variables.grayOnGray13} colorName="grayOnGray13" />
            <ColorGridCell background={variables.grayAlpha14} backgroundName="grayAlpha14" color={variables.grayOnGray14} colorName="grayOnGray14" />
            <ColorGridCell background={variables.grayAlpha15} backgroundName="grayAlpha15" color={variables.grayOnGray15} colorName="grayOnGray15" />
            <ColorGridCell background={variables.grayAlpha16} backgroundName="grayAlpha16" color={variables.grayOnGray16} colorName="grayOnGray16" />
            <ColorGridCell background={variables.grayAlpha17} backgroundName="grayAlpha17" color={variables.grayOnGray17} colorName="grayOnGray17" />
            <ColorGridCell background={variables.grayAlpha18} backgroundName="grayAlpha18" color={variables.grayOnGray18} colorName="grayOnGray18" />
            <ColorGridCell background={variables.grayAlpha19} backgroundName="grayAlpha19" color={variables.grayOnGray19} colorName="grayOnGray19" />
            <ColorGridCell background={variables.grayAlpha20} backgroundName="grayAlpha20" color={variables.grayOnGray20} colorName="grayOnGray20" />
          </div>

          <div className="grid__column">
            <ColorGridCell background={variables.red01} backgroundName="red01" color={variables.grayOnRed01} colorName="grayOnRed01" />
            <ColorGridCell background={variables.red02} backgroundName="red02" color={variables.grayOnRed02} colorName="grayOnRed02" />
            <ColorGridCell background={variables.red03} backgroundName="red03" color={variables.grayOnRed03} colorName="grayOnRed03" />
            <ColorGridCell background={variables.red04} backgroundName="red04" color={variables.grayOnRed04} colorName="grayOnRed04" />
            <ColorGridCell background={variables.red05} backgroundName="red05" color={variables.grayOnRed05} colorName="grayOnRed05" />
            <ColorGridCell background={variables.red06} backgroundName="red06" color={variables.grayOnRed06} colorName="grayOnRed06" />
            <ColorGridCell background={variables.red07} backgroundName="red07" color={variables.grayOnRed07} colorName="grayOnRed07" />
            <ColorGridCell background={variables.red08} backgroundName="red08" color={variables.grayOnRed08} colorName="grayOnRed08" />
            <ColorGridCell background={variables.red09} backgroundName="red09" color={variables.grayOnRed09} colorName="grayOnRed09" />
            <ColorGridCell background={variables.red10} backgroundName="red10" color={variables.grayOnRed10} colorName="grayOnRed10" />
            <ColorGridCell background={variables.red11} backgroundName="red11" color={variables.grayOnRed11} colorName="grayOnRed11" />
            <ColorGridCell background={variables.red12} backgroundName="red12" color={variables.grayOnRed12} colorName="grayOnRed12" />
            <ColorGridCell background={variables.red13} backgroundName="red13" color={variables.grayOnRed13} colorName="grayOnRed13" />
            <ColorGridCell background={variables.red14} backgroundName="red14" color={variables.grayOnRed14} colorName="grayOnRed14" />
            <ColorGridCell background={variables.red15} backgroundName="red15" color={variables.grayOnRed15} colorName="grayOnRed15" />
            <ColorGridCell background={variables.red16} backgroundName="red16" color={variables.grayOnRed16} colorName="grayOnRed16" />
            <ColorGridCell background={variables.red17} backgroundName="red17" color={variables.grayOnRed17} colorName="grayOnRed17" />
            <ColorGridCell background={variables.red18} backgroundName="red18" color={variables.grayOnRed18} colorName="grayOnRed18" />
            <ColorGridCell background={variables.red19} backgroundName="red19" color={variables.grayOnRed19} colorName="grayOnRed19" />
            <ColorGridCell background={variables.red20} backgroundName="red20" color={variables.grayOnRed20} colorName="grayOnRed20" />
          </div>

          <div className="grid__column">
            <ColorGridCell background={variables.blue01} backgroundName="blue01" color={variables.grayOnBlue01} colorName="grayOnBlue01" />
            <ColorGridCell background={variables.blue02} backgroundName="blue02" color={variables.grayOnBlue02} colorName="grayOnBlue02" />
            <ColorGridCell background={variables.blue03} backgroundName="blue03" color={variables.grayOnBlue03} colorName="grayOnBlue03" />
            <ColorGridCell background={variables.blue04} backgroundName="blue04" color={variables.grayOnBlue04} colorName="grayOnBlue04" />
            <ColorGridCell background={variables.blue05} backgroundName="blue05" color={variables.grayOnBlue05} colorName="grayOnBlue05" />
            <ColorGridCell background={variables.blue06} backgroundName="blue06" color={variables.grayOnBlue06} colorName="grayOnBlue06" />
            <ColorGridCell background={variables.blue07} backgroundName="blue07" color={variables.grayOnBlue07} colorName="grayOnBlue07" />
            <ColorGridCell background={variables.blue08} backgroundName="blue08" color={variables.grayOnBlue08} colorName="grayOnBlue08" />
            <ColorGridCell background={variables.blue09} backgroundName="blue09" color={variables.grayOnBlue09} colorName="grayOnBlue09" />
            <ColorGridCell background={variables.blue10} backgroundName="blue10" color={variables.grayOnBlue10} colorName="grayOnBlue10" />
            <ColorGridCell background={variables.blue11} backgroundName="blue11" color={variables.grayOnBlue11} colorName="grayOnBlue11" />
            <ColorGridCell background={variables.blue12} backgroundName="blue12" color={variables.grayOnBlue12} colorName="grayOnBlue12" />
            <ColorGridCell background={variables.blue13} backgroundName="blue13" color={variables.grayOnBlue13} colorName="grayOnBlue13" />
            <ColorGridCell background={variables.blue14} backgroundName="blue14" color={variables.grayOnBlue14} colorName="grayOnBlue14" />
            <ColorGridCell background={variables.blue15} backgroundName="blue15" color={variables.grayOnBlue15} colorName="grayOnBlue15" />
            <ColorGridCell background={variables.blue16} backgroundName="blue16" color={variables.grayOnBlue16} colorName="grayOnBlue16" />
            <ColorGridCell background={variables.blue17} backgroundName="blue17" color={variables.grayOnBlue17} colorName="grayOnBlue17" />
            <ColorGridCell background={variables.blue18} backgroundName="blue18" color={variables.grayOnBlue18} colorName="grayOnBlue18" />
            <ColorGridCell background={variables.blue19} backgroundName="blue19" color={variables.grayOnBlue19} colorName="grayOnBlue19" />
            <ColorGridCell background={variables.blue20} backgroundName="blue20" color={variables.grayOnBlue20} colorName="grayOnBlue20" />
          </div>

          <div className="grid__column">
            <ColorGridCell background={variables.theme01} backgroundName="theme01" color={variables.grayOnTheme01} colorName="grayOnTheme01" />
            <ColorGridCell background={variables.theme02} backgroundName="theme02" color={variables.grayOnTheme02} colorName="grayOnTheme02" />
            <ColorGridCell background={variables.theme03} backgroundName="theme03" color={variables.grayOnTheme03} colorName="grayOnTheme03" />
            <ColorGridCell background={variables.theme04} backgroundName="theme04" color={variables.grayOnTheme04} colorName="grayOnTheme04" />
            <ColorGridCell background={variables.theme05} backgroundName="theme05" color={variables.grayOnTheme05} colorName="grayOnTheme05" />
            <ColorGridCell background={variables.theme06} backgroundName="theme06" color={variables.grayOnTheme06} colorName="grayOnTheme06" />
            <ColorGridCell background={variables.theme07} backgroundName="theme07" color={variables.grayOnTheme07} colorName="grayOnTheme07" />
            <ColorGridCell background={variables.theme08} backgroundName="theme08" color={variables.grayOnTheme08} colorName="grayOnTheme08" />
            <ColorGridCell background={variables.theme09} backgroundName="theme09" color={variables.grayOnTheme09} colorName="grayOnTheme09" />
            <ColorGridCell background={variables.theme10} backgroundName="theme10" color={variables.grayOnTheme10} colorName="grayOnTheme10" />
            <ColorGridCell background={variables.theme11} backgroundName="theme11" color={variables.grayOnTheme11} colorName="grayOnTheme11" />
            <ColorGridCell background={variables.theme12} backgroundName="theme12" color={variables.grayOnTheme12} colorName="grayOnTheme12" />
            <ColorGridCell background={variables.theme13} backgroundName="theme13" color={variables.grayOnTheme13} colorName="grayOnTheme13" />
            <ColorGridCell background={variables.theme14} backgroundName="theme14" color={variables.grayOnTheme14} colorName="grayOnTheme14" />
            <ColorGridCell background={variables.theme15} backgroundName="theme15" color={variables.grayOnTheme15} colorName="grayOnTheme15" />
            <ColorGridCell background={variables.theme16} backgroundName="theme16" color={variables.grayOnTheme16} colorName="grayOnTheme16" />
            <ColorGridCell background={variables.theme17} backgroundName="theme17" color={variables.grayOnTheme17} colorName="grayOnTheme17" />
            <ColorGridCell background={variables.theme18} backgroundName="theme18" color={variables.grayOnTheme18} colorName="grayOnTheme18" />
            <ColorGridCell background={variables.theme19} backgroundName="theme19" color={variables.grayOnTheme19} colorName="grayOnTheme19" />
            <ColorGridCell background={variables.theme20} backgroundName="theme20" color={variables.grayOnTheme20} colorName="grayOnTheme20" />
          </div>

          <div className="grid__column">
            <ColorGridCell background={variables.gray01} backgroundName="gray01" color={variables.themeOnGray01} colorName="themeOnGray01" />
            <ColorGridCell background={variables.gray02} backgroundName="gray02" color={variables.themeOnGray02} colorName="themeOnGray02" />
            <ColorGridCell background={variables.gray03} backgroundName="gray03" color={variables.themeOnGray03} colorName="themeOnGray03" />
            <ColorGridCell background={variables.gray04} backgroundName="gray04" color={variables.themeOnGray04} colorName="themeOnGray04" />
            <ColorGridCell background={variables.gray05} backgroundName="gray05" color={variables.themeOnGray05} colorName="themeOnGray05" />
            <ColorGridCell background={variables.gray06} backgroundName="gray06" color={variables.themeOnGray06} colorName="themeOnGray06" />
            <ColorGridCell background={variables.gray07} backgroundName="gray07" color={variables.themeOnGray07} colorName="themeOnGray07" />
            <ColorGridCell background={variables.gray08} backgroundName="gray08" color={variables.themeOnGray08} colorName="themeOnGray08" />
            <ColorGridCell background={variables.gray09} backgroundName="gray09" color={variables.themeOnGray09} colorName="themeOnGray09" />
            <ColorGridCell background={variables.gray10} backgroundName="gray10" color={variables.themeOnGray10} colorName="themeOnGray10" />
            <ColorGridCell background={variables.gray11} backgroundName="gray11" color={variables.themeOnGray11} colorName="themeOnGray11" />
            <ColorGridCell background={variables.gray12} backgroundName="gray12" color={variables.themeOnGray12} colorName="themeOnGray12" />
            <ColorGridCell background={variables.gray13} backgroundName="gray13" color={variables.themeOnGray13} colorName="themeOnGray13" />
            <ColorGridCell background={variables.gray14} backgroundName="gray14" color={variables.themeOnGray14} colorName="themeOnGray14" />
            <ColorGridCell background={variables.gray15} backgroundName="gray15" color={variables.themeOnGray15} colorName="themeOnGray15" />
            <ColorGridCell background={variables.gray16} backgroundName="gray16" color={variables.themeOnGray16} colorName="themeOnGray16" />
            <ColorGridCell background={variables.gray17} backgroundName="gray17" color={variables.themeOnGray17} colorName="themeOnGray17" />
            <ColorGridCell background={variables.gray18} backgroundName="gray18" color={variables.themeOnGray18} colorName="themeOnGray18" />
            <ColorGridCell background={variables.gray19} backgroundName="gray19" color={variables.themeOnGray19} colorName="themeOnGray19" />
            <ColorGridCell background={variables.gray20} backgroundName="gray20" color={variables.themeOnGray20} colorName="themeOnGray20" />
          </div>
        </div>
      </section>

      <style jsx>{`
        section {
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size012};
        }
        .grid {
          display: flex;
          overflow-x: auto;
        }
        .grid__column {
          flex-shrink: 0;
        }
        .grid__cell {
          display: flex;
          align-items: center;
          justify-content: space-evenly;
          gap: ${theme.size008};
          inline-size: ${theme.size084};
          block-size: ${theme.size032};
        }
        .grid__text {
          font: ${theme.fontSans14Normal};
        }
        .grid__text--default {
          color: ${theme.colorObjectDefault};
        }
        .grid__text--inverted {
          color: ${theme.colorObjectInverted};
        }
      `}</style>
    </>
  );
};
