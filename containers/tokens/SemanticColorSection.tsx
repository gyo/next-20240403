import { FC } from "react";
import { HeadingLevel1 } from "../../components/general/HeadingLevel1";
import { useTheme } from "../../contexts/theme";
import { ColorGridCell } from "./ColorGridCell";

export const SemanticColorSection: FC = () => {
  const theme = useTheme();

  return (
    <>
      <section>
        <HeadingLevel1>Semantic Color</HeadingLevel1>

        <div className="grid">
          <div className="grid__column">
            <ColorGridCell background={theme.colorBackgroundDefault} color={theme.colorObjectDefault} backgroundName="colorBackgroundDefault" />
            <ColorGridCell background={theme.colorBackgroundDefault} color={theme.colorObjectDefault} colorName="colorObjectDefault" />
            <ColorGridCell background={theme.colorBackgroundDefault} color={theme.colorObjectSubtle} colorName="colorObjectSubtle" />
            <ColorGridCell background={theme.colorBackgroundDefault} color={theme.colorObjectError} colorName="colorObjectError" />
            <ColorGridCell background={theme.colorBackgroundDefault} color={theme.colorObjectLink} colorName="colorObjectLink" />
            <ColorGridCell background={theme.colorBackgroundDefault} color={theme.colorObjectTheme} colorName="colorObjectTheme" />
            <ColorGridCell background={theme.colorBackgroundDefault} color={theme.colorSurfaceTheme} colorName="colorSurfaceTheme" />
            <ColorGridCell background={theme.colorBackgroundDefault} color={theme.colorSurfaceThemeSubtle} colorName="colorSurfaceThemeSubtle" />
            <ColorGridCell background={theme.colorBackgroundDefault} color={theme.colorSurfaceRequired} colorName="colorSurfaceRequired" />
          </div>

          <div className="grid__column">
            <ColorGridCell background={theme.colorBackgroundSubtle} color={theme.colorObjectDefault} backgroundName="colorBackgroundSubtle" />
            <ColorGridCell background={theme.colorBackgroundSubtle} color={theme.colorObjectDefault} colorName="colorObjectDefault" />
            <ColorGridCell background={theme.colorBackgroundSubtle} color={theme.colorObjectSubtle} colorName="colorObjectSubtle" />
            <ColorGridCell background={theme.colorBackgroundSubtle} color={theme.colorObjectError} colorName="colorObjectError" />
            <ColorGridCell background={theme.colorBackgroundSubtle} color={theme.colorObjectLink} colorName="colorObjectLink" />
            <ColorGridCell background={theme.colorBackgroundSubtle} color={theme.colorObjectTheme} colorName="colorObjectTheme" />
            <ColorGridCell background={theme.colorBackgroundSubtle} color={theme.colorSurfaceTheme} colorName="colorSurfaceTheme" />
            <ColorGridCell background={theme.colorBackgroundSubtle} color={theme.colorSurfaceThemeSubtle} colorName="colorSurfaceThemeSubtle" />
            <ColorGridCell background={theme.colorBackgroundSubtle} color={theme.colorSurfaceRequired} colorName="colorSurfaceRequired" />
          </div>

          <div className="grid__column">
            <ColorGridCell background={theme.colorBackgroundInverted} color={theme.colorObjectInverted} backgroundName="colorBackgroundInverted" />
            <ColorGridCell background={theme.colorBackgroundInverted} color={theme.colorObjectInverted} colorName="colorObjectInverted" />
          </div>

          <div className="grid__column">
            <ColorGridCell background={theme.colorSurfaceTheme} color={theme.colorObjectOnTheme} backgroundName="colorSurfaceTheme" />
            <ColorGridCell background={theme.colorSurfaceTheme} color={theme.colorObjectOnTheme} colorName="colorObjectOnTheme" />
          </div>

          <div className="grid__column">
            <ColorGridCell background={theme.colorSurfaceThemeSubtle} color={theme.colorObjectOnThemeSubtle} backgroundName="colorSurfaceThemeSubtle" />
            <ColorGridCell background={theme.colorSurfaceThemeSubtle} color={theme.colorObjectOnThemeSubtle} colorName="colorObjectOnThemeSubtle" />
          </div>

          <div className="grid__column">
            <ColorGridCell background={theme.colorSurfaceRequired} color={theme.colorObjectOnRequired} backgroundName="colorSurfaceRequired" />
            <ColorGridCell background={theme.colorSurfaceRequired} color={theme.colorObjectOnRequired} colorName="colorObjectOnRequired" />
          </div>
        </div>
      </section>

      <style jsx>{`
        section {
          display: grid;
          grid-template-columns: 1fr;
          gap: ${theme.size012};
        }
        .grid {
          display: flex;
          overflow-x: auto;
        }
        .grid__column {
          flex-shrink: 0;
        }
        .grid__cell {
          display: flex;
          align-items: center;
          justify-content: center;
          inline-size: ${theme.size084};
          block-size: ${theme.size032};
        }
        .grid__text {
          font: ${theme.fontSans14Normal};
        }
      `}</style>
    </>
  );
};
