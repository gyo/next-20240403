import { FC } from "react";
import { useTheme } from "../../contexts/theme";

type Props = {
  background?: string;
  backgroundName?: string;
  color: string;
  colorName?: string;
};

export const ColorGridCell: FC<Props> = ({ background = "transparent", backgroundName, color, colorName }) => {
  const theme = useTheme();

  return (
    <>
      <div className="cell" style={{ background, color }}>
        {backgroundName != null ? <div className="cell__text">背景：{backgroundName}</div> : null}
        {colorName != null ? <div className="cell__text">文字：{colorName}</div> : null}
      </div>

      <style jsx>{`
        .cell {
          padding-block: ${theme.size004};
          padding-inline: ${theme.size012};
          inline-size: ${theme.size220};
        }
        .cell__text {
          font: ${theme.fontSans14Normal};
          white-space: nowrap;
        }
      `}</style>
    </>
  );
};
