import Head from "next/head";
import Link from "next/link";
import { AppWrapper } from "../../components/layout/AppWrapper";
import { useTheme } from "../../contexts/theme";
import { BorderRadiusSection } from "./BorderRadiusSection";
import { FontSection } from "./FontSection";
import { PrimitiveColorSection } from "./PrimitiveColorSection";
import { SemanticColorSection } from "./SemanticColorSection";
import { ShadowSection } from "./ShadowSection";
import { SizeSection } from "./SizeSection";

export const Page = () => {
  const theme = useTheme();

  return (
    <>
      <Head>
        <title>デザイントークン</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <AppWrapper>
        <div>
          <Link href="../">
            <span className="link">{"<< "}インデックスページ</span>
          </Link>
        </div>

        <div className="section">
          <PrimitiveColorSection />
        </div>

        <div className="section">
          <SemanticColorSection />
        </div>

        <div className="section">
          <SizeSection />
        </div>

        <div className="section">
          <BorderRadiusSection />
        </div>

        <div className="section">
          <ShadowSection />
        </div>

        <div className="section">
          <FontSection />
        </div>
      </AppWrapper>

      <style jsx>{`
        .link {
          font: ${theme.fontSans16Normal};
          color: ${theme.colorObjectLink};
        }
        .section {
          margin-block-start: ${theme.size032};
        }
      `}</style>
    </>
  );
};
