import { AppProps } from "next/app";
import { useServerInsertedHTML } from "next/navigation";
import { ReactNode, useState } from "react";
import { StyleRegistry, createStyleRegistry } from "styled-jsx";
import { ThemeControl } from "../components/form/ThemeControl";
import { GlobalStyle } from "../components/utilities/GlobalStyle";
import { ColorSchemeProvider } from "../contexts/color-scheme/Provider";
import { ThemeProvider } from "../contexts/theme/Provider";
import { VerticalModeProvider } from "../contexts/vertical-mode/Provider";

function StyledJsxRegistry({ children }: { children: ReactNode }) {
  // Only create stylesheet once with lazy initial state
  // x-ref: https://reactjs.org/docs/hooks-reference.html#lazy-initial-state
  const [jsxStyleRegistry] = useState(() => createStyleRegistry());

  useServerInsertedHTML(() => {
    const styles = jsxStyleRegistry.styles();
    jsxStyleRegistry.flush();
    return <>{styles}</>;
  });

  return <StyleRegistry registry={jsxStyleRegistry}>{children}</StyleRegistry>;
}

export default function App({ Component, pageProps }: AppProps) {
  const colorScheme = "system";
  const baseSize = 16;
  const lightness = 50;
  const chroma = 0.18;
  const hue = 120;
  const verticalMode = "horizontal-tb";

  return (
    <StyledJsxRegistry>
      <ColorSchemeProvider colorScheme={colorScheme}>
        <ThemeProvider baseSize={baseSize} lightness={lightness} chroma={chroma} hue={hue}>
          <VerticalModeProvider verticalMode={verticalMode}>
            <GlobalStyle />
            <Component {...pageProps} />
            <ThemeControl size={baseSize} lightness={lightness} chroma={chroma} hue={hue} />
          </VerticalModeProvider>
        </ThemeProvider>
      </ColorSchemeProvider>
    </StyledJsxRegistry>
  );
}
