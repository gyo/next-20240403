import { FC } from "react";
import { useTheme } from "../../../contexts/theme";

export const Avatar: FC = () => {
  const theme = useTheme();

  return (
    <>
      <div className="avatar">
        <img src="/sample.png" alt="サンプル画像" />
      </div>

      <style jsx>{`
        .avatar {
          --c-size: ${theme.size084};
          block-size: var(--c-size);
          inline-size: var(--c-size);
          border: 1px solid ${theme.colorBorderObvious};
          border-radius: 9999px;
        }
        .avatar > img {
          block-size: 100%;
          inline-size: 100%;
          border-radius: 9999px;
          object-fit: cover;
        }
      `}</style>
    </>
  );
};
