import { FC, useId } from "react";
import { AiOutlineSetting } from "react-icons/ai";
import { useColorScheme, useColorSchemeDispatch } from "../../../contexts/color-scheme";
import { useTheme, useThemeDispatch } from "../../../contexts/theme";
import { useVerticalMode, useVerticalModeDispatch } from "../../../contexts/vertical-mode";
import { useInputNumber } from "../../../hooks/useInput";
import { useToggle } from "../../../hooks/useToggle";

type Props = {
  size: number;
  lightness: number;
  chroma: number;
  hue: number;
};

export const ThemeControl: FC<Props> = ({ size: propsSize, lightness: propsLightness, chroma: propsChroma, hue: propsHue }) => {
  const colorSchemeSystemId = useId();
  const colorSchemeLightId = useId();
  const colorSchemeDarkId = useId();
  const verticalModeHorizontalTbId = useId();
  const verticalModeVerticalRlId = useId();
  const sizeId = useId();
  const lightnessId = useId();
  const chromaId = useId();
  const hueId = useId();

  const { colorScheme } = useColorScheme();
  const updateColorScheme = useColorSchemeDispatch();

  const theme = useTheme();
  const updateTheme = useThemeDispatch();

  const verticalMode = useVerticalMode();
  const updateVerticalMode = useVerticalModeDispatch();

  const { value: size, handleChangeValue: handleChangeSize } = useInputNumber(propsSize, (value) => {
    updateTheme(value, lightness, chroma, hue);
  });

  const { value: lightness, handleChangeValue: handleChangeLightness } = useInputNumber(propsLightness, (value) => {
    updateTheme(size, value, chroma, hue);
  });

  const { value: chroma, handleChangeValue: handleChangeChroma } = useInputNumber(propsChroma, (value) => {
    updateTheme(size, lightness, value, hue);
  });

  const { value: hue, handleChangeValue: handleChangeHue } = useInputNumber(propsHue, (value) => {
    updateTheme(size, lightness, chroma, value);
  });

  const { opened, toggle } = useToggle();

  return (
    <>
      <div className="theme-control">
        <button className="theme-control__trigger" onClick={toggle} type="button">
          <AiOutlineSetting />
        </button>

        {opened ? (
          <div className="theme-control__editor">
            <div className="theme-control__title">テーマ</div>

            <div className="theme-control__control">
              <div className="theme-control__input">
                <input
                  type="radio"
                  name="color-scheme"
                  value="system"
                  id={colorSchemeSystemId}
                  checked={colorScheme === "system"}
                  onChange={() => {
                    updateColorScheme("system");
                  }}
                />
                <label htmlFor={colorSchemeSystemId}>システム</label>
              </div>

              <div className="theme-control__input">
                <input
                  type="radio"
                  name="color-scheme"
                  value="light"
                  id={colorSchemeLightId}
                  checked={colorScheme === "light"}
                  onChange={() => {
                    updateColorScheme("light");
                  }}
                />
                <label htmlFor={colorSchemeLightId}>ライト</label>
              </div>

              <div className="theme-control__input">
                <input
                  type="radio"
                  name="color-scheme"
                  value="dark"
                  id={colorSchemeDarkId}
                  checked={colorScheme === "dark"}
                  onChange={() => {
                    updateColorScheme("dark");
                  }}
                />
                <label htmlFor={colorSchemeDarkId}>ダーク</label>
              </div>
            </div>

            <div className="theme-control__title">書字方向</div>

            <div className="theme-control__control">
              <div className="theme-control__input">
                <input
                  type="radio"
                  name="vertical-mode"
                  value="horizontal-tb"
                  id={verticalModeHorizontalTbId}
                  checked={verticalMode === "horizontal-tb"}
                  onChange={() => {
                    updateVerticalMode("horizontal-tb");
                  }}
                />
                <label htmlFor={verticalModeHorizontalTbId}>横書き</label>
              </div>

              <div className="theme-control__input">
                <input
                  type="radio"
                  name="vertical-mode"
                  value="vertical-rl"
                  id={verticalModeVerticalRlId}
                  checked={verticalMode === "vertical-rl"}
                  onChange={() => {
                    updateVerticalMode("vertical-rl");
                  }}
                />
                <label htmlFor={verticalModeVerticalRlId}>縦書き</label>
              </div>
            </div>

            <label className="theme-control__title" htmlFor={sizeId}>
              サイズ
            </label>

            <div className="theme-control__control">
              <span className="theme-control__input">
                <input id={sizeId} type="number" min="10" max="32" step="1" value={size} onChange={handleChangeSize} />
              </span>
            </div>

            <label className="theme-control__title" htmlFor={lightnessId}>
              明度
            </label>

            <div className="theme-control__control">
              <span className="theme-control__input">
                <input id={lightnessId} type="range" min="0" max="100" step="1" value={lightness} onChange={handleChangeLightness} />
                <output>{lightness}</output>
              </span>
            </div>

            <label className="theme-control__title" htmlFor={chromaId}>
              彩度
            </label>

            <div className="theme-control__control">
              <span className="theme-control__input">
                <input id={chromaId} type="range" min="0" max="0.5" step="0.002" value={chroma} onChange={handleChangeChroma} />
                <output>{chroma.toFixed(3)}</output>
              </span>
            </div>

            <label className="theme-control__title" htmlFor={hueId}>
              色相角
            </label>

            <div className="theme-control__control">
              <span className="theme-control__input">
                <input id={hueId} type="range" min="0" max="360" step="1" value={hue} onChange={handleChangeHue} />
                <output>{hue}</output>
              </span>
            </div>
          </div>
        ) : null}
      </div>

      <style jsx>{`
        .theme-control {
          position: fixed;
          inset-block-start: 0;
          inset-inline-end: 0;
          display: grid;
          grid-template-columns: auto;
          justify-items: end;
          gap: ${theme.size016};
          padding: ${theme.size016};
          pointer-events: none;
        }
        .theme-control__trigger {
          display: block;
          border-radius: 9999px;
          padding: ${theme.size012};
          background: ${theme.colorBackgroundDefault};
          font: ${theme.fontSans32Normal};
          line-height: 0;
          color: ${theme.colorObjectDefault};
          box-shadow: ${theme.boxShadow8};
          pointer-events: auto;
        }
        .theme-control__editor {
          display: grid;
          grid-template-columns: auto auto;
          align-items: center;
          column-gap: ${theme.size016};
          row-gap: ${theme.size004};
          padding-block: ${theme.size008};
          padding-inline: ${theme.size016};
          background: ${theme.colorBackgroundDefault};
          box-shadow: ${theme.boxShadow8};
          pointer-events: auto;
        }
        .theme-control__title {
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectDefault};
        }
        .theme-control__control {
          display: flex;
          gap: ${theme.size008};
        }
        .theme-control__input {
          display: grid;
          grid-template-columns: auto auto;
          align-items: center;
          column-gap: ${theme.size004};
        }
        input[type="range"] {
          inline-size: ${theme.size220};
        }
        label {
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectDefault};
        }
        output {
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectDefault};
        }
      `}</style>
    </>
  );
};
