import { FC, ReactNode, useId } from "react";
import { useTheme } from "../../../contexts/theme";

type Props = JSX.IntrinsicElements["input"] & {
  label: ReactNode;
  required?: boolean;
  description?: ReactNode;
  error?: ReactNode;
};

export const TextField: FC<Props> = ({ label, required, description, error, ...props }) => {
  const theme = useTheme();

  const id = useId();

  return (
    <>
      <div className={`text-field${error != null ? " text-field--invalid" : ""}`}>
        <label className="text-field__label" htmlFor={id}>
          <span className="text-field__label-text">{label}</span>
          {required != null ? <span className="text-field__label-required">必須</span> : null}
        </label>

        <input className="text-field__input" id={id} {...props} />

        {description != null ? <p className="text-field__description">{description}</p> : null}

        {error != null ? <p className="text-field__error">{error}</p> : null}
      </div>

      <style jsx>{`
        .text-field {
          display: grid;
          grid-template-columns: 100%;
          gap: ${theme.size002};
        }
        .text-field__label {
          display: flex;
          align-items: center;
          gap: ${theme.size004};
        }
        .text-field__label-text {
          font: ${theme.fontSans12Normal};
          color: ${theme.colorObjectDefault};
        }
        .text-field__label-required {
          border-radius: 9999px;
          padding-inline: ${theme.size008};
          background: ${theme.colorSurfaceRequired};
          font: ${theme.fontSans11Normal};
          color: ${theme.colorObjectOnRequired};
        }
        .text-field__input {
          --c-border-width: 1px;
          inline-size: 100%;
          border: var(--c-border-width) solid ${theme.colorBorderDefault};
          border-radius: ${theme.size004};
          padding-block: calc(${theme.size008} - var(--c-border-width));
          padding-inline: calc(${theme.size016} - var(--c-border-width));
          background: ${theme.colorBackgroundSubtle};
          font: ${theme.fontSans16Normal};
          color: ${theme.colorObjectDefault};
        }
        .text-field__input:focus-visible {
          outline: none;
          border-color: ${theme.colorObjectTheme};
        }
        .text-field--invalid .text-field__input {
          border-color: ${theme.colorObjectError};
          color: ${theme.colorObjectError};
        }
        .text-field__description {
          font: ${theme.fontSans12Normal};
          color: ${theme.colorObjectSubtle};
        }
        .text-field__error {
          font: ${theme.fontSans12Normal};
          color: ${theme.colorObjectError};
        }
      `}</style>
    </>
  );
};
