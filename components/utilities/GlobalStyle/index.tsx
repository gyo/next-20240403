import { FC } from "react";
import { useColorScheme } from "../../../contexts/color-scheme";
import { useTheme, useThemeCustomPropertyDefinitions } from "../../../contexts/theme";
import { useVerticalMode } from "../../../contexts/vertical-mode";

export const GlobalStyle: FC = () => {
  const { colorSchemeCssValue } = useColorScheme();

  const theme = useTheme();

  const customPropertyDefinitions = useThemeCustomPropertyDefinitions();

  const verticalMode = useVerticalMode();

  return (
    <>
      <style jsx global>{`
        :root {
          color-scheme: ${colorSchemeCssValue};
        }
      `}</style>

      <style jsx global>{`
        :root {
          ${customPropertyDefinitions}
        }
      `}</style>

      <style jsx global>{`
        body {
          writing-mode: ${verticalMode};
        }
      `}</style>

      <style jsx global>{`
        *,
        *::before,
        *::after {
          box-sizing: border-box;
          margin: 0;
          padding: 0;
          border: none;
          text-decoration: none;
        }
        ul,
        ol {
          list-style: none;
        }
        img,
        picture {
          max-inline-size: 100%;
          display: block;
        }
        a {
          color: ${theme.colorObjectLink};
          text-decoration: underline;
        }
        a,
        button {
          cursor: pointer;
        }
        input,
        progress {
          accent-color: ${theme.colorObjectTheme};
        }
      `}</style>
    </>
  );
};
