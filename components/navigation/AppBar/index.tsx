import { FC } from "react";
import { AiOutlineBell, AiOutlineMenu } from "react-icons/ai";
import { useTheme } from "../../../contexts/theme";

type Props = {
  color?: "theme";
};

export const AppBar: FC<Props> = ({ color }) => {
  const theme = useTheme();

  const backgroundColorValue = color != null ? theme.colorSurfaceThemeSubtle : theme.colorBackgroundDefault;
  const textColorValue = color != null ? theme.colorObjectOnThemeSubtle : theme.colorObjectDefault;

  return (
    <>
      <div className="app-bar">
        <div className="app-bar__left">
          <AiOutlineMenu />
        </div>

        <div className="app-bar__center">タイトル</div>

        <div className="app-bar__right">
          <AiOutlineBell />
        </div>
      </div>

      <style jsx>{`
        .app-bar {
          position: sticky;
          inset-block-start: 0;
          display: grid;
          grid-template-columns: ${theme.size052} 1fr ${theme.size052};
          grid-template-rows: auto;
          align-items: center;
          justify-items: center;
          inline-size: 100%;
          block-size: ${theme.size052};
          background: ${backgroundColorValue};
        }
        .app-bar__left {
          grid-column: 1 / 2;
          font: ${theme.fontSans16Normal};
          color: ${textColorValue};
        }
        .app-bar__center {
          grid-column: 2 / 3;
          font: ${theme.fontSans16Normal};
          color: ${textColorValue};
        }
        .app-bar__right {
          grid-column: 3 / 4;
          font: ${theme.fontSans16Normal};
          color: ${textColorValue};
        }
      `}</style>
    </>
  );
};
