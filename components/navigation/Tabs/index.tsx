import { FC } from "react";
import { useTheme } from "../../../contexts/theme";

export const Tabs: FC = () => {
  const theme = useTheme();

  return (
    <>
      <div className="tabs">
        <div className="tabs__item tabs__item--active">タブ1</div>
        <div className="tabs__item">タブ2</div>
        <div className="tabs__item">タブ3</div>
      </div>

      <style jsx>{`
        .tabs {
          --c-padding-block: ${theme.size008};
          --c-default-border-width: 1px;
          --c-active-border-width: ${theme.size002};

          display: grid;
          grid-auto-columns: 1fr;
          grid-auto-flow: column;
        }
        .tabs__item {
          display: flex;
          align-items: center;
          justify-content: center;
          padding-block-start: var(--c-padding-block);
          padding-block-end: calc(var(--c-padding-block) - var(--c-default-border-width));
          border-block-end: var(--c-default-border-width) solid ${theme.colorBorderDefault};
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectDefault};
        }
        .tabs__item--active {
          padding-block-end: 0;
          padding-block-end: calc(var(--c-padding-block) - var(--c-active-border-width));
          border-block-end: var(--c-active-border-width) solid ${theme.colorObjectTheme};
          font: ${theme.fontSans14Bold};
          color: ${theme.colorObjectTheme};
        }
      `}</style>
    </>
  );
};
