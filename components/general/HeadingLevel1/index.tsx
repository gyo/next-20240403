import { FC } from "react";
import { useTheme } from "../../../contexts/theme";

type Props = JSX.IntrinsicElements["h1"];

export const HeadingLevel1: FC<Props> = ({ ...props }) => {
  const theme = useTheme();

  return (
    <>
      <h1 className="heading-level-1" {...props} />

      <style jsx>{`
        .heading-level-1 {
          font: ${theme.fontSans21Bold};
          color: ${theme.colorObjectDefault};
        }
      `}</style>
    </>
  );
};
