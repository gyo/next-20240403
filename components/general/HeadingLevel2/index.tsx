import { FC } from "react";
import { useTheme } from "../../../contexts/theme";

type Props = JSX.IntrinsicElements["h2"];

export const HeadingLevel2: FC<Props> = ({ ...props }) => {
  const theme = useTheme();

  return (
    <>
      <h2 className="heading-level-2" {...props} />

      <style jsx>{`
        .heading-level-2 {
          font: ${theme.fontSans18Bold};
          color: ${theme.colorObjectDefault};
        }
      `}</style>
    </>
  );
};
