import { FC } from "react";
import { useTheme } from "../../../contexts/theme";

type Props = JSX.IntrinsicElements["button"] & {
  size?: "medium" | "large";
  inlineSize?: "fit" | "full";
};

export const Button: FC<Props> = ({ size, inlineSize, ...restProps }) => {
  const theme = useTheme();

  const sizeModifierClassName = size === "large" ? " button--large" : "";
  const inlineSizeModifierClassName = inlineSize === "full" ? " button--full" : "";

  return (
    <>
      <button className={`button${sizeModifierClassName}${inlineSizeModifierClassName}`} {...restProps} />

      <style jsx>{`
        .button {
          --c-display: inline-flex;
          --c-padding-block: ${theme.size002};
          --c-padding-inline: ${theme.size016};
          --c-font: ${theme.fontSans16Normal};
          --c-border: 1px;
          display: var(--c-display);
          align-items: center;
          justify-content: center;
          border: var(--c-border) solid ${theme.colorSurfaceTheme};
          border-radius: 9999px;
          padding-block: calc(var(--c-padding-block) - var(--c-border));
          padding-inline: calc(var(--c-padding-inline) - var(--c-border));
          background: ${theme.colorSurfaceTheme};
          font: var(--c-font);
          color: ${theme.colorObjectOnTheme};
          text-decoration: none;
        }
        .button--large {
          --c-padding-block: ${theme.size004};
          --c-padding-inline: ${theme.size016};
          --c-font: ${theme.fontSans18Normal};
        }
        .button--full {
          --c-display: flex;
          inline-size: 100%;
        }
      `}</style>
    </>
  );
};
