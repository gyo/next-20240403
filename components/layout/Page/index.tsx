import { FC, ReactNode } from "react";
import { useTheme } from "../../../contexts/theme";

type Props = {
  children: ReactNode;
};

export const Page: FC<Props> = ({ children }) => {
  const theme = useTheme();

  return (
    <>
      <div className="page">{children}</div>

      <style jsx>{`
        .page {
          inline-size: 100%;
          min-block-size: 100%;
          background: ${theme.colorBackgroundDefault};
        }
      `}</style>
    </>
  );
};
