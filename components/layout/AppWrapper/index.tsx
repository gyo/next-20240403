import { FC, ReactNode } from "react";
import { useTheme } from "../../../contexts/theme";

type Props = {
  children: ReactNode;
};

export const AppWrapper: FC<Props> = ({ children }) => {
  const theme = useTheme();

  return (
    <>
      <div className="app-wrapper">{children}</div>

      <style jsx>{`
        .app-wrapper {
          min-block-size: 100dvb;
          padding: ${theme.size016};
          background: ${theme.colorBackgroundSubtle};
          font: ${theme.fontSans16Normal};
          color: ${theme.colorObjectDefault};
        }
      `}</style>
    </>
  );
};
