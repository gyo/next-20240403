import { FC } from "react";
import { useTheme } from "../../../contexts/theme";
import { Button } from "../../general/Button";

export const Banner: FC = () => {
  const theme = useTheme();

  return (
    <>
      <div className="banner">
        <div className="banner__title">タイトル</div>
        <div className="banner__body">これはサンプルの文章です。これはサンプルの文章です。</div>
        <div className="banner__button">
          <Button type="button">ボタン</Button>
        </div>
      </div>

      <style jsx>{`
        .banner {
          display: grid;
          grid-template-columns: 1fr;
          justify-items: center;
          border-radius: ${theme.borderRadius8};
          padding: ${theme.size016};
          background: ${theme.colorSurfaceThemeSubtle};
        }
        .banner__title {
          font: ${theme.fontSans14Bold};
          color: ${theme.colorObjectOnThemeSubtle};
        }
        .banner__body {
          padding-block-start: ${theme.size008};
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectOnThemeSubtle};
        }
        .banner__button {
          padding-block-start: ${theme.size002};
        }
      `}</style>
    </>
  );
};
