import { FC } from "react";
import { AiOutlineClose } from "react-icons/ai";
import { useTheme } from "../../../contexts/theme";

export const Notification: FC = () => {
  const theme = useTheme();

  return (
    <>
      <div className="notification">
        <div className="notification__button">
          <AiOutlineClose />
        </div>
        <div className="notification__title">タイトル</div>
        <div className="notification__body">これはサンプルの文章です。これはサンプルの文章です。</div>
      </div>

      <style jsx>{`
        .notification {
          position: sticky;
          inset-block-end: 0;
          inset-inline: 0;
          inline-size: 100%;
          display: grid;
          grid-template-columns: 1fr;
          padding: ${theme.size016};
          background: ${theme.colorSurfaceThemeSubtle};
        }
        .notification__button {
          position: absolute;
          inset-block-start: ${theme.size016};
          inset-inline-end: ${theme.size016};
          padding-block-start: ${theme.size002};
          font: ${theme.fontSans14Normal};
        }
        .notification__title {
          font: ${theme.fontSans14Bold};
          color: ${theme.colorObjectOnThemeSubtle};
        }
        .notification__body {
          padding-block-start: ${theme.size008};
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectOnThemeSubtle};
        }
      `}</style>
    </>
  );
};
