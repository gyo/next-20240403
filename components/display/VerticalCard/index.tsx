import { FC } from "react";
import { AiOutlineComment, AiOutlineShareAlt, AiOutlineSmile, AiOutlineStar } from "react-icons/ai";
import { useTheme } from "../../../contexts/theme";

export const VerticalCard: FC = () => {
  const theme = useTheme();

  return (
    <>
      <div className="vertical-card">
        <div className="vertical-card__image">
          <img src="/sample.png" alt="サンプル画像" />
        </div>

        <div className="vertical-card__title">タイトル</div>

        <div className="vertical-card__body">これはサンプルの文章です。これはサンプルの文章です。</div>

        <div className="vertical-card__footer">
          <div className="vertical-card__footer-item vertical-card__footer-item--active">
            <AiOutlineComment />
            <span>99</span>
          </div>

          <div className="vertical-card__footer-item">
            <AiOutlineStar />
            <span>99</span>
          </div>

          <div className="vertical-card__footer-item">
            <AiOutlineSmile />
            <span>99</span>
          </div>

          <div className="vertical-card__footer-item">
            <AiOutlineShareAlt />
            <span>99</span>
          </div>
        </div>
      </div>

      <style jsx>{`
        .vertical-card {
          border: 1px solid ${theme.colorBorderDefault};
          border-radius: ${theme.borderRadius8};
          background: ${theme.colorBackgroundDefault};
        }
        .vertical-card__image > img {
          display: block;
          border-start-start-radius: ${theme.borderRadius8};
          border-start-end-radius: ${theme.borderRadius8};
          inline-size: 100%;
          aspect-ratio: 16 / 9;
          object-fit: cover;
        }
        .vertical-card__title {
          padding-block-start: ${theme.size016};
          padding-inline: ${theme.size016};
          font: ${theme.fontSans16Normal};
          color: ${theme.colorObjectDefault};
        }
        .vertical-card__body {
          padding-block-start: ${theme.size012};
          padding-inline: ${theme.size016};
          padding-block-end: ${theme.size016};
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectDefault};
        }
        .vertical-card__footer {
          border-top: 1px solid ${theme.colorBorderDefault};
          display: flex;
          align-items: center;
          justify-content: space-between;
        }
        .vertical-card__footer-item {
          display: flex;
          align-items: center;
          gap: ${theme.size008};
          padding-block: ${theme.size012};
          padding-inline: ${theme.size016};
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectSubtle};
        }
        .vertical-card__footer-item--active {
          font: ${theme.fontSans14Bold};
          color: ${theme.colorObjectTheme};
        }
      `}</style>
    </>
  );
};
