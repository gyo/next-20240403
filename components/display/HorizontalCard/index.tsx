import { FC } from "react";
import { useTheme } from "../../../contexts/theme";

export const HorizontalCard: FC = () => {
  const theme = useTheme();

  return (
    <>
      <div className="horizontal-card">
        <div className="horizontal-card__media">
          <div className="horizontal-card__image">
            <img src="/sample.png" alt="サンプル画像" />
          </div>
        </div>

        <div className="horizontal-card__content">
          <div className="horizontal-card__title">タイトル</div>

          <div className="horizontal-card__body">これはサンプルの文章です。これはサンプルの文章です。</div>
        </div>
      </div>

      <style jsx>{`
        .horizontal-card {
          display: flex;
          border: 1px solid ${theme.colorBorderDefault};
          border-radius: ${theme.borderRadius8};
          background: ${theme.colorBackgroundDefault};
        }
        .horizontal-card__media {
          flex-shrink: 0;
        }
        .horizontal-card__image {
          block-size: 100%;
        }
        .horizontal-card__image > img {
          display: block;
          border-start-start-radius: ${theme.borderRadius8};
          border-end-start-radius: ${theme.borderRadius8};
          inline-size: ${theme.size084};
          block-size: 100%;
          object-fit: cover;
        }
        .horizontal-card__content {
          flex-grow: 1;
          padding: ${theme.size012};
        }
        .horizontal-card__title {
          padding-block-end: ${theme.size004};
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectDefault};
        }
        .horizontal-card__body {
          font: ${theme.fontSans13Normal};
          color: ${theme.colorObjectDefault};
        }
      `}</style>
    </>
  );
};
