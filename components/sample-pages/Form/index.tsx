import { FC } from "react";
import { useTheme } from "../../../contexts/theme";
import { Avatar } from "../../form/Avatar";
import { TextField } from "../../form/TextField";
import { Button } from "../../general/Button";
import { HeadingLevel1 } from "../../general/HeadingLevel1";
import { Page } from "../../layout/Page";
import { AppBar } from "../../navigation/AppBar";

export const Form: FC = () => {
  const theme = useTheme();

  return (
    <>
      <Page>
        <AppBar />

        <div className="heading-area">
          <HeadingLevel1>タイトル</HeadingLevel1>
        </div>

        <div className="avatar-area">
          <Avatar />
          <TextField label="タイトル" defaultValue="入力された値" />
        </div>

        <div className="input-area">
          <TextField label="タイトル" required={true} description="これはサンプルの文章です。" defaultValue="入力された値" />
          <TextField label="タイトル" placeholder="プレースホルダー" />
          <TextField label="タイトル" error="これはサンプルの文章です。" defaultValue="入力された値" />
          <TextField label="タイトル" />
        </div>

        <div className="submit-area">
          <Button size="large" inlineSize="full" type="button">
            ボタン
          </Button>

          <div className="button-note">これはサンプルの文章です。これはサンプルの文章です。</div>
        </div>
      </Page>

      <style jsx>{`
        .heading-area {
          padding-block: ${theme.size016};
          padding-inline: ${theme.size016};
        }
        .avatar-area {
          display: grid;
          grid-template-columns: auto 1fr;
          gap: ${theme.size016};
          align-items: end;
          padding-inline: ${theme.size016};
          padding-block-end: ${theme.size020};
        }
        .input-area {
          display: grid;
          grid-template-columns: 100%;
          gap: ${theme.size016};
          padding-inline: ${theme.size016};
          padding-block-end: ${theme.size032};
        }
        .submit-area {
          padding-inline: ${theme.size016};
          padding-block-end: ${theme.size016};
        }
        .button-note {
          margin-block-start: ${theme.size016};
          font: ${theme.fontSans12Normal};
          color: ${theme.colorObjectSubtle};
          text-align: center;
        }
      `}</style>
    </>
  );
};
