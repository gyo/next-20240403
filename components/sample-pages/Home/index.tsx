import { FC } from "react";
import { useTheme } from "../../../contexts/theme";
import { Banner } from "../../display/Banner";
import { HorizontalCard } from "../../display/HorizontalCard";
import { Page } from "../../layout/Page";
import { AppBar } from "../../navigation/AppBar";
import { Tabs } from "../../navigation/Tabs";

export const Home: FC = () => {
  const theme = useTheme();

  return (
    <>
      <Page>
        <AppBar />

        <div className="cover-area">
          <img src="/sample.png" alt="サンプル画像" />
        </div>

        <div className="tab-area">
          <Tabs />
        </div>

        <div className="banner-area">
          <Banner />
        </div>

        <div className="card-area">
          <HorizontalCard />
          <HorizontalCard />
          <HorizontalCard />
          <HorizontalCard />
        </div>
      </Page>

      <style jsx>{`
        .cover-area {
          padding-block-end: ${theme.size016};
        }
        .cover-area > img {
          display: block;
          inline-size: 100%;
          aspect-ratio: 3 / 1;
          object-fit: cover;
        }
        .tab-area {
          padding-block-end: ${theme.size016};
        }
        .banner-area {
          padding-inline: ${theme.size016};
          padding-block-end: ${theme.size016};
        }
        .card-area {
          padding-inline: ${theme.size016};
          padding-block-end: ${theme.size016};
          display: flex;
          flex-direction: column;
          gap: ${theme.size016};
        }
      `}</style>
    </>
  );
};
