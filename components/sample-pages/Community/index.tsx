import { FC } from "react";
import { useTheme } from "../../../contexts/theme";
import { HorizontalCard } from "../../display/HorizontalCard";
import { Button } from "../../general/Button";
import { HeadingLevel1 } from "../../general/HeadingLevel1";
import { HeadingLevel2 } from "../../general/HeadingLevel2";
import { Page } from "../../layout/Page";
import { AppBar } from "../../navigation/AppBar";

export const Community: FC = () => {
  const theme = useTheme();

  return (
    <>
      <Page>
        <AppBar color="theme" />

        <div className="cover-area">
          <img src="/sample.png" alt="サンプル画像" />
        </div>

        <div className="heading-1-area">
          <HeadingLevel1>タイトル</HeadingLevel1>
        </div>

        <div className="description-area">
          <p className="description">これはサンプルの文章です。これはサンプルの文章です。これはサンプルの文章です。これはサンプルの文章です。</p>
        </div>

        <div className="button-area">
          <Button size="large" inlineSize="full" type="button">
            ボタン
          </Button>

          <div className="button-note">これはサンプルの文章です。これはサンプルの文章です。</div>
        </div>

        <div className="heading-2-area">
          <HeadingLevel2>タイトル</HeadingLevel2>
        </div>

        <p className="body-area">
          これはサンプルの文章です。これはサンプルの文章です。これはサンプルの文章です。これはサンプルの文章です。
          <br />
          これはサンプルの文章です。これはサンプルの文章です。これはサンプルの文章です。これはサンプルの文章です。
          <br />
          これはサンプルの文章です。これはサンプルの文章です。これはサンプルの文章です。これはサンプルの文章です。
        </p>

        <div className="image-area">
          <div className="image-list">
            <div className="image-list__item">
              <img src="/sample.png" alt="サンプル画像" />
              <p>サンプル画像</p>
            </div>
            <div className="image-list__item">
              <img src="/sample.png" alt="サンプル画像" />
              <p>サンプル画像</p>
            </div>
          </div>
        </div>

        <div className="heading-2-area">
          <HeadingLevel2>タイトル</HeadingLevel2>
        </div>

        <div className="card-area">
          <HorizontalCard />
          <HorizontalCard />
        </div>
      </Page>

      <style jsx>{`
        .cover-area {
          padding: ${theme.size016};
          background: linear-gradient(
            to bottom,
            ${theme.colorSurfaceThemeSubtle} 0%,
            ${theme.colorSurfaceThemeSubtle} 50%,
            ${theme.colorBackgroundDefault} 50%,
            ${theme.colorBackgroundDefault} 100%
          );
        }
        .cover-area > img {
          display: block;
          border-radius: ${theme.borderRadius8};
          inline-size: 100%;
          aspect-ratio: 16 / 9;
          object-fit: cover;
        }
        .heading-1-area {
          padding-inline: ${theme.size016};
          padding-block-end: ${theme.size016};
        }
        .description-area {
          padding-inline: ${theme.size016};
          padding-block-end: ${theme.size016};
        }
        .description {
          font: ${theme.fontSans14Normal};
          color: ${theme.colorObjectDefault};
        }
        .button-area {
          padding: ${theme.size016};
        }
        .button-note {
          margin-block-start: ${theme.size016};
          font: ${theme.fontSans12Normal};
          color: ${theme.colorObjectSubtle};
          text-align: center;
        }
        .heading-2-area {
          padding-inline: ${theme.size016};
          padding-block-end: ${theme.size008};
        }
        .body-area {
          padding-inline: ${theme.size016};
          padding-block-end: ${theme.size012};
          font: ${theme.fontSans13Normal};
          color: ${theme.colorObjectDefault};
        }
        .image-area {
          padding-inline: ${theme.size016};
          padding-block-end: ${theme.size016};
        }
        .image-list {
          display: grid;
          grid-template-columns: 1fr 1fr;
          gap: ${theme.size016};
          overflow-x: auto;
        }
        .image-list__item {
          display: grid;
          grid-template-columns: auto;
          gap: ${theme.size004};
        }
        .image-list__item > img {
          border-radius: ${theme.size008};
          inline-size: 100%;
          aspect-ratio: 16 / 9;
          object-fit: cover;
        }
        .image-list__item > p {
          font: ${theme.fontSans11Normal};
          color: ${theme.colorObjectSubtle};
        }
        .card-area {
          padding-inline: ${theme.size016};
          padding-block-end: ${theme.size016};
          display: flex;
          flex-direction: column;
          gap: ${theme.size016};
        }
      `}</style>
    </>
  );
};
