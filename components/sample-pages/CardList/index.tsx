import { FC } from "react";
import { useTheme } from "../../../contexts/theme";
import { Notification } from "../../display/Notification";
import { VerticalCard } from "../../display/VerticalCard";
import { HeadingLevel1 } from "../../general/HeadingLevel1";
import { Page } from "../../layout/Page";
import { AppBar } from "../../navigation/AppBar";

export const CardList: FC = () => {
  const theme = useTheme();

  return (
    <>
      <Page>
        <AppBar />

        <div className="heading-area">
          <HeadingLevel1>タイトル</HeadingLevel1>
        </div>

        <div className="card-area">
          <VerticalCard />
          <VerticalCard />
          <VerticalCard />
          <VerticalCard />
        </div>

        <Notification />
      </Page>

      <style jsx>{`
        .heading-area {
          padding: ${theme.size016};
        }
        .card-area {
          padding-inline: ${theme.size016};
          padding-block-end: ${theme.size016};
          display: flex;
          flex-direction: column;
          gap: ${theme.size016};
        }
      `}</style>
    </>
  );
};
