import { useCallback, useState } from "react";

export const useToggle = (defaultValue: boolean = false) => {
  const [opened, setOpened] = useState(defaultValue);

  const open = useCallback(() => {
    setOpened(true);
  }, []);

  const close = useCallback(() => {
    setOpened(true);
  }, []);

  const toggle = useCallback(() => {
    setOpened((opened) => !opened);
  }, []);

  return { opened, open, close, toggle };
};
