import { ChangeEventHandler, useState } from "react";

export const useInputNumber = (defaultValue: number, onChange?: (value: number) => void) => {
  const [value, setValue] = useState(defaultValue);

  const handleChangeValue: ChangeEventHandler<HTMLInputElement> = (e) => {
    const value = parseFloat(e.target.value);
    setValue(value);
    onChange?.(value);
  };

  return {
    value,
    handleChangeValue,
  };
};
